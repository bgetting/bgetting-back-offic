import storage from "./storage";

export const localStore = async (key, data, expires) => {
    return await storage.save({
        key: key, // Note: Do not use underscore("_") in key!
        data: data,
        expires: expires
    });
};

export const getLocalStore = async (key) => {
    const result = await storage
        .load({
            key: key,
            autoSync: true,
            syncInBackground: true,
            syncParams: {
                extraFetchOptions: {
                    // blahblah
                },
                someFlag: true
            }
        })
        .catch(err => {
            // console.warn(err.message);
            // switch (err.name) {
            //     case 'NotFoundError':
            //         // TODO;
            //         break;
            //     case 'ExpiredError':
            //         // TODO
            //         break;
            // }
        });
    return result;
};

export const removeSingle = async (key) => {
    return await storage.remove({
        key: key
      });
}

export const removeAll = async () => {
    return await storage.clearMap();
}
