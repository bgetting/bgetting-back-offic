import React, { PureComponent, useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import AntDesign from "react-native-vector-icons/AntDesign";
import Ionicons from 'react-native-vector-icons/Ionicons';


const PlayerControlsVideo = (props) => {
    const {
        playing,
        showPreviousAndNext,
        showSkip,
        previousDisabled,
        nextDisabled,
        onPlay,
        onPause,
        skipForwards,
        skipBackwards,
        onNext,
        onPrevious,
    } = props;

    return (
        <View style={styles.wrapper}><Text>tEST</Text>
          
                <TouchableOpacity
                  
                 >
                    <AntDesign name="fastbackward" size="25" />
                </TouchableOpacity>
         

            {/* {showSkip && (
                <TouchableOpacity style={styles.touchable} onPress={skipBackwards}>
                    <Ionicons name="ios-play-skip-back-outline" size="25" />
                </TouchableOpacity>
            )}

            <TouchableOpacity
                style={styles.touchable}
                onPress={playing ? onPause : onPlay}>
                {playing ? <Ionicons name="md-pause-circle-outline" size="25" /> : <Ionicons name="play-circle-outline" size="25" />}
            </TouchableOpacity>

            {showSkip && (
                <TouchableOpacity style={styles.touchable} onPress={skipForwards}>
                    <Ionicons name="ios-play-skip-forward-outline" size="25" />
                </TouchableOpacity>
            )}

            {showPreviousAndNext && (
                <TouchableOpacity
                    style={[styles.touchable, nextDisabled && styles.touchableDisabled]}
                    onPress={onNext}
                    disabled={nextDisabled}>
                    <Ionicons name="md-play-forward-sharp" size="25" />
                </TouchableOpacity>
            )} */}
        </View>
    );

}
export default PlayerControlsVideo;

const styles = StyleSheet.create({
    wrapper: {
        paddingHorizontal: 5,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flex: 3,
    },
    touchable: {
        padding: 5,
    },
    touchableDisabled: {
        opacity: 0.3,
    },
});
