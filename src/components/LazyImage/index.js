import React, { useEffect, useState } from 'react';
import { Animated, ImageBackground, Image, StyleSheet, View, Text, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
const AnimatedOriginal = Animated.createAnimatedComponent(Image);
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

export default function LazyImage({
  smallSource,
  source,
  shouldLoad = false,
  aspectRatio = 1,
  providerName,
  videoNumber = 1,
  videoId,
  itemVideoId
}) {
  const opacity = new Animated.Value(0);

  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    if (shouldLoad) {
      const timeout = setTimeout(() => {
        setLoaded(true);
      }, 1000);
      return () => clearTimeout(timeout);
    }
    // return () => cleanup();

  }, [shouldLoad])

  function handleAnimate() {
    Animated.timing(opacity, {
      duration: 500,
      toValue: 1,
      useNativeDriver: true,
    }).start();
  }

  return (
    <>
      <ImageBackground
        source={smallSource}
        aspect={aspectRatio}
        resizeMode={providerName === 'YouTube' ? "cover" : "stretch"}
        blurRadius={1}
        style={styles.small}
      >
        {loaded && (
          <AnimatedOriginal
            style={styles.original}
            onLoadEnd={handleAnimate}
            source={source}
            aspect={aspectRatio}
            resizeMode={providerName === 'YouTube' ? "cover" : "stretch"}
          />
        )}
        {videoNumber > 1 ? <View style={styles.durationTimeList}>
          <Text style={styles.durationTextList}>{videoNumber && videoNumber >= 50 ? '50+' : videoNumber}</Text>
          <Ionicons name="radio-outline" size={25} color="#fff" />
        </View> : <View style={styles.durationTime}>
          <Text style={styles.durationText}>{aspectRatio}{providerName === 'YouTube' ? providerName : null}</Text>
        </View>}
        {itemVideoId === videoId && videoNumber === '' ? <View style={styles.videoPlayActive}><Ionicons name="radio-outline" size={25} color="#fff" /></View> : null}
      </ImageBackground>
    </>
  );
}

const styles = StyleSheet.create({
  small: {
    width: '100%',
    backgroundColor: '#eee',
    aspectRatio: 1.77,
    borderRadius: 7,
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: "#2F3337"
  },
  original: {
    top: 0,
    width: '100%',
    aspectRatio: 1.77,
  },
  durationTime: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    borderRadius: 9,
    paddingLeft: 12,
    paddingRight: 12,
    paddingBottom: 3,
    paddingTop: 2,
    bottom: 15,
    right: 20
  },
  durationText: {
    color: '#FFF',
    fontSize: 11
  },
  durationTimeList: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    // borderTopRightRadius: 7,
    // borderBottomRightRadius: 7,
    paddingLeft: 7,
    paddingRight: 7,
    paddingBottom: 3,
    paddingTop: 2,
    bottom: 0,
    right: 0,
    height: "100%",
    width: WIDTH / 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  durationTextList: {
    color: '#FFF',
    fontSize: 25
  },
  videoPlayActive: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    // borderBottomLeftRadius: 7,
    // borderBottomRightRadius: 7,
    paddingLeft: 7,
    paddingRight: 7,
    paddingBottom: 3,
    paddingTop: 2,
    bottom: 0,
    right: 0,
    height: WIDTH / 7,
    width: "100%",
    alignItems: 'center',
    justifyContent: 'center',
  }
});