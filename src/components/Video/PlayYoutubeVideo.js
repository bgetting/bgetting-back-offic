import React, { useState, useRef, useEffect } from 'react';
import { View, StyleSheet, Dimensions, ImageBackground } from 'react-native';
// import YoutubePlayer from 'react-native-youtube-iframe';
//import { WebView } from 'react-native-webview';
import Orientation from 'react-native-orientation';
import { getYoutubeMeta } from 'react-native-youtube-iframe';
import ArticleMoreVideo from "../Article/ArticleMoreVideo";
import axios from 'axios';
import YouTube from 'react-native-youtube';
// import Icon from 'react-native-vector-icons/Ionicons';
import Loading from '../Loading/loadingVideo';
import { API_URL } from '../../Services/api';
const WIDTH = Dimensions.get('window').width;
// const HEIGHT = Dimensions.get('window').height;
const height = WIDTH * 0.5625;

const PlayYoutubeVideo = (props) => {

    const [onLoading, setOnLoading] = useState(false);
    const youtubePlayerRef = useRef();
    const [isReady, setIsReady] = useState(false);
    const [status, setStatus] = useState(null);
    const [quality, setQuality] = useState(null);
    const [error, setError] = useState(null);
    const [isPlaying, setIsPlaying] = useState(true);
    const [isLooping, setIsLooping] = useState(true);
    const [duration, setDuration] = useState(0);
    const [currentTime, setCurrentTime] = useState(0);
    const [fullscreen, setFullscreen] = useState(false);
    const [containerMounted, setContainerMounted] = useState(false);
    const [containerWidth, setContainerWidth] = useState(null);
    const [orientation, setOrientation] = useState('PORTRAIT');

    const isProgress = (e) => {
        setDuration(e.duration);
        setCurrentTime(e.currentTime);
    }

    useEffect(() => {
        getYoutubeMeta(props.route.params.video_patch).then(meta => {
            // console.log('title of the video : ' + JSON.stringify(meta));
            setOnLoading(true);
        });
    }, []);

    React.useLayoutEffect(() => {
        fetchVideoView();
        Dimensions.addEventListener('change', () => {
            Orientation.getOrientation((err, orientation) => {
                _orientationDidChange(orientation);
            });
        });
    }, [props.navigation, props.route]);

    const _orientationDidChange = (orientation) => {
        if (orientation === 'PORTRAIT') {
            setOrientation('PORTRAIT');
            Orientation.lockToPortrait();
        } else {
            setOrientation('LANDSCAPE');
            Orientation.lockToLandscape();
        }
    }

    const onFullScreen = () => {
        setFullscreen(!fullscreen);
        if (!fullscreen) {
            Orientation.lockToLandscape();            
        } else {
            Orientation.lockToPortrait();
        }
    };

    const fetchVideoView = async () => {
        const { video_id } = props.route.params;
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/watch/${video_id}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET'
            },
        })
            .catch(error => {
                console.log(error);
            });
    };

    //const { orientation } = props;
    const { video_patch, thumbnail } = props.route.params;
    return (
        <View style={styles.container}>
            <View style={orientation == 'PORTRAIT' ? styles.layoutVideoPlayerPortrait : styles.layoutVideoPlayerLandscape}>
                <ImageBackground
                    source={{ uri: thumbnail }}
                    aspect={12}
                    resizeMode="cover"
                    blurRadius={0}
                    style={styles.small}
                >
                    <YouTube
                        ref={youtubePlayerRef}
                        // You must have an API Key
                        apiKey="AIzaSyDX9YM9Y1vRWiZCuAfOCVydVwyRR7XmQzY"
                        // Un-comment one of videoId / videoIds / playlist.
                        videoId={video_patch}
                        // videoIds={listVideoIds}
                        // playlistId="PLF797E961509B4EB5"
                        play={true}
                        autoplay={true}
                        loop={false}
                        fullscreen={orientation == 'PORTRAIT' ? fullscreen : true}
                        // fullscreen={ fullscreen }
                        controls={1}
                        style={[
                            {
                                height: orientation == 'PORTRAIT' ? WIDTH * 0.5625 : '100%',
                            },
                        ]}
                        onError={(e) => setError(e.error)}
                        onReady={(e) => setIsReady(true)}
                        onChangeState={(e) => setStatus(e.state)}
                        onChangeQuality={(e) => setQuality(e.quality)}
                        onChangeFullscreen={onFullScreen}
                        onProgress={isProgress}
                    />                    
                </ImageBackground>
            </View>
            <ArticleMoreVideo {...props} orientation={orientation} Arstyle={orientation == 'PORTRAIT' ? null : styles.ar_stylenone} />
        </View>
    );

};

export default PlayYoutubeVideo;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height: WIDTH * 0.5625,
        width: '100%',
        backgroundColor: '#1E2429',
    },
    small: {
        width: '100%',
        backgroundColor: '#eee',
        aspectRatio: 1.77,
        borderRadius: 0,
        overflow: 'hidden',
    },
    controlContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    layoutVideoPlayerPortrait: {
        width: '100%',
        height: height,
    },
    layoutVideoPlayerLandscape: {
        width: '100%',
        height: '100%',
    },
});