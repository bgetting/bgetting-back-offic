import React, { useState, useRef, useEffect } from "react";
import { StyleSheet, View, Dimensions, TouchableOpacity, Animated, Button } from "react-native";
import Video from "react-native-video";
import axios from 'axios';
import * as theme from '../../constants/theme';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import StatusBarTop from '../../components/StatusBar';
import Orientation from 'react-native-orientation';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import ArticleMoreVideo from "../Article/ArticleMoreVideo";
import MediaControls, { PLAYER_STATES } from 'react-native-media-controls';
import { API_URL } from '../../Services/api';
import Loading from '../../components/Loading/loadingVideo';
const WIDTH = Dimensions.get('window').width;
const height = WIDTH * 0.5625;

const PlayVideo = (props) => {
    const videoPlayer = useRef(null);
    const [currentTime, setCurrentTime] = useState(0);
    const [duration, setDuration] = useState(0);
    const [isFullScreen, setIsFullScreen] = useState(false);
    const [isVideoFullScreen, setIsVideoFullScreen] = useState(false);
    const [isLoading, setIsLoading] = useState(true);
    const [paused, setPaused] = useState(false);
    const [playerState, setPlayerState] = useState(PLAYER_STATES.PLAYING);
    const [mediaPlayerFullScreen, setMediaPlayerFullScreen] = useState(false);
    const [resizeMode, setResizeMode] = useState('contain');
    const [orientation, setOrientation] = useState('PORTRAIT');

    React.useLayoutEffect(() => {
        fetchVideoView();
        Dimensions.addEventListener('change', () => {
            Orientation.getOrientation((err, orientation) => {
                _orientationDidChange(orientation);               
            });         
        });

    }, [props.navigation, props.route]);

    const _orientationDidChange = (orientation) => {
        if (orientation === 'PORTRAIT') {
            setOrientation('PORTRAIT');
            setIsFullScreen(false);
            setIsVideoFullScreen(false);
            setMediaPlayerFullScreen(false);
        } else {
            setOrientation('LANDSCAPE');
            setIsFullScreen(true);
            setIsVideoFullScreen(true);
            setMediaPlayerFullScreen(true);
        }
    }
    
    const fetchVideoView = async () => {
        const { video_id } = props.route.params;
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/watch/${video_id}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET'
            },
        })
            .catch(error => {
                console.log(error);
            });
    };

    const onSeek = seek => {
        videoPlayer.current.seek(seek);
    };
    const onPaused = playerState => {
        setPaused(!paused);
        setPlayerState(playerState);
    };
    const onReplay = () => {
        videoPlayer.current.seek(0);
        setPlayerState(PLAYER_STATES.PLAYING);
    };

    const onProgress = data => {
        if (!isLoading && playerState !== PLAYER_STATES.ENDED) {
            setCurrentTime(data.currentTime);
        }
    };

    const onLoad = data => {
        setDuration(data.duration);
        setIsLoading(false);
    };

    const onLoadStart = data => setIsLoading(true);

    const onEnd = () => setPlayerState(PLAYER_STATES.ENDED);

    const onFullScreen = () => {

        setIsFullScreen(!isFullScreen);
        setIsVideoFullScreen(!isVideoFullScreen);
        setMediaPlayerFullScreen(!mediaPlayerFullScreen);
        setResizeMode('contain');
        if (!isFullScreen) {
            Orientation.lockToLandscape();            
        } else {
            Orientation.lockToPortrait();
            Orientation.unlockAllOrientations();
        }
    };
    const skipBackward = () => {
        videoPlayer?.current.seek(currentTime - 10);
        setCurrentTime(currentTime - 10);
    }

    const skipForward = () => {
        videoPlayer?.current.seek(currentTime + 10);
        setCurrentTime(currentTime + 10)
    }
    const renderToolbar = () => (
        <View style={styles.toolbar}>
            <View style={styles.back}>
                <TouchableOpacity onPress={skipBackward} style={styles.next_back}>
                    <MaterialIcons name="replay-10" size={50} color="#fff" />
                </TouchableOpacity>
            </View>
            <View style={styles.next}>
                <TouchableOpacity onPress={skipForward} style={styles.next_back}>
                    <MaterialIcons name="forward-10" size={50} color="#fff" />
                </TouchableOpacity>
            </View>

        </View>
    );

    const onSeeking = currentTime => setCurrentTime(currentTime);

    //const { orientation } = props;

    return (
        <View style={styles.container} >
            {isLoading? <Loading />: null}
            <StatusBarTop hidden={orientation == 'PORTRAIT' ? false : true} barStyle='default' backgroundColor={theme.colors.themBlack} />
            <Animated.View style={orientation == 'PORTRAIT' ? styles.layoutVideoPlayerPortrait : styles.layoutVideoPlayerLandscape}>

                <Video
                    source={{ uri: props.videoUri }}
                    onEnd={onEnd}
                    onLoad={onLoad}
                    onLoadStart={onLoadStart}
                    onProgress={onProgress}
                    paused={paused}
                    ref={(ref) => (videoPlayer.current = ref)}
                    resizeMode={resizeMode}
                    onFullScreen={isFullScreen}
                    rotat={true}
                    controls={true}
                    hideShutterView={false}
                    useNativeDriver={true}
                    style={orientation == 'PORTRAIT' ? (mediaPlayerFullScreen ? styles.playerFullScreen : styles.mediaPlayer) : styles.playerFullScreen}
                />
                {/* <MediaControls
                    duration={duration}
                    isLoading={isLoading}
                    mainColor="#D7143A"
                    onFullScreen={onFullScreen}
                    onPaused={onPaused}
                    onReplay={onReplay}
                    onSeek={onSeek}
                    onSeeking={onSeeking}
                    playerState={playerState}
                    progress={currentTime}
                    containerStyle={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}
                >
                    <MediaControls.Toolbar>
                        {renderToolbar()}
                    </MediaControls.Toolbar>
                </MediaControls> */}
            </Animated.View>
            {/* {!isFullScreen && <ArticleMoreVideo {...props} />} */}
            {/* <ArticleMoreVideo {...props} orientation={orientation} Arstyle={orientation == 'PORTRAIT' ? isFullScreen ? styles.ar_stylenone : null : !isFullScreen ? styles.ar_stylenone : null} /> */}
        </View>
    );
};

const styles = StyleSheet.create({
    ar_stylenone: {
        display: 'none'
    },
    container: {
        flex: 1,
        height: WIDTH * 0.5625,
        width: '100%',
        backgroundColor: '#1E2429',
    },
    toolbar: {
        flex: 1,
        flexDirection: 'row',
        position: 'absolute',
        width: '100%',
        height: '250%',
    },
    next: {
        flex: 2,
        alignItems: 'flex-end',
        marginTop: 40,
        right: '20%',
        justifyContent: "center",
    },
    back: {
        flex: 2,
        alignItems: 'flex-start',
        marginTop: 40,
        left: '20%',
        justifyContent: "center",
    },
    next_back: {
        alignItems: 'center',
    },
    mediaPlayer: {
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: "black",
        height: WIDTH * 0.5625,
    },
    playerFullScreen: {
        //zIndex: 999,
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor: "#000",
        height: '100%',
        width: '100%',
    },
    meiaControlsStyle: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 200,
    },
    controls: {
        backgroundColor: "rgba(0,0,0,0.6)",
        height: '100%',
        left: 0,
        bottom: 0,
        right: 0,
        position: 'absolute',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    duration: {
        color: '#fff',
        marginLeft: 15
    },
    wrapper: {
        flex: 1,
        left: 0,
        bottom: 0,
        right: 0,
        paddingLeft: 10,
        paddingRight: 10,
        alignItems: 'center',
        position: 'absolute',
    },
    slider: {
        marginBottom: 0,
    },
    timeWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        bottom: 10
    },
    timeLeft: {
        flex: 1,
        fontSize: 16,
        color: '#FFFFFF',
    },
    timeRight: {
        flex: 1,
        fontSize: 16,
        color: '#FFFFFF',
        textAlign: 'right',
    },
    iconPlay: {
        top: 0,
        alignItems: "center",
        justifyContent: "center",
        bottom: 0,
        height: '100%'
    },
    iconBack: {
        top: 0,
        left: 10,
        position: 'absolute',
        bottom: 0,
    },
    layoutVideoPlayerPortrait: {
        width: '100%',
        height: height,
    },
    layoutVideoPlayerLandscape: {
        width: '100%',
        height: '100%',
    },
});

export default PlayVideo;