import React from 'react';
import { Platform, StatusBar } from 'react-native';

const StatusBarTop = (props) =>{
    const { hidden, backgroundColor, barStyle } = props;
    return Platform.OS === 'ios' ? <StatusBar barStyle={barStyle} hidden ={hidden} {...props} /> : <StatusBar barStyle={barStyle} backgroundColor={backgroundColor} hidden ={hidden} />
}

export default StatusBarTop;