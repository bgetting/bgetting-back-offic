import React from "react";
import { View, Text, Image, StyleSheet } from 'react-native';

function NotFound(props) {
    const { source, title, desc } = props;
    return (
        <View style={styles.container}>
            <Image source={source} style={styles.img} resizeMode='contain' />
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.desc}>{desc}</Text>
        </View>
    );
}

export default NotFound;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        marginBottom: 10,
    },
    img: {
        width: 150
    },
    title: {
        fontSize: 20,
        fontWeight: '900'
    },
    desc: {
        color: '#aaa'
    } 
});