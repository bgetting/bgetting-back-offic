import React, { useEffect } from 'react';
import { View, TouchableHighlight, Text, StyleSheet, ActivityIndicator } from 'react-native';
// import Icon from 'react-native-vector-icons/Ionicons';
// import Feather from 'react-native-vector-icons/Feather';


// THEME IMPORT
import * as theme from '../../constants/theme';



function Button(props) {
    const { title, style, action, disabled, activityIndicator=false} = props;

    return (
        <React.Fragment>
            <TouchableHighlight
                onPress={action}
                style={[styles.btn, style, disabled?styles.disabledBtn:null]}
                activeOpacity={0.6}
                underlayColor="#f97c7c"
                disabled={disabled}>
                <Text style={styles.text} >
                    {title}
                    {activityIndicator ? <View style={styles.loadings}><ActivityIndicator size={20} color="#fff" /></View>: null}
                </Text>
            </TouchableHighlight>
        </React.Fragment>
    );
}
export default Button;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: "center",
    },
    loadings: {
        paddingLeft: 10,
        width: 40
    },
    btn: {
        backgroundColor: theme.colors.themeRed,
        paddingTop: 7,
        paddingBottom: 7,
        paddingLeft: 20,
        paddingRight: 20,
        borderRadius: 7,
        alignItems: 'center',
        justifyContent: "center",
    },
    disabledBtn: {
        backgroundColor: theme.colors.themeDisable,
    },
    text: {
        color: '#fff',
    }
});