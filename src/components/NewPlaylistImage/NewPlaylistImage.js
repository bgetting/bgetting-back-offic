import React, { useEffect, useState } from 'react';
import { Animated, ImageBackground, Image, StyleSheet, View, Text, Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

const AnimatedOriginal = Animated.createAnimatedComponent(Image);

export default function LazyImageShort({
    smallSource,
    source,
    shouldLoad = false,
    aspectRatio = 1,
    videoNumber = 1
}) {
    const opacity = new Animated.Value(0);

    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        if (shouldLoad) {
            const timeout = setTimeout(() => {
                setLoaded(true);
            }, 1000);
            // return () => cleanup()
            return () => clearTimeout(timeout);
        }
    }, [shouldLoad])

    function handleAnimate() {
        Animated.timing(opacity, {
            duration: 500,
            toValue: 1,
            useNativeDriver: true,
        }).start();
    }

    return (
        <>
            <ImageBackground
                source={smallSource}
                aspect={aspectRatio}
                resizeMode="cover"
                blurRadius={0}
                style={styles.small}
            >
                {loaded && (
                    <AnimatedOriginal
                        style={styles.original}
                        onLoadEnd={handleAnimate}
                        source={source}
                        aspect={aspectRatio}
                        resizeMode="cover"
                    />
                )}
                <View style={styles.durationTime}>
                    <Text style={styles.durationText}>{videoNumber && videoNumber >= 50 ? '50+' : videoNumber}</Text>
                    <Ionicons name="radio-outline" size={25} color="#fff" />
                </View>
            </ImageBackground>
        </>
    );
}

const styles = StyleSheet.create({
    small: {
        width: WIDTH / 2.2,
        backgroundColor: '#eee',
        aspectRatio: 1.77,
        borderRadius: 7,
        overflow: 'hidden',
        borderWidth: 1,
        borderColor: "#2F3337"
    },
    original: {
        top: 0,
        width: '100%',
        aspectRatio: 1.77,
    },
    durationTime: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        borderTopRightRadius: 7,
        borderBottomRightRadius: 7,
        paddingLeft: 7,
        paddingRight: 7,
        paddingBottom: 3,
        paddingTop: 2,
        bottom: 0,
        right: 0,
        height: "100%",
        width: WIDTH / 6,
        alignItems: 'center',
        justifyContent: 'center',
    },
    durationText: {
        color: '#FFF',
        fontSize: 16
    }
});