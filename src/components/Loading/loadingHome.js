import React from 'react';
import { View, ActivityIndicator, StyleSheet, } from 'react-native';

// THEME IMPORT
import * as theme from '../../constants/theme';

function Loading() {
    return (
        <View style={styles.container}>
            <View style={styles.containerLoading}>
                <ActivityIndicator size="large" color={theme.colors.themeRed} />
            </View>
        </View>
    );
}
export default Loading;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 99,
        position: 'absolute',
        backgroundColor: theme.colors.themeWhite,
    },
    containerLoading: {
        width: 80,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 7,
        // backgroundColor: theme.colors.themeWhite,
    }

});