import React from 'react';
import { View, StyleSheet, } from 'react-native';
import { InstagramLoader } from 'react-native-easy-content-loader';

// THEME IMPORT
import * as theme from '../../constants/theme';

function Loading() {
    return (
        // <View style={styles.container}><ActivityIndicator size="large" color={theme.colors.themeRed} /></View>
        <View style={styles.container}>
            <InstagramLoader active />
            <InstagramLoader active />
            <InstagramLoader active />
        </View>
    );
}
export default Loading;
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        width: '100%',
        height: '100%',
        // justifyContent: 'center',
        // alignItems: 'center',
        // zIndex: 99,
        // position: 'absolute',
        backgroundColor: theme.colors.themeWhite,
    },

});