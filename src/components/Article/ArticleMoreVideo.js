import React, { Component } from 'react';
import {
    View,
    Text,
    FlatList,
    Image,
    ActivityIndicator,
    Dimensions,
    // Animated,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    TouchableHighlight,
    Alert,
    ToastAndroid
} from 'react-native';
import axios from 'axios';
import Orientation from 'react-native-orientation';
import DeviceInfo from 'react-native-device-info';
// import BottomSheet from 'react-native-simple-bottom-sheet';
import Share from 'react-native-share';
import ImgToBase64 from 'react-native-image-base64';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Hyperlink from 'react-native-hyperlink';
import LazyImage from '../../components/LazyImage';
import * as storeService from '../../storage/service';
import NetworkUtils from '../../network/NetworkUtills';
// import LazyImageShort from '../../components/LazyImageShort';
// import { ButtomSheetSignUp, ButtomSheetSignIn } from '../../components/BottomSheet';
import { API_URL, API_URL_FILE } from '../../Services/api';
import NotFound from '../../components/NotFound';
import Loading from '../../components/Loading';
import TouchableIcon from '../TouchableIcon';
// THEME IMPORT
import * as theme from '../../constants/theme';
const url = `${API_URL_FILE}/resources/`;
const url_profile = `${API_URL_FILE}/resources-profile/`;
const WIDTH = Dimensions.get('window').width;

class ArticleMoreVideo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            page: 1,
            refreshing: false,
            error: null,
            loading: false,
            notfound: false,
            contentVerticalOffset: 0,
            active: 0,
            findUri: this.props.route.params.findUri,
            tagsRecommended: [],
            findTitle: "",
            isNetwork: true,
            onlike: 0,
            checkonlike: false,
            onunlike: 0,
            checkonunlike: false,
            panelRef: null,
            panelVideoRef: null,
            bodyBottomSheet: true,
            likes_view: 0,
            unlikes_view: 0,
            checkSubscribe: false,
            isTablet: DeviceInfo.isTablet(),
            vodeoTotal: '',
            lengJsonData: '',
        };
    }

    componentDidMount() {
        this.fetchVideo();
        this.fetchVideoHistoryRecord();
    }

    showToast = (ms) => {
        ToastAndroid.show(ms, ToastAndroid.SHORT);
    }

    myCustomShare = async () => {
        const { thumbnail, title } = this.props.route.params;
        const imgBase64 = await ImgToBase64.getBase64String(thumbnail)
            .then(base64String => {
                return 'data:image/png;base64,' + base64String;
            })
            .catch(err => doSomethingWith(err));
        const shareOptions = {
            title: "BGetting",
            message: title,
            url: imgBase64
        }

        try {
            const ShareResponse = await Share.open(shareOptions);
        } catch (error) {
            console.log('Error => ', error);
        }
    };

    signIn = () => {
        this.setState({ bodyBottomSheet: true });
    };

    signUp = () => {
        this.setState({ bodyBottomSheet: false });
    };

    _onPressTo = (data) => {
        this.props.navigation.navigate(data, { type: data });
    }

    fetchVideoHistoryRecord = async () => {
        const { video_id } = this.props.route.params;
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        if (localdataJs.user.id) {
            await axios({
                method: 'POST',
                url: `${API_URL}/api/auth-service/v1/video/video-history-record/${video_id}/${localdataJs.user.id}`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                    Authorization: `Bearer ${localdataJs.accessToken}`
                },
            })
                .catch(error => {
                    this.setState({ error: error });
                });
        }
    };

    fetchVideoLikeUnlike = async () => {
        const { video_id } = this.props.route.params;
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/like-unlike?video_id=${video_id}${localdataJs.user.id ? '&user_id=' + localdataJs.user.id : ''}${this.state.checkonlike ? '&like=' + this.state.onlike : ''}${this.state.checkonunlike ? '&unlike=' + this.state.onunlike : ''}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET',
                Authorization: `Bearer ${localdataJs.accessToken}`
            },
        })
            .then(response => {
                this.fetchVideoView();
            })
            .catch(error => {
                this.setState({ error: error });
            });
    };

    fetchVideoView = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        const { video_id } = this.props.route.params;
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/like-unlike-view?video_id=${video_id}${localdataJs.user.id ? '&user_id=' + localdataJs.user.id : ''}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET'
            },
        })
            .then(response => {
                this.setState({
                    likes_view: response.data[0].likes_view,
                    unlikes_view: response.data[0].unlikes_view,
                    onlike: response.data[0]?.likes,
                    onunlike: response.data[0]?.unlikes,
                    defaultLikes: response.data[0].likes_view
                });
            })
            .catch(error => {
                this.setState({ error: error });
            });
    };


    fetchCheckSubscribe = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        const { itemId } = this.props.route.params;
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/subscribe/${itemId}/${localdataJs.user.id}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET'
            },
        })
            .then(response => {
                this.setState({ checkSubscribe: response.data.status == 404 ? false : true });
            })
            .catch(error => {
                this.setState({ checkSubscribe: false });
            });
    };

    _onPressSubscribeConfirmDialog = () => {
        return Alert.alert(
            null,
            "Are you sure you want to Unsubscribe?",
            [
                {
                    text: "Cancel",
                },
                {
                    text: "Unsubscribe",
                    onPress: () => this._onPressSubscribe(),
                },
            ]
        );
    };

    _onPressSubscribe = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        if (localdataJs.success) {
            const { itemId } = this.props.route.params;
            await axios({
                method: 'POST',
                url: `${API_URL}/api/auth-service/v1/video/subscribe/${itemId}/${localdataJs.user.id}`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                    Authorization: `Bearer ${localdataJs.accessToken}`
                },
            })
                .then(response => {
                    this.showToast(response.data.message);
                    this.setState({ checkSubscribe: response.data.status });
                });
        } else {
            //this.state.panelRef.togglePanel();
            Alert.alert(
                null,
                'Please sing in or sign up.',
                [{ text: 'OK' }]
            );
        }
    };

    fetchVideo = async () => {
        const { page, findUri } = this.state;
        const { video_id } = this.props.route.params;
        const isConnected = await NetworkUtils.isNetworkAvailable();
        const UrlPlayList = this.props.route.params.playlist_id;
        const checkUrlPlayList = UrlPlayList ? `/video-get-list?playlist_id=${UrlPlayList}&` : `?${this.state.findTitle ? 'title_tags=' + this.state.findTitle + '&' : ''}${video_id ? 'video_not_in_id=' + video_id : ''}${this.state.findTitle ? '' : findUri}&`;
        this.setState({ isNetwork: isConnected });
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        this.setState({ refreshing: false });
        this.setState({ loading: true });
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video${checkUrlPlayList}limit=10&page=${page}${localdataJs.user.id ? '&user_watch_id=' + localdataJs.user.id : ''}${video_id ? '&video_watch_id=' + video_id : ''}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET'
            },
        })
            .then(response => {
                this.fetchVideoView();
                this.fetchCheckSubscribe();
                this.setState({
                    datas: this.state.datas.concat(response.data.data),
                });
                this.setState({ lengJsonData: Object.keys(response.data.data).length });
                this.setState({ vodeoTotal: response.data.total });
                this.setState({ notfound: true })
                if (this.state.tagsRecommended.length == 0) {
                    this.setState({
                        tagsRecommended: this.state.tagsRecommended.concat(response.data.recommended),
                    });
                }
                this.setState({ loading: false });
            })
            .catch(error => {
                this.setState({ error: error });
            });
    };

    fetchMoreData = () => {
        if (this.state.lengJsonData <= this.state.vodeoTotal && this.state.lengJsonData != 0) {
            this.setState(
                prevState => ({
                    page: prevState.page + 1,
                }),
                () => {
                    this.fetchVideo();
                },
            );
        }
    };
    fetchRefresh = () => {
        this.setState(
            prevState => ({
                page: prevState.page / prevState.page,
                datas: [],
                notfound: false
            }),
            () => {
                this.fetchVideo();
            },
        );

    };

    handleRefresh = () => {
        this.setState({ refreshing: true }, () => { this.fetchRefresh() });
        this.setState({ refreshing: false });
    }

    Empty = () => {
        return (<View style={styles.emptyContain}>
            <NotFound
                source={require('../../../assets/404.png')}
                title='Connect to the internet'
                desc='Check your connection' />
            <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
                <Text style={styles.emptyListStyle} >
                    RETRY
                </Text>
            </TouchableHighlight>
        </View>)
    }

    _onPress(item) {
        const UrlPlayListRefresh = this.props.route.params.playlist_id;
        if (!UrlPlayListRefresh) {
            this.handleRefresh();
        }
        Orientation.unlockAllOrientations();
        const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
        const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
        const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
        const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
        const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
        const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
        const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
        const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
        const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
        const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
        const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

        this.props.navigation.navigate('Watch', {
            itemId: item.id,
            video_id: item.video_id,
            title: item.title,
            video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
            image_path: item.image_path,
            image_cover_path: item.image_cover_path,
            thumbnail: url + item.thumbnail,
            provider_name: item.provider_name,
            time_agos: item.time_agos,
            views: item.views,
            subscribe: item.subscribe,
            username: item.username,
            phone_number: item.phone_number,
            destination: item.destination,
            findUri: getUri,
            playlist_id: item.playlist_id,
            title_playlist: item.title_playlist,
            desc_playlist: item.desc_playlist,
            video_number: item.video_number,
            description: item.description,
            email_address: item.email_address
        });
        this.setState({ findUri: getUri });
        this.props.navigation.setOptions({ tabBarVisible: false });
    }

    _renderItem = ({ item, index }) => {
        return (
            <View style={[styles.post, this.state.isTablet ? styles.post_orientation : null]}>
                <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={0.6}
                    underlayColor="rgba(198, 198, 198, 0.1)">
                    <LazyImage
                        aspectRatio={item?.duration}
                        shouldLoad={true}
                        smallSource={{ uri: url + item?.thumbnail }}
                        source={{ uri: url + item?.thumbnail }}
                        providerName={item?.provider_name}
                        videoNumber={item?.video_number}
                        videoId={this.props.route.params?.video_id}
                        itemVideoId={item?.video_id}
                    />
                </TouchableHighlight>
                <View style={styles.footer}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Channel',
                            {
                                userid: item.id,
                                username: item.username,
                                phone_number: item.phone_number,
                                image_path: item.image_path,
                                image_cover_path: item.image_cover_path,
                            })} >
                        <Image style={styles.avatar} source={item.image_path ? { uri: url_profile + item.image_path } : require('../../../assets/me1.png')} />
                    </TouchableOpacity>
                    <View style={styles.timeAgo}>
                        <Text style={styles.title}>{item.title_playlist ? item?.title_playlist : item?.title}</Text>
                        <Text style={styles.titleTime}>{item.username ? `${item.username} - ` : null} {item.time_agos}</Text>
                    </View>
                </View>
            </View>
        )

    };

    _renderItemList = ({ item, index }) => {
        return (
            <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={1} underlayColor="rgba(198, 198, 198, 0.5)" style={this.props.route.params?.video_id === item?.video_id ? { backgroundColor: 'rgba(198, 198, 198, 0.5)' } : null}>
                <View style={styles.playlistVideo1}>
                    <View style={styles.playlistVideoThum1}>
                        <Image
                            source={{ uri: url + item?.thumbnail }}
                            style={styles.playlistThumbnail1}
                            resizeMode={item.provider_name === 'YouTube' ? "cover" : "stretch"}
                        />
                        <View style={styles.durationTime1}><Text style={styles.durationText1}>{item.duration ? item.duration : 'Youtube'}</Text></View>
                    </View>
                    <View style={styles.playlistText1}>
                        <Text>{item.title.length >= 60 ? this.state.orientation ? item?.title : item?.title.substring(0, 60) + '...' : item?.title}</Text>
                        <Text style={styles.playlistSubText1}>{item.views} <Entypo name='dot-single' size={10} /> {item.time_agos}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    };

    Render_FlatList_Sticky_header = () => {
        const {
            itemId,
            title,
            time_agos,
            views,
            username,
            phone_number,
            image_path,
            image_cover_path,
            subscribe,
            description,
            email_address
        } = this.props.route.params;
        var Sticky_header_View = (
            <View>
                <View style={[styles.padding]}>
                    <Text style={styles.titleVideo}>{title}</Text>
                    <Text style={styles.viewsdata}>{views} <Entypo name='dot-single' size={10} /> {time_agos}</Text>
                    <View style={styles.likeRow}>
                        <TouchableIcon name="like2" onPress={() => this._handleLikePress(this.state.onlike == 1 ? 2 : 1)} active={this.state.onlike == 1 ? "#1878F3" : "#767577"}>{this.state.likes_view}</TouchableIcon>
                        <TouchableIcon name="dislike2" onPress={() => this._handleUnLikePress(this.state.onunlike == 1 ? 2 : 1)} active={this.state.onunlike == 1 ? "#1878F3" : "#767577"}>{this.state.unlikes_view}</TouchableIcon>
                        <TouchableIcon name="sharealt" onPress={this.myCustomShare}>Share</TouchableIcon>
                        {/* <TouchableIcon name="download">Download</TouchableIcon> */}
                        <TouchableIcon name="save">Save</TouchableIcon>
                    </View>
                    {description ? <View style={styles.description}><Hyperlink linkDefault={true} linkStyle={{color: '#2980b9', fontSize: 12}}>
                        <Text style={{fontSize: 12}}>
                            {description}
                        </Text>
                    </Hyperlink></View> : null}
                </View>

                <View style={[styles.channelInfo, styles.padding]}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Channel',
                            {
                                userid: itemId,
                                username: username,
                                phone_number: phone_number,
                                image_path: image_path,
                                image_cover_path: image_cover_path
                            })}>
                        <Image
                            source={image_path ? { uri: url_profile + image_path } : require('../../../assets/me1.png')}
                            style={styles.channelIcon}
                            resizeMode="cover"
                        />
                    </TouchableOpacity>
                    <View style={styles.channelText}>
                        <Text style={styles.channelTitle}>{username ? username.length > 15 ? username.substring(0, 15) + '...' : username : email_address.length > 10 ? email_address.substring(0, 10) + '...' : email_address}</Text>
                        <Text style={styles.channelSubscribers}>{subscribe} subscribers</Text>
                    </View>
                    {this.state.checkSubscribe ? <TouchableOpacity style={styles.subscribers} onPress={() => this._onPressSubscribeConfirmDialog()}>
                        <Text style={styles.title_subscribed}>SUBSCRIBED <MaterialCommunityIcons name='bell-ring-outline' size={20} /></Text>
                    </TouchableOpacity> :
                        <TouchableOpacity style={styles.subscribers} onPress={() => this._onPressSubscribe()}>
                            <Text style={styles.title_subscribers}>SUBSCRIBE</Text>
                        </TouchableOpacity>}
                </View>
                <View style={styles.padding}>
                    <Text style={styles.playlistUpNext}>Up next</Text>
                </View>
            </View>
        );

        return Sticky_header_View;

    };
    _handlePress = (value) => {
        this.setState({ active: value.rownum });
        this.setState({ findTitle: value.title_tags });
        this.fetchRefresh();
    };

    _handleLikePress = async (value) => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : {};
        if (localdataJs.success) {
            this.setState({ onlike: value });
            this.setState({ checkonlike: true }, () => { this.fetchVideoLikeUnlike() });
            this.setState({ checkonunlike: false });
        } else {
            // this.state.panelRef.togglePanel();
            Alert.alert(
                null,
                'Please sing in or sign up.',
                [{ text: 'OK' }]
            );
        }

    };

    _handleUnLikePress = async (value) => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : {};
        if (localdataJs.success) {
            this.setState({ onunlike: value });
            this.setState({ checkonunlike: true }, () => { this.fetchVideoLikeUnlike() });
            this.setState({ checkonlike: false });
        } else {
            //this.state.panelRef.togglePanel();
            Alert.alert(
                null,
                'Please sing in or sign up.',
                [{ text: 'OK' }]
            );
        }

    };

    render() {
        const {
            Arstyle,
            orientation
        } = this.props;
        const checkUrlPlayList = this.props.route.params.playlist_id;
        return (
            <React.Fragment>
                {!this.state.notfound ? orientation == 'PORTRAIT' ? <Loading /> : null : null}
                <View style={[styles.header_style, Arstyle]}>
                    <View style={[styles.container_view, Arstyle]}>
                        <ScrollView
                            horizontal={true}
                            pagingEnabled={false}
                            showsVerticalScrollIndicator={false}
                            showsHorizontalScrollIndicator={false}>
                            <View style={styles.viewChip}>
                                <TouchableOpacity
                                    onPress={() => this._handlePress({ rownum: 0, title_tags: '' })}
                                    style={this.state.active === 0 ? styles.btnActive : styles.btn}>
                                    <Text style={this.state.active === 0 ? styles.textChip : styles.text} > All </Text>
                                </TouchableOpacity>
                            </View>
                            {
                                !checkUrlPlayList && this.state.tagsRecommended && this.state.tagsRecommended.map((data) => {
                                    return (<View style={styles.viewChip} key={data.rownum}>
                                        <TouchableOpacity
                                            onPress={() => this._handlePress(data)}
                                            style={this.state.active === data.rownum ? styles.btnActive : styles.btn}>
                                            <Text style={this.state.active === data.rownum ? styles.textChip : styles.text} > {data.title_tags.length >= 20 ? data.title_tags.substring(0, 20) + "..." : data.title_tags} </Text>
                                        </TouchableOpacity>
                                    </View>)
                                })
                            }
                        </ScrollView>
                    </View>
                </View>
                <FlatList
                    numColumns={this.state.isTablet ? 2 : 1}
                    key={this.state.isTablet ? "three" : "list"}
                    data={this.state.datas}
                    keyExtractor={(item, index) => index.toString()}
                    onViewableItemsChanged={this.handleViewableChanged}
                    viewabilityConfig={{
                        viewAreaCoveragePercentThreshold: 10,
                    }}
                    showsVerticalScrollIndicator={false}
                    onEndReachedThreshold={0.1}
                    onEndReached={this.fetchMoreData}
                    ListFooterComponent={this.state.loading && this.state.datas.length > 5 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
                    ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
                    renderItem={this.props.route.params.playlist_id ? this.state.isTablet ? this._renderItem : this._renderItemList : this._renderItem}
                    style={[{ backgroundColor: "#fff", marginBottom: 0 }, Arstyle]}
                    ListHeaderComponent={this.Render_FlatList_Sticky_header}
                />
                {/* <BottomSheet
                    isOpen={false}
                    sliderMinHeight={0}
                    sliderMaxHeight={Dimensions.get('window').height * 0.90}
                    // ref={ref => panelRef.current = ref}
                    ref={ref => this.state.panelRef = ref}
                    wrapperStyle={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        paddingLeft: 0,
                        paddingRight: 0,
                        borderWidth: 1,
                        borderColor: theme.colors.themBtnBorder
                    }}
                >
                    {this.state.bodyBottomSheet ? <ButtomSheetSignIn signUp={this.signUp} _onPressTo={() => this._onPressTo('SignIn')} /> : <ButtomSheetSignUp signIn={this.signIn} _onPressTo={() => this._onPressTo('SignUp')} />}
                </BottomSheet> */}
            </React.Fragment>
        );
    }
}
export default ArticleMoreVideo;

const styles = StyleSheet.create({
    ar_stylenone: {
        display: 'none'
    },
    description: {
        paddingVertical: 7
    },
    post: {
        flex: 1,
        paddingHorizontal: 15,
    },
    post_orientation: {
        paddingHorizontal: 10,
        paddingTop: 15
    },
    flatlistLayout: {
        paddingHorizontal: 7,
    },
    footer: {
        paddingTop: 10,
        paddingBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 7,
    },
    avatar: {
        width: 35,
        height: 35,
        borderRadius: 18,
        marginRight: 10,
    },
    title: {
        // fontWeight: '700',
        flexWrap: 'wrap',
        flex: 1
    },
    // description: {
    //     padding: 15,
    //     lineHeight: 18
    // },
    emptyContain: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 50
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        color: '#fff'
    },
    timeAgo: {
        width: 0,
        flexGrow: 1,
    },
    titleTime: {
        color: '#aaaaaa',
        fontSize: 12
    },
    shortLayout: {
        backgroundColor: '#FFF',
        margin: 5,
    },
    titleHeaderShorts: {
        paddingTop: 7,
        paddingBottom: 7
    },
    layoutShorts: {
        paddingLeft: 10,
        paddingRight: 10
    },
    btnRetry: {
        backgroundColor: theme.colors.themeRed,
        width: 100,
        borderRadius: 5,
    },
    channelInfo: {
        flexDirection: "row",
        alignItems: "center",
        borderBottomWidth: 1,
        borderBottomColor: theme.colors.themBtnBorder,
        borderTopWidth: 1,
        borderTopColor: theme.colors.themBtnBorder,
    },
    subscribers: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center",
    },
    title_subscribers: {
        color: theme.colors.themeRed,
        fontWeight: 'bold'
    },
    title_subscribed: {
        color: theme.colors.themBlackRgba,
        fontWeight: 'bold'
    },
    channelIcon: {
        width: 45,
        height: 45,
        borderRadius: 45,
        borderWidth: 1,
        borderColor: 'rgba(226, 226, 226, 1)'
    },
    channelText: {
        flex: 1,
        marginLeft: 15,
    },
    channelTitle: {
        fontSize: 18,
        marginBottom: 5,
    },
    channelSubscribers: {
        fontSize: 12,
        color: theme.colors.themBlackRgba
    },
    titleVideo: {
        fontSize: 18,
        fontWeight: '900'
    },
    likeRow: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingVertical: 15,
    },
    padding: {
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    playlistUpNext: {
        fontSize: 18,
        marginBottom: 7,
    },
    playlistVideo: {
        flexDirection: "row",
        height: 100,
        marginTop: 10,
    },
    playlistThumbnail: {
        width: null,
        height: null,
        borderRadius: 0,
        flex: 1,
    },
    playlistText: {
        flex: 1,
        paddingLeft: 15,
    },
    playlistVideoTitle: {
        fontSize: 17,
    },
    playlistSubText: {
        color: "#555",
    },
    header_style: {
        width: '100%',
        height: 45,
        backgroundColor: theme.colors.themeWhite,
        justifyContent: 'center',
        left: 0,
        right: 0,
        top: 0,
        borderBottomColor: theme.colors.themBorderRgba,
        borderBottomWidth: 1,
    },
    container_view: {
        backgroundColor: theme.colors.themeWhite,
        paddingTop: 3
    },
    btn: {
        alignItems: 'center',
        borderColor: theme.colors.themeRed,
        borderRadius: 20,
        borderWidth: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        paddingBottom: 4,
    },
    btnActive: {
        alignItems: 'center',
        backgroundColor: theme.colors.themeRed,
        borderColor: theme.colors.themeRed,
        borderRadius: 20,
        borderWidth: 1,
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 3,
        paddingBottom: 4,
    },
    viewChip: {
        padding: 5
    },
    textChip: {
        color: theme.colors.themeWhite
    },
    text: {
        color: theme.colors.themeRed
    },
    viewsdata: {
        marginTop: 7,
        fontSize: 12
    },

    playlistVideo1: {
        flexDirection: "row",
        height: WIDTH / 3.7,
        marginVertical: 10,
        paddingHorizontal: 15
    },
    playlistVideoThum1: {
        flex: 2,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#2F3337",
        overflow: 'hidden',
    },
    playlistThumbnail1: {
        width: "100%",
        height: "100%",
    },
    playlistText1: {
        flex: 2,
        paddingLeft: 15,
    },
    playlistSubText1: {
        color: theme.colors.themBlackRgba,
        fontSize: 12
    },
    durationTime1: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        borderRadius: 9,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 3,
        paddingTop: 2,
        bottom: 15,
        right: 20
    },
    durationText1: {
        color: '#FFF',
        fontSize: 11
    }
});