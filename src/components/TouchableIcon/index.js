
import React from 'react';
import { TouchableOpacity, StyleSheet, Text } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

export default TouchableIcon = ({ name, children, active="#767577", onPress }) => {
    return (
        <TouchableOpacity style={styles.touchIcon} onPress={onPress}>
            <AntDesign name={name} size={25} color={active} />
            <Text style={styles.iconText}>{children}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    touchIcon: {
        alignItems: "center",
        justifyContent: "center",
    },
    iconText: {
        marginTop: 5,
        fontSize: 11,
    },
});