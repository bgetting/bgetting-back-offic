import React from 'react';
import { View, TouchableHighlight, Text, StyleSheet, } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

import {
    HEADER_TITLE_SIGN_UP,
    HEADER_TITLE_LOG_IN,
    HEADER_TITLE_LOG_IN_SUB,
    HEADER_TITLE_SIGN_UP_SUB
} from '../../constants/login';

// THEME IMPORT
import * as theme from '../../constants/theme';

function RenderBtn ({ icon, name, action, style, styleText }) {
    return <View><TouchableHighlight onPress={action}
        style={[styles.btnClickContain, style]}
        underlayColor={theme.colors.themeRedRgba}>
        <View
            style={styles.btnContainer}>
            <AntDesign
                name={icon}
                size={20}
                color='#042'
                style={[styles.btnIcon, styleText]} />
            <Text style={[styles.btnText, styleText]}>{name}</Text>
        </View>
    </TouchableHighlight>
    </View>
}

export const ButtomSheetSignUp = (props) => {
    return <View>
        <View style={styles.constainButtomSheet}>
            <View style={styles.constain_signUp}>
                <Text style={styles.title_signUp}>{HEADER_TITLE_SIGN_UP}</Text>
                <Text style={styles.title_signUpsub}>{HEADER_TITLE_SIGN_UP_SUB}</Text>
            </View>
            <RenderBtn
                action={props._onPressTo}
                style={{backgroundColor: '#01A3F0'}}
                styleText={{color: '#fff'}}
                name='Sing up'
                icon='user' />
        </View>
        <View style={styles.footer}>
            <TouchableHighlight
                onPress={props.signIn}
                style={styles.btnFooter}
                underlayColor={theme.colors.themBorderRgba}>
                <View style={styles.footerText}>
                    <Text style={styles.footerTextsub}>Already have an account? <Text style={styles.footerLogin}>Login</Text></Text>
                </View>
            </TouchableHighlight>
        </View>
    </View>
}

export const ButtomSheetSignIn = (props) => {
    return <View>
        <View style={styles.constainButtomSheet}>
            <View style={styles.constain_signUp}>
                <Text style={styles.title_signUp}>{HEADER_TITLE_LOG_IN}</Text>
                <Text style={styles.title_signUpsub}>{HEADER_TITLE_LOG_IN_SUB}</Text>
            </View>
            <RenderBtn
                action={props._onPressTo}
                style={{backgroundColor: theme.colors.themeRed}}
                styleText={{color: '#fff'}}
                name='Sign in'
                icon='login' />
        </View>
        <View style={styles.footer}>
            <TouchableHighlight
                onPress={props.signUp}
                style={styles.btnFooter}
                underlayColor={theme.colors.themBorderRgba}>
                <View style={styles.footerText}>
                    <Text style={styles.footerTextsub}>Don't have an account? <Text style={styles.footerLogin}>Sign up</Text></Text>
                </View>
            </TouchableHighlight>
        </View>
    </View>
}

const styles = StyleSheet.create({
    constainButtomSheet: {
        flex: 1,
        paddingBottom: 50,
        paddingLeft: 25,
        paddingRight: 25,
    },
    constain_signUp: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    title_signUp: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    title_signUpsub: {
        color: theme.colors.themBlackRgba,
        textAlign: "center",
        marginTop: 10,
        marginBottom: 15,
    },
    btnClickContain: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
        alignSelf: 'stretch',
        // borderColor: theme.colors.themBtnBorder,
        // borderWidth: 1,
        borderRadius: 5,        
        padding: 10,
        marginTop: 15,
        marginBottom: 15,
        paddingLeft: 15,
        paddingRight: 15,
    },
    btnContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
        alignSelf: 'stretch',
        borderRadius: 10,
    },
    btnIcon: {
        flex: 1,
        color: theme.colors.themBlack
    },
    btnText: {
        flex: 2,
        fontSize: 16,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 20,
        color: theme.colors.themBlack,
    },
    btnFooter: {
        backgroundColor: '#F6F6F6',
        paddingBottom: 15,
        paddingTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopColor: theme.colors.themBtnBorder,
        borderTopWidth: 1,
    },
    footerLogin: {
        color: theme.colors.themeRed,
        fontWeight: '900'
    }
});