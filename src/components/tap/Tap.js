/* Libraries */
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, SafeAreaView, Dimensions, Button, TouchableHighlight } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
// import LinearGradient from 'react-native-linear-gradient';
import BottomSheet from 'react-native-simple-bottom-sheet';

// ICONS IMPORT
import Octicons from 'react-native-vector-icons/Octicons';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
// THEME IMPORT
import * as theme from '../../constants/theme';
import {
    HEADER_TITLE_SIGN_UP,
    HEADER_TITLE_LOG_IN,
    HEADER_TITLE_LOG_IN_SUB,
    HEADER_TITLE_SIGN_UP_SUB
} from '../../constants/login';

class Tap extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile: null,
            body: true,
        };
    }

    signIn = () => {
        this.setState({ body: false });
    }

    signUp = () => {
        this.setState({ body: true });
    }

    RenderBtn = ({ icon, name }) => {
        return <TouchableHighlight
            style={styles.btnClickContain}
            underlayColor={theme.colors.themBorderRgba}>
            <View
                style={styles.btnContainer}>
                <AntDesign
                    name={icon}
                    size={20}
                    color='#042'
                    style={styles.btnIcon} />
                <Text style={styles.btnText}>{name}</Text>
            </View>
        </TouchableHighlight>
    }


    render() {
        const { body } = this.state;
        const { RenderBtn } = this;

        return (
            <>
                {/* <LinearGradient
                    // start={{ x: 0, y: 1 }}
                    // end={{ x: 1, y: 0 }}
                    colors={['rgba(255,255,255,1.8)', 'rgba(255,255,255,1.5)', theme.colors.themeWhite]}
                    style={{ position: 'absolute', bottom: 0, width: '100%', height: EStyleSheet.value('55rem') }}>
                    <SafeAreaView style={{ flex: 1 }}>
                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', paddingBottom: EStyleSheet.value('5rem') }}>
                            <TouchableOpacity style={{ ...styles.tabHeader }} onPress={() => this.props.navigation.navigate('Home')}>
                                <Octicons name={'home'} style={[styles.octicons, styles.activeOcticons]} size={EStyleSheet.value('25rem')} />
                                <Text style={[styles.textTap, styles.activeOcticons]}>Home</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ ...styles.tabHeader }} onPress={() => this.props.navigation.navigate('Video')}>
                                <Ionicons name={'videocam-outline'} style={styles.octicons} size={EStyleSheet.value('25rem')} />
                                <Text style={{ ...styles.textTap }}>Video</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ ...styles.tabHeader }} onPress={() => this.state.profile.togglePanel()}>
                                <MaterialIcons name={'add-circle-outline'} style={styles.octicons} size={EStyleSheet.value('43rem')} />
                            </TouchableOpacity>
                            <TouchableOpacity style={{ ...styles.tabHeader }} onPress={() => this.props.navigation.navigate('PostScreen')}>
                                <MaterialIcons name={'notifications-none'} style={styles.octicons} size={EStyleSheet.value('25rem')} />
                                <Text style={{ ...styles.textTap }}>Home</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ ...styles.tabHeader }} onPress={() => this.props.navigation.navigate('Profile')}>
                                <Feather name={'user'} style={styles.octicons} size={EStyleSheet.value('25rem')} />
                                <Text style={{ ...styles.textTap }}>Me</Text>
                            </TouchableOpacity>
                        </View>
                    </SafeAreaView>

                </LinearGradient> */}
                <BottomSheet
                    isOpen={false}
                    sliderMinHeight={0}
                    sliderMaxHeight={Dimensions.get('window').height * 0.90}
                    ref={ref => this.state.profile = ref}
                    wrapperStyle={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        paddingLeft: 0,
                        paddingRight: 0,
                        borderWidth: 1,
                        borderColor: theme.colors.themBtnBorder
                    }}
                >
                    <View style={styles.constain}>
                        <View style={styles.constain_signUp}>
                            <Text style={styles.title_signUp}>{body ? HEADER_TITLE_SIGN_UP : HEADER_TITLE_LOG_IN}</Text>
                            <Text style={styles.title_signUpsub}>{body ? HEADER_TITLE_SIGN_UP_SUB : HEADER_TITLE_LOG_IN_SUB}</Text>
                        </View>
                        <RenderBtn
                            name='Use phone or email'
                            icon='user' />
                    </View>
                    <View style={styles.footer}>
                        {body ? <TouchableHighlight
                            onPress={this.signIn}
                            style={styles.btnFooter}
                            underlayColor={theme.colors.themBorderRgba}>
                            <View style={styles.footerText}>
                                <Text style={styles.footerTextsub}>Already have an account? <Text style={styles.footerLogin}>Login</Text></Text>
                            </View>
                        </TouchableHighlight> :
                            <TouchableHighlight
                                onPress={this.signUp}
                                style={styles.btnFooter}
                                underlayColor={theme.colors.themBorderRgba}>
                                <View style={styles.footerText}>
                                    <Text style={styles.footerTextsub}>Don't have an account? <Text style={styles.footerLogin}>Sign up</Text></Text>
                                </View>
                            </TouchableHighlight>}
                    </View>
                </BottomSheet>
            </>
        );
    }
    // END RENDERING FUNCTIONS
}

const styles = EStyleSheet.create({
    tabHeader: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textTap: {
        color: theme.colors.themBlackRgba,
        fontSize: '10rem'
    },
    octicons: {
        color: theme.colors.themBlackRgba,
    },
    activeOcticons: {
        color: theme.colors.themeRed,
    },
    constain: {
        flex: 1,
        paddingBottom: 50,
        paddingLeft: 25,
        paddingRight: 25,
    },
    constain_signUp: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    title_signUp: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    title_signUpsub: {
        color: theme.colors.themBlackRgba,
        textAlign: "center",
        marginTop: 10,
        marginBottom: 15,
    },
    btnClickContain: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'stretch',
        alignSelf: 'stretch',
        borderColor: theme.colors.themBtnBorder,
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        marginTop: 15,
        marginBottom: 15,
        paddingLeft: 15,
        paddingRight: 15,
    },
    btnContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'stretch',
        alignSelf: 'stretch',
        borderRadius: 10,
    },
    btnIcon: {
        flex: 1,
        color: theme.colors.themBlack
    },
    btnText: {
        flex: 3,
        fontSize: 16,
        color: theme.colors.themBlack,
    },
    btnFooter: {
        backgroundColor: '#F6F6F6',
        paddingBottom: 15,
        paddingTop: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderTopColor: theme.colors.themBtnBorder,
        borderTopWidth: 1,
    },
    footerLogin: {
        color: theme.colors.themeRed,
        fontWeight: '900'
    }
});

export default Tap;
