const colors = {
    themeBlue: '#69c9d0',
    themeRed: '#D7143A',
    themeRedRgba: 'rgba(215, 20, 58, 0.6)',
    themeDisable: 'rgba(0, 0, 0, 0.3)',
    themeWhite: "#ffffff",
    themBlack: "#000000",
    themText: "#474749",
    themBlackRgba: "rgba(0, 0, 0, 0.6)",
    themBorderRgba: "rgba(0, 0, 0, 0.1)",
    themBtnBorder: "#DFDFDF",
    themBlue: "#0063F7",
    themBlackWhite: "#F7F7F7"
};

const appDims = {
    boundary: 20,
};

export {colors, appDims};
