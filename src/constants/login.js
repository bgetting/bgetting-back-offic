export const HEADER_TITLE_SIGN_UP = "Sign up for BGeting";
export const HEADER_TITLE_LOG_IN = "Sign in to BGeting";
export const HEADER_TITLE_SIGN_UP_SUB = "Create a profile, follow other accounts, make your own video, and more.";
export const HEADER_TITLE_LOG_IN_SUB = "Manage your account, check notifications, comment on videos, and more";