import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { StyleSheet } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

// ICONS IMPORT
import Octicons from 'react-native-vector-icons/Octicons';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import * as storeService from './storage/service';
// THEME IMPORT
// import * as theme from './constants/theme';
import SplashScreen from 'react-native-splash-screen';
import RootHome from './Screens/Home';
// import Loading from './components/Loading/loadingHome';
import RootProfile from './Screens/Profile/RootProfile';
import RootUpload from './Screens/Upload/index';
import RootExplore from './Screens/Video/rootExplore';
import RootNotification from './Screens/Notification';

const Tab = createBottomTabNavigator();
const iconSize = 18;

function App(props) {
    const [isLoading, setIsLoading] = useState(true);
    useEffect(() => {
        SplashScreen.hide();        
        // setTimeout(() => {
        //     setIsLoading(false);
        //   }, 5000);
    }, []);

    return (
        <React.Fragment>
            {/* {isLoading?<Loading />: null} */}
            <Tab.Navigator
                initialRouteName="Home"
                backBehavior="initialRoute"
                shifting={true}
                sceneAnimationEnabled={false}
                tabBarOptions={{
                    activeTintColor: '#e91e63',
                    // inactiveTintColor: 'lightgray',
                    // activeBackgroundColor: '#c4461c',
                    // inactiveBackgroundColor: 'rgba(34,36,40,0.1)',
                    // style: {
                    //     backgroundColor: '#292929'
                    // }
                }}
            >
                <Tab.Screen
                    name="Home"
                    component={RootHome}
                    options={route => ({
                        tabBarLabel: 'Home',
                        tabBarIcon: ({ color, size }) => (
                            <Octicons name="home" color={color} size={iconSize} />
                        ),
                        // tabBarVisible: false,
                    })}
                />
                <Tab.Screen
                    name="Video"
                    component={RootExplore}
                    options={{
                        tabBarLabel: 'Video',
                        tabBarIcon: ({ color, size }) => (
                            <Ionicons name="videocam-outline" color={color} size={iconSize} />
                        ),
                    }}

                />
                <Tab.Screen name="Upload"
                    component={RootUpload}
                    options={({ navigation, route }) => {
                        return {
                            tabBarIcon: ({ color, size }) => (
                                <MaterialIcons name="add-circle-outline" color={color} size={iconSize} />
                            ),
                            // tabBarButton: (props) => (<TouchableOpacity  {...props} onPress={() => { panelRef.current.togglePanel() }} />),
                        };
                    }} />
                <Tab.Screen
                    name="Notifications"
                    component={RootNotification}
                    options={{
                        tabBarLabel: 'Notifications',
                        tabBarIcon: ({ color, size }) => (
                            <MaterialIcons name="notifications-none" color={color} size={iconSize} />
                        ),
                        // tabBarBadge: 3,
                    }}
                />
                <Tab.Screen
                    name="Me"
                    component={RootProfile}
                    options={{
                        tabBarLabel: 'Me',
                        tabBarIcon: ({ color, size }) => (
                            <Feather name="user" color={color} size={iconSize} />
                        ),
                    }}
                />
            </Tab.Navigator>
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "flex-end",
        alignItems: 'center',
        backgroundColor: "#efeff4",
    },
    textContent: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});
export default App;