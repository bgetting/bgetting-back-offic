import React, { Component } from 'react';
import {
    View,
    TouchableHighlight,
    Text, StyleSheet, TextInput, TouchableOpacity, Alert,
    Image, Dimensions,
    FlatList,
    ActivityIndicator,
    ToastAndroid
} from 'react-native';
import axios from 'axios';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import NetworkUtils from '../../network/NetworkUtills';
import * as storeService from '../../storage/service';
import { API_URL, API_URL_FILE } from '../../Services/api';
import Loading from '../../components/Loading';
import NotFound from '../../components/NotFound';
// import Action from '../../components/Action';
// THEME IMPORT
import * as theme from '../../constants/theme';
const url = `${API_URL_FILE}/resources/`;
const WIDTH = Dimensions.get('window').width;

class MyVideoChannel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            onLoading: false,
            orientation: false,
            page: 1,
            refreshing: false,
            error: null,
            loading: false,
            notfound: false,
            isNetwork: true,
            searchClear: false,
            textSearch: "",
            submitTextSearch: "",
            dropdown: "",
            editProfile: false,
            vodeoTotal: '',
            lengJsonData: ''
        };
    }

    componentDidMount() {
        Dimensions.addEventListener('change', ({ window: { width, height } }) => {
            if (width < height) {
                this.setState({ orientation: false });
            } else {
                this.setState({ orientation: true });
            }
        });
        this.fetchVideo();
    }

    componentWillReceiveProps(nextProps) {
        for (const index in nextProps) {
            if (nextProps[index] !== this.props[index]) {
                this.fetchRefresh();
            }
        }
    }

    showToast = (ms) => {
        ToastAndroid.show(ms, ToastAndroid.SHORT);
    }

    fetchVideo = async () => {
        const isConnected = await NetworkUtils.isNetworkAvailable();
        this.setState({ isNetwork: isConnected });
        const { page } = this.state;
        const { userid } = this.props.route?.params;
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        const statusProfile = userid === localdataJs.user.id ? true : false;
        this.setState({ editProfile: statusProfile });
        this.setState({ refreshing: false });
        this.setState({ loading: true });
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/channel/${userid + '?'}limit=10&page=${page}${this.state.submitTextSearch ? '&title=' + this.state.submitTextSearch : ''}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET',
                Authorization: `Bearer ${localdataJs.accessToken}`
            },
        })
            .then(response => {
                this.setState({
                    datas: this.state.datas.concat(response.data.data),
                });
                this.setState({ lengJsonData: Object.keys(response.data.data).length });
                this.setState({ vodeoTotal: response.data.total });
                this.setState({ loading: false });
                this.setState({ notfound: true })
            })
            .catch(error => {
                this.setState({ error: error });
            });
    };

    fetchMoreData = () => {
        if (this.state.lengJsonData <= this.state.vodeoTotal && this.state.lengJsonData != 0) {
            this.setState(
                prevState => ({
                    page: prevState.page + 1,
                }),
                () => {
                    this.fetchVideo();
                },
            );
        }
    };
    fetchRefresh = () => {
        this.setState(
            prevState => ({
                page: prevState.page / prevState.page,
                datas: [],
                notfound: false
            }),
            () => {
                this.fetchVideo();
            },
        );

    };

    handleRefresh = () => {
        this.setState({ refreshing: true }, () => { this.fetchRefresh() });
        this.setState({ refreshing: false });
    }

    Empty = () => {
        return (<View style={styles.emptyContain}>
            <NotFound
                source={require('../../../assets/404.png')}
                title='Connect to the internet'
                desc='Check your connection' />
            <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
                <Text style={styles.emptyListStyle} >
                    RETRY
                </Text>
            </TouchableHighlight>
        </View>)
    }
    _onPress(item) {
        const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
        const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
        const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
        const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
        const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
        const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
        const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
        const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
        const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
        const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
        const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

        this.props.navigation.navigate('Watch', {
            rownum: item.rownum,
            itemId: item.id,
            video_id: item.video_id,
            title: item?.title,
            video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
            image_path: item.image_path,
            image_cover_path: item.image_cover_path,
            thumbnail: url + item?.thumbnail,
            provider_name: item.provider_name,
            time_agos: item.time_agos,
            views: item.views,
            subscribe: item.subscribe,
            username: item.username,
            phone_number: item.phone_number,
            destination: item.destination,
            findUri: getUri,
            playlist_id: item.playlist_id,
            title_playlist: item.title_playlist,
            desc_playlist: item.desc_playlist,
            video_number: item.video_number,
            description: item.description,
            email_address: item.email_address
        });
        this.props.navigation.setOptions({ tabBarVisible: false });
    }

    _handlePressDropdown = (item) => {
        this.setState({ dropdown: item.rownum });
    }
    deleteConfirmDialog = (item) => {
        return Alert.alert(
            "Confirm Delete",
            "Are you sure you want to delete?",
            [
                {
                    text: "Yes",
                    onPress: () => this.deleteVideo(item),
                },
                {
                    text: "No",
                    onPress: () => this._handlePressDropdown(0),
                },
            ]
        );
    };

    deleteVideo = async (item) => {
        // console.log(item);
        this._handlePressDropdown(0);
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        await axios({
            method: 'DELETE',
            url: `${API_URL}/api/auth-service/v1/video/${item.video_id}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET',
                Authorization: `Bearer ${localdataJs.accessToken}`
            },
        })
            .then(response => {
                this.fetchRefresh();
                this.showToast("Delete successfully !");
            })
            .catch(error => {
                console.log(error);
            });
    }

    _renderItem = ({ item, index }) => {
        return (
            <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={1} underlayColor="rgba(198, 198, 198, 0.1)">
                <View style={styles.playlistVideo}>
                    <View style={this.state.orientation ? styles.playlistVideoThumOrientation : styles.playlistVideoThum}>
                        <Image
                            source={{ uri: url + item?.thumbnail }}
                            style={styles.playlistThumbnail}
                            resizeMode={item.provider_name === 'YouTube' ? "cover" : "stretch"}
                        />
                        <View style={styles.durationTime}><Text style={styles.durationText}>{item.duration ? item.duration : 'Youtube'}</Text></View>
                    </View>
                    <View style={styles.playlistText}>
                        <View>
                            <View style={styles.action}>
                                {this.state.editProfile && <TouchableOpacity
                                    style={styles.actionbtn}
                                    onPress={() => this._handlePressDropdown(item)}>
                                    <Entypo name='dots-three-vertical' size={10} />
                                </TouchableOpacity>}
                            </View>
                            {this.state.editProfile && this.state.dropdown === item.rownum ? <View style={styles.actionDropDown}>
                                <View style={styles.actionHeader}>
                                    <Text style={styles.actionTitleHeader}>Action</Text>
                                    <TouchableOpacity
                                        style={styles.actionClose}
                                        onPress={() => this._handlePressDropdown(0)}>
                                        <AntDesign name='close' size={18} />
                                    </TouchableOpacity>
                                </View>
                                <TouchableHighlight
                                    activeOpacity={1}
                                    underlayColor={theme.colors.themBorderRgba}
                                    onPress={() => this.deleteConfirmDialog(item)}>
                                    <View style={styles.actionList}>
                                        <Text>Delete</Text>
                                    </View>
                                </TouchableHighlight>
                            </View> : null}
                        </View>
                        <Text>{item.title.length >= 60 ? this.state.orientation ? item?.title : item?.title.substring(0, 60) + '...' : item?.title}</Text>
                        <Text style={styles.playlistSubText}>{item.views} <Entypo name='dot-single' size={10} /> {item.time_agos}</Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    };

    focusNext = (text) => {
        if (text) {
            this.setState({ searchClear: true });
            this.setState({ textSearch: text });
        }
    }

    btnSearchClear = () => {
        this.setState({ searchClear: false });
        this.setState({ textSearch: "" });
        this.setState({ submitTextSearch: "" });
        this.setState(
            prevState => ({
                page: prevState.page / prevState.page,
                datas: [],
                notfound: false
            }),
            () => {
                this.fetchVideo();
            },
        );
    }
    submitSearch = () => {
        this.setState({ submitTextSearch: this.state.textSearch });
        this.setState(
            prevState => ({
                page: prevState.page / prevState.page,
                datas: [],
                notfound: false
            }),
            () => {
                this.fetchVideo();
            },
        );
    }
    SearchHistory = () => (
        <>
            <View>
                <View style={styles.searchIcon}>
                    <EvilIcons name="search" size={30} color="#8a8b8c" />
                </View>
                <TextInput
                    selectionColor={theme.colors.themeRed}
                    underlineColorAndroid="transparent"
                    maxLength={250}
                    keyboardType='default'
                    style={styles.otpInputStyle}
                    placeholder="Search your videos"
                    defaultValue={this.state.textSearch}
                    onChangeText={text => this.focusNext(text)}
                    returnKeyType="search"
                    onSubmitEditing={() => this.submitSearch()}
                    placeholderTextColor="rgba(0,0,0,0.6)"
                />
                {this.state.searchClear ? <View style={styles.searchIconDelete}>
                    <TouchableOpacity onPress={() => this.btnSearchClear()}>
                        <EvilIcons name="close" size={25} color="#8a8b8c" />
                    </TouchableOpacity>
                </View> : null}

            </View>
            <View style={styles.historyTitle}><Text>Your Videos</Text></View>
        </>
    );

    render() {

        return (
            <React.Fragment>
                {/* {onLoading ? <View style={styles.isloading}><Loading /></View> : null} */}
                {!this.state.notfound ? <Loading /> : null}
                <FlatList
                    numColumns={1}
                    key="list"
                    data={this.state.datas}
                    keyExtractor={(item, index) => index.toString()}
                    onViewableItemsChanged={this.handleViewableChanged}
                    viewabilityConfig={{
                        viewAreaCoveragePercentThreshold: 10,
                    }}
                    showsVerticalScrollIndicator={false}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    onEndReachedThreshold={0.1}
                    onEndReached={this.fetchMoreData}
                    ListFooterComponent={this.state.loading && this.state.datas.length > 5 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
                    ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
                    renderItem={this._renderItem}
                    ListHeaderComponent={this.SearchHistory}
                    style={{ backgroundColor: "#fff" }}
                />
            </React.Fragment>
        );
    }
}
export default MyVideoChannel;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    otpInputStyle: {
        height: 45,
        width: '100%',
        paddingHorizontal: 50,
        textAlign: 'left',
        backgroundColor: "rgba(145, 145, 145, 0.05)"
    },
    searchIcon: {
        position: "absolute",
        marginTop: 10,
        paddingLeft: 10
    },
    searchIconDelete: {
        position: "absolute",
        marginTop: 12,
        right: 15
    },
    historyTitle: {
        fontWeight: "900",
        marginVertical: 10,
        paddingHorizontal: 15
    },
    list_container: {
        paddingHorizontal: 15
    },
    isloading: {
        flex: 1,
        backgroundColor: theme.colors.themBlackRgba,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    playlistVideo: {
        flexDirection: "row",
        height: WIDTH / 3.7,
        marginVertical: 10,
        paddingHorizontal: 15
    },
    playlistVideoThum: {
        flex: 2,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#2F3337",
        overflow: 'hidden',
    },
    playlistVideoThumOrientation: {
        flex: 0.7,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#2F3337",
        overflow: 'hidden',
    },
    playlistThumbnail: {
        width: "100%",
        height: "100%",
        borderRadius: 7
    },
    playlistText: {
        flex: 2,
        paddingLeft: 15,
    },
    playlistSubText: {
        color: theme.colors.themBlackRgba,
        fontSize: 12
    },
    emptyContain: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 50
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        color: '#fff'
    },
    btnRetry: {
        backgroundColor: theme.colors.themeRed,
        width: 100,
        borderRadius: 5,
    },
    actionHeader: {
        flex: 1,
        paddingHorizontal: 15,
        paddingVertical: 10,
        // borderColor: theme.colors.themBorderRgba,
        // borderBottomWidth: 1,
    },
    actionClose: {
        position: "absolute",
        right: 7,
        top: 7
    },
    actionTitleHeader: {
        fontWeight: "bold"
    },
    action: {
        position: "absolute",
        right: -10,
        top: -7,
        zIndex: 999
    },
    actionbtn: {
        padding: 7,
        borderRadius: 15
    },
    actionDropDown: {
        width: 150,
        backgroundColor: "#fff",
        position: "absolute",
        zIndex: 999,
        right: 0,
        top: 0,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        paddingBottom: 5
    },
    actionList: {
        paddingHorizontal: 25,
        paddingVertical: 7,
        width: "100%"
    },
    durationTime: {
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.7)',
        borderRadius: 9,
        paddingLeft: 12,
        paddingRight: 12,
        paddingBottom: 3,
        paddingTop: 2,
        bottom: 15,
        right: 20
      },
      durationText: {
        color: '#FFF',
        fontSize: 11
      }
});