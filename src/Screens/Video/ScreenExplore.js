import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  SafeAreaView,
  YellowBox,
  ScrollView
} from 'react-native';
import axios from 'axios';
import Orientation from 'react-native-orientation';
// import Slideshow from 'react-native-image-slider-show';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import Entypo from 'react-native-vector-icons/Entypo';
import DeviceInfo from 'react-native-device-info';
import FlatListSlider from '../../components/Slider/FlatListSlider';
import LazyImage from '../../components/LazyImage';
import { API_URL, API_URL_FILE } from '../../Services/api';
import NotFound from '../../components/NotFound';
import Loading from '../../components/Loading';
import NetworkUtils from '../../network/NetworkUtills';
// THEME IMPORT
import * as theme from '../../constants/theme';
const url = `${API_URL_FILE}/resources/`;
const urlImg = `${API_URL_FILE}/resources/images/`;
// const url_profile = `${API_URL_FILE}/resources-profile/`;
const WIDTH = Dimensions.get('window').width;
const jsonSlide = [];
YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

class ScreenExplore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      datasTopic: [],
      datasExplore: [],
      page: 1,
      refreshing: false,
      error: null,
      loading: false,
      notfound: false,
      isNetwork: true,
      orientation: false,
      onActive: 22,
      findTitle: '',
      activeIndex: 0,
      isTablet: DeviceInfo.isTablet(),
      position: 1,
      interval: null,
      datasSlide: [],
    };
  }
  componentDidMount() {
    Orientation.lockToPortrait();
    Dimensions.addEventListener('change', ({ window: { width, height } }) => {
      if (width < height) {
        this.setState({ orientation: false });
      } else {
        this.setState({ orientation: true });
      }
    });
    this.fetchVideo();
    this.fetchSlideExplore();
    this.fetchSlider();
    // this.setState({
    //   interval: setInterval(() => {
    //     this.setState({
    //       position: this.state.position === this.state.datasSlide.length ? 0 : this.state.position + 1
    //     });
    //   }, 10000)
    // });
  }

  componentWillReceiveProps(nextProps) {
    for (const index in nextProps) {
      if (nextProps[index] !== this.props[index]) {
        this.fetchRefresh();
      }
    }
  }

  componentWillUnmount() {
    Orientation.lockToPortrait();
    clearInterval(this.state.interval);
  }

  fetchVideo = async () => {
    const isConnected = await NetworkUtils.isNetworkAvailable();
    this.setState({ isNetwork: isConnected });
    const { page } = this.state;
    this.setState({ refreshing: false });
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ notfound: true })
    }, 15000);
    await axios({
      method: 'GET',
      // url: `${API_URL}/api/auth-service/v1/video/video-hottet-topic?limit=15&page=${page}${this.state.findTitle ? '&title_tags=' + this.state.findTitle : ''}`,
      url: `${API_URL}/api/auth-service/v1/video/video-playlist?limit=16&page=${page}${this.state.findTitle ? '&title_tags=' + this.state.findTitle : ''}`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        this.setState({
          datas: this.state.datas.concat(response.data.data),
        });
        this.setState({ loading: false });
        this.setState({ notfound: true })
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  fetchSlideExplore = async () => {
    await axios({
      method: 'GET',
      url: `${API_URL}/api/auth-service/v1/video/slide-explore`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        // response.data.slide.map((row) => {
        //   const newValues = {};
        //   newValues.desc = row.description;
        //   newValues.image = `${urlImg}background/${row.image}`;
        //   return jsonSlide.push(newValues);
        // });
        // this.setState({ datasSlide: jsonSlide });
        this.setState({
          datasExplore: response.data.explore,
        });
        this.setState({
          datasTopic: response.data.topic,
        });
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  fetchSlider = async () => {
    await axios({
      method: 'GET',
      url: `${API_URL}/api/auth-service/v1/video/slide-explore`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        response.data.slide.map((row, index) => {
          const newValues = {};
          newValues.id = index;
          newValues.name_en = row.lov_code;
          newValues.desc = row.description;
          newValues.tags = row.lov_code;
          newValues.image = `${urlImg}background/${row.image}`;
          return jsonSlide.push(newValues);
        });
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  sliderFindEntries = (num) => {
    // const selected = Object.entries(jsonSlide).find(a =>a[1].id === num);
    const selected = Object.entries(jsonSlide).find(([_, e]) => e.id === num);
    const [key, findObj] = selected;
    this.props.navigation.navigate('ScreenExploreCategory', {id:findObj.id, image: findObj.image, name_en: findObj.name_en, tags: findObj.tags})
  }

  fetchMoreData = () => {
    this.setState(
      prevState => ({
        page: prevState.page + 1,
      }),
      () => {
        this.fetchVideo();
      },
    );

  };
  fetchRefresh = () => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        datasSlide: [],
        datasExplore: [],
        notfound: false
      }),
      () => {
        this.fetchVideo();
        this.fetchSlideExplore();
      },
    );

  };

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => { this.fetchRefresh() });
    this.setState({ refreshing: false });
  }

  Empty = () => {
    return (<View style={styles.emptyContain}>
      <NotFound
        source={require('../../../assets/404.png')}
        title='Connect to the internet'
        desc='Check your connection' />
      <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
        <Text style={styles.emptyListStyle} >
          RETRY
        </Text>
      </TouchableHighlight>
    </View>)
  }

  _onPress(item) {
    const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
    const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
    const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
    const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
    const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
    const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
    const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
    const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
    const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
    const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
    const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

    this.props.navigation.navigate('Watch', {
      rownum: item.rownum,
      itemId: item.id,
      video_id: item.video_id,
      title: item.title,
      video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
      image_path: item.image_path,
      image_cover_path: item.image_cover_path,
      thumbnail: url + item.thumbnail,
      provider_name: item.provider_name,
      time_agos: item.time_agos,
      views: item.views,
      subscribe: item.subscribe,
      username: item.username,
      phone_number: item.phone_number,
      destination: item.destination,
      findUri: getUri,
      playlist_id: item.playlist_id,
      title_playlist: item.title_playlist,
      desc_playlist: item.desc_playlist,
      video_number: item.video_number,
      description: item.description,
      email_address: item.email_address
    });
    this.props.navigation.setOptions({ tabBarVisible: false });
  }

  _renderBorder = () => (
    <View style={{
      backgroundColor: theme.colors.themBorderRgba,
      width: '100%',
      height: 1,
      marginTop: 20
    }}></View>
  );  

  _renderHeader = () => {
    return (
      <View>
        <View style={styles.slideshoCont}>
          {/* <Slideshow
            dataSource={this.state.datasSlide}
            position={this.state.position}
            overlay={false}
            indicatorSize={7}
            scrollEnabled={true}
            arrowLeft={<Entypo name="chevron-small-left" size={30} color={theme.colors.themeWhite} />}
            arrowRight={<Entypo name="chevron-small-right" size={30} color={theme.colors.themeWhite} />}
            indicatorColor='#fff'
            indicatorSelectedColor='#F75431'
            titleStyle={{ color: theme.colors.themeWhite, fontSize: 16 }}
            captionStyle={{ color: theme.colors.themeWhite }}
            onPositionChanged={position => this.setState({ position })} /> */}
          <FlatListSlider
            data={jsonSlide}
            timer={10000}
            onPress={item => this.sliderFindEntries(item)}
            indicatorContainerStyle={{ position: 'absolute', bottom: 20 }}
            indicatorActiveColor={'#8e44ad'}
            indicatorInActiveColor={'#BABFC4'}
            indicatorActiveWidth={30}
            animation
            height={WIDTH / 2}
          />
        </View>
        <View style={styles.hotest_topic_container}>
          <Text style={styles.hotest_topicLeft}>Topic</Text>
          <TouchableOpacity
            style={styles.readMore}
            onPress={() => this.props.navigation.navigate('ScreenExploreCategoryList')}>
            <Text style={styles.hotest_topicRight}>More <AntDesign name="right" size={10} color={theme.colors.themeWhite} /></Text>
          </TouchableOpacity>
        </View>
        <View style={styles.exploreItemList}>
          <FlatList
            numColumns={2}
            listKey={(item, index) => index.toString()}
            data={this.state.datasExplore}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            onEndReachedThreshold={0.1}
            ListEmptyComponent={null}
            renderItem={this.exploreItem}
            style={{ paddingHorizontal: 5 }}
          />
        </View>
        {this._renderBorder()}
        <View style={styles.hotest_topic_container}>
          <Text style={styles.hotest_topicLeft}>Categories</Text>
        </View>
        <View style={styles.container_view}>
          <ScrollView
            horizontal={true}
            pagingEnabled={false}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}>
            {
              this.state.datasTopic && this.state.datasTopic.map((data) => {
                return (<View style={styles.viewChip} key={data.id}>
                  <TouchableOpacity
                    onPress={() => this._handlePress(data)}
                    style={this.state.onActive === data.id ? styles.btnActive : styles.btn}>
                    <Text style={this.state.onActive === data.id ? styles.textChip : styles.text} > {data.name_en.length >= 20 ? data.name_en.substring(0, 20) + "..." : data.name_en} </Text>
                  </TouchableOpacity>
                </View>)
              })
            }
          </ScrollView>
        </View>
      </View>)
  }

  exploreItem = ({ item, index }) => (
    <View style={styles.post_explore} key={item.id}>
      <View style={styles.exploreLayout}>
        <View style={styles.exploreLayoutBtn}>
          <TouchableOpacity
            activeOpacity={.7}
            onPress={() => this.props.navigation.navigate('ScreenExploreCategory', item)}>
            <View style={styles.iconlayout}>
              <View style={styles.iconleft}>
                <Image
                  source={{ uri: `${urlImg}icon/${item.icon}` }}
                  resizeMode="cover"
                  style={styles.exploreIcon}
                />
              </View>
              <View style={styles.icontext}>
                <Text style={styles.textExplore}>
                  {item.name_en.length >= 10 ? this.state.orientation ? item.name_en.length <= 35 ? item.name_en : item.name_en.substring(0, 35) + '...' : item.name_en.substring(0, 10) + '...' : item.name_en}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  _handlePress = (value) => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        notfound: false,
        findTitle: value.tags,
        onActive: value.id
      }),
      () => {
        this.fetchVideo();
      },
    );
  };

  _renderItem = ({ item, index }) => (
    <View style={[styles.post, this.state.isTablet ? styles.post_orientation : this.state.orientation ? styles.post_orientation : null]}>
      <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={1} underlayColor="rgba(198, 198, 198, 0.1)">
        <LazyImage
          aspectRatio={item?.duration}
          shouldLoad={true}
          smallSource={{ uri: url + item?.thumbnail }}
          source={{ uri: url + item?.thumbnail }}
          providerName={item?.provider_name}
          videoNumber={item?.video_number}
        />
      </TouchableHighlight>
      <View style={styles.footer}>
        {/* <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Channel',
            {
              userid: item.id,
              username: item.username,
              phone_number: item.phone_number,
              image_path: item.image_path,
              image_cover_path: item.image_cover_path
            })} >
          <Image style={styles.avatar} source={item.image_path ? { uri: url_profile + item.image_path } : require('../../../assets/me1.png')} />
        </TouchableOpacity> */}
        <View style={styles.timeAgo}>
          <Text style={styles.title}>{item.title_playlist ? item?.title_playlist : item?.title}</Text>
          {/* <Text style={styles.titleTime}>{item.username ? `${item.username} - ` : null} {item.time_agos}</Text> */}
        </View>
      </View>
    </View>
  );
  render() {
    return (
      <React.Fragment>
        {!this.state.notfound ? <Loading /> : null}
        <SafeAreaView style={{ flex: 1, backgroundColor: theme.colors.themeWhite }}>
          {/* <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            showsVerticalScrollIndicator={false}> */}
          {/* {this._renderHeader()} */}
          <View style={[this.state.isTablet ? styles.flatlistLayout : this.state.orientation ? styles.flatlistLayout : null]}>
            <FlatList
              // style={{ backgroundColor: theme.colors.themeWhite }}
              numColumns={this.state.isTablet ? this.state.orientation ? 3 : 2 : 2}
              key={this.state.isTablet ? this.state.orientation ? "three" : "foure" : this.state.orientation ? "three" : "list"}
              data={this.state.datas}
              keyExtractor={(item, index) => index.toString()}
              onViewableItemsChanged={this.handleViewableChanged}
              viewabilityConfig={{
                viewAreaCoveragePercentThreshold: 15,
              }}
              showsVerticalScrollIndicator={false}
              onRefresh={this.handleRefresh}
              refreshing={this.state.refreshing}
              onEndReachedThreshold={0.1}
              onEndReached={this.fetchMoreData}
              ListFooterComponent={this.state.loading && this.state.datas.length > 10 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
              ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
              renderItem={this._renderItem}
              ListHeaderComponent={this._renderHeader}
            />
          </View>
          {/* </ScrollView> */}
        </SafeAreaView>
      </React.Fragment>
    );
  }
}

export default ScreenExplore;

const styles = StyleSheet.create({
  post: {
    flex: 1,
    paddingHorizontal: 7,
  },
  post_explore: {
    flex: 1,
  },
  post_orientation: {
    paddingHorizontal: 10,
    paddingTop: 15
  },
  flatlistLayout: {
    paddingHorizontal: 7,
  },
  footer: {
    paddingTop: 10,
    paddingBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 7,
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: 18,
    marginRight: 10,
  },
  title: {
    //fontWeight: '700',
    flexWrap: 'wrap',
    flex: 1
  },
  description: {
    padding: 15,
    lineHeight: 18
  },
  emptyContain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "center",
    paddingTop: 50
  },
  emptyListStyle: {
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
    color: '#fff'
  },
  timeAgo: {
    width: 0,
    flexGrow: 1,
  },
  titleTime: {
    color: '#aaaaaa',
    fontSize: 12
  },
  shortLayout: {
    backgroundColor: '#FFF',
    margin: 5,
  },
  titleHeaderShorts: {
    paddingTop: 7,
    paddingBottom: 7
  },
  layoutShorts: {
    paddingLeft: 10
  },
  btnRetry: {
    backgroundColor: theme.colors.themeRed,
    width: 100,
    borderRadius: 5,
  },
  exploreItemList: {
    height: 180
  },
  exploreLayout: {
    paddingTop: 13,
    paddingLeft: 7,
    paddingRight: 7,
  },
  exploreLayoutBtn: {
    borderRadius: 5,
    width: '100%',
    height: 45,
  },
  textExplore: {
    color: theme.colors.themBlackRgba,
    paddingRight: 10,
  },
  exploreIcon: {
    width: 25,
    height: 25,
  },
  iconlayout: {
    flexDirection: 'row',
    height: '100%',
    width: '100%',
    backgroundColor: '#EADEF9',
    borderRadius: 5,
  },
  iconleft: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1,
  },
  icontext: {
    flex: 2,
    justifyContent: "center",
  },
  hottestLayout: {
    marginTop: 10,
    marginRight: 5,
    marginLeft: 5,
    height: 35,
    borderRadius: 50,
    backgroundColor: 'rgba(0, 55, 43, 0.05)'
  },
  activeBtn: {
    backgroundColor: theme.colors.themeRed,
  },
  ativeBtnText: {
    color: theme.colors.themeWhite
  },
  hottestBtn: {
    alignItems: 'center',
    justifyContent: "center",
    width: '100%',
    height: '100%',
    paddingHorizontal: 15
  },
  hottestBtnText: {
    color: theme.colors.themBlackRgba
  },
  hotest_topic_container: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    marginTop: 15,
    marginBottom: 5
  },
  hotest_topicLeft: {
    flex: 1,
    color: theme.colors.themBlackRgba
  },
  hotest_topicRight: {
    flex: 2,
    textAlign: 'right',
    color: theme.colors.themeWhite,
    fontSize: 10
  },
  itemSlide: {
    width: '100%',
    height: 200,
  },
  itemSlideRotate: {
    width: '100%',
    height: 400,
  },
  imageContainerSlide: {
    flex: 1,
    marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
    backgroundColor: 'white',
    borderRadius: 8,
  },
  imageSlide: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'cover',
  },
  titleSlide: {
    position: "absolute",
    zIndex: 1,
    bottom: 15,
    left: 15,
    fontSize: 16,
    fontWeight: '900',
    color: '#fff'
  },
  container_view: {
    backgroundColor: theme.colors.themeWhite,
    paddingBottom: 15,
    marginTop: 7
  },
  btn: {
    alignItems: 'center',
    borderColor: 'rgba(0, 55, 43, 0.1)',
    borderRadius: 20,
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 7,
    backgroundColor: 'rgba(0, 55, 43, 0.05)'
  },
  btnActive: {
    alignItems: 'center',
    backgroundColor: theme.colors.themeRed,
    borderColor: theme.colors.themeRed,
    borderRadius: 20,
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 7,
  },
  viewChip: {
    padding: 5
  },
  textChip: {
    color: theme.colors.themeWhite
  },
  text: {
    color: theme.colors.themBlackRgba
  },
  slideshoCont: {
    borderRadius: 10,
    marginHorizontal: 10,
    borderColor: 'rgba(0, 55, 43, 0.1)',
    borderWidth: 1,
    overflow: 'hidden',
    height: WIDTH / 2,
    backgroundColor: theme.colors.themBlackWhite
  },
  readMore: {
    backgroundColor: theme.colors.themeRed,
    paddingVertical: 2,
    paddingHorizontal:10,
    borderRadius: 15,
  }
});