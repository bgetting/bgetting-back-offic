import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import axios from 'axios';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { API_URL, API_URL_FILE } from '../../Services/api';
import NotFound from '../../components/NotFound';
import Loading from '../../components/Loading';
import NetworkUtils from '../../network/NetworkUtills';
// THEME IMPORT
import * as theme from '../../constants/theme';
const urlImg = `${API_URL_FILE}/resources/images/`;

class ScreenExploreCateList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      page: 1,
      refreshing: false,
      error: null,
      loading: false,
      notfound: false,
      isNetwork: true,
      orientation: false,
    };
  }
  componentDidMount() {
    Dimensions.addEventListener('change', ({ window: { width, height } }) => {
      if (width < height) {
        this.setState({ orientation: false });
      } else {
        this.setState({ orientation: true });
      }
    });
    this.fetchVideo();
  }

  componentWillReceiveProps(nextProps) {
    for (const index in nextProps) {
      if (nextProps[index] !== this.props[index]) {
        this.fetchRefresh();
      }
    }
  }

  fetchVideo = async () => {
    const isConnected = await NetworkUtils.isNetworkAvailable();
    this.setState({ isNetwork: isConnected });
    const { page } = this.state;
    this.setState({ refreshing: false });
    this.setState({ loading: true });
    await axios({
      method: 'GET',
      url: `${API_URL}/api/auth-service/v1/video/explore?limit=20&page=${page}`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        this.setState({
          datas: this.state.datas.concat(response.data.data),
        });
        this.setState({ loading: false });
        this.setState({ notfound: true })
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  fetchMoreData = () => {
    this.setState(
      prevState => ({
        page: prevState.page + 1,
      }),
      () => {
        this.fetchVideo();
      },
    );

  };
  fetchRefresh = () => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        notfound: false
      }),
      () => {
        this.fetchVideo();
      },
    );

  };

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => { this.fetchRefresh() });
    this.setState({ refreshing: false });
  }

  Empty = () => {
    return (<View style={styles.emptyContain}>
      <NotFound
        source={require('../../../assets/404.png')}
        title='Connect to the internet'
        desc='Check your connection' />
      <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
        <Text style={styles.emptyListStyle} >
          RETRY
        </Text>
      </TouchableHighlight>
    </View>)
  }

  _renderBorder = () => (
    <View style={{
      backgroundColor: theme.colors.themBorderRgba,
      width: '100%',
      height: 1,
    }}></View>
  )

  _handlePress = (value) => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        notfound: false,
        findTitle: value.title,
      }),
      () => {
        this.fetchVideo();
      },
    );
  };

  _renderItem = ({ item, index }) => (
    <View style={styles.post} key={item.id}>
      <View style={styles.exploreLayoutBtn}>
        <TouchableOpacity
          activeOpacity={.7}
          onPress={() => this.props.navigation.navigate('ScreenExploreCategory', item)}>
          <View style={styles.iconlayout}>
            <View style={styles.iconleft}>
              <Image
                source={{ uri: `${urlImg}icon/${item.icon}` }}
                resizeMode="cover"
                style={styles.exploreIcon}
              />
            </View>
            <View style={styles.icontext}>
              <Text style={styles.textExplore}>
                {item.name_en.length >= 35 ? this.state.orientation ? item.name_en.length <= 50 ? item.name_en : item.name_en.substring(0, 50) + '...' : item.name_en.substring(0, 35) + '...' : item.name_en}
              </Text>              
            </View>
            <View style={styles.ExploreIconNext}>
             <AntDesign name="right" size={12} color={theme.colors.themBlack} />
            </View>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );

  render() {
    return (
      <React.Fragment>
        {!this.state.notfound ? <Loading /> : null}
        <FlatList
          style={{ backgroundColor: theme.colors.themeWhite }}
          numColumns={1}
          key="list"
          data={this.state.datas}
          keyExtractor={(item, index) => index.toString()}
          onViewableItemsChanged={this.handleViewableChanged}
          viewabilityConfig={{
            viewAreaCoveragePercentThreshold: 20,
          }}
          showsVerticalScrollIndicator={false}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReachedThreshold={0.1}
          onEndReached={this.fetchMoreData}
          ListFooterComponent={this.state.loading && this.state.datas.length > 15 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
          ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
          renderItem={this._renderItem}
          ItemSeparatorComponent={this._renderBorder}
        />

      </React.Fragment>
    );
  }
}
export default ScreenExploreCateList;

const styles = StyleSheet.create({
  post: {
    flex: 1,
  },
  btnRetry: {
    backgroundColor: theme.colors.themeRed,
    width: 100,
    borderRadius: 5,
  },
  exploreItemList: {
    height: 180
  },
  exploreLayout: {
    paddingTop: 13,
    paddingLeft: 7,
    paddingRight: 7,
  },
  imgExplore: {
    width: '100%',
    height: '100%',
    justifyContent: "center",
  },
  exploreLayoutBtn: {
    width: '100%',
    height: 45,
  },
  textExplore: {
    color: theme.colors.themBlackRgba,
    paddingRight: 10,
  },
  exploreIcon: {
    width: 25,
    height: 25,
  },
  iconlayout: {
    flexDirection: 'row',
    height: '100%',
    width: '100%',
  },
  iconleft: {
    alignItems: "center",
    justifyContent: "center",
    width: 50
  },
  icontext: {
    flex: 10,
    justifyContent: "center",
  },
  ExploreIconNext: {
    width: 25,
    justifyContent: "center",
  },
});