import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import LazyImage from '../../components/LazyImage';
import { API_URL, API_URL_FILE } from '../../Services/api';
import NotFound from '../../components/NotFound';
import Loading from '../../components/Loading';
import NetworkUtils from '../../network/NetworkUtills';
// THEME IMPORT
import * as theme from '../../constants/theme';
const url = `${API_URL_FILE}/resources/`;
const url_profile = `${API_URL_FILE}/resources-profile/`;

class ScreenExploreCate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      page: 1,
      refreshing: false,
      error: null,
      loading: false,
      notfound: false,
      isNetwork: true,
      orientation: false,
      findTitle: '',
      activeIndex: 0,
      isTablet: DeviceInfo.isTablet(),
    };
  }
  componentDidMount() {
    const { params } = this.props.route;
    this.props.navigation.setOptions({
      title: params.name_en.length >= 20 ? this.state.orientation ? params.name_en.length <= 35 ? params.name_en : params.name_en.substring(0, 35) + '...' : params.name_en.substring(0, 20) + '...' : params.name_en
    })
    Dimensions.addEventListener('change', ({ window: { width, height } }) => {
      if (width < height) {
        this.setState({ orientation: false });
      } else {
        this.setState({ orientation: true });
      }
    });
    this.fetchVideo();
  }

  componentWillReceiveProps(nextProps) {
    for (const index in nextProps) {
      if (nextProps[index] !== this.props[index]) {
        this.fetchRefresh();
      }
    }
  }

  fetchVideo = async () => {
    const { params } = this.props.route;
    const isConnected = await NetworkUtils.isNetworkAvailable();
    this.setState({ isNetwork: isConnected });
    const { page } = this.state;
    this.setState({ refreshing: false });
    this.setState({ loading: true });
    await axios({
      method: 'GET',
      url: `${API_URL}/api/auth-service/v1/video?limit=30&page=${page}${params.tags ? '&title_tags=' + params.tags : ''}`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        this.setState({
          datas: this.state.datas.concat(response.data.data),
        });
        this.setState({ loading: false });
        this.setState({ notfound: true })
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  fetchMoreData = () => {
    this.setState(
      prevState => ({
        page: prevState.page + 1,
      }),
      () => {
        this.fetchVideo();
      },
    );

  };
  fetchRefresh = () => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        notfound: false
      }),
      () => {
        this.fetchVideo();
      },
    );

  };

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => { this.fetchRefresh() });
    this.setState({ refreshing: false });
  }

  Empty = () => {
    return (<View style={styles.emptyContain}>
      <NotFound
        source={require('../../../assets/404.png')}
        title='Connect to the internet'
        desc='Check your connection' />
      <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
        <Text style={styles.emptyListStyle} >
          RETRY
        </Text>
      </TouchableHighlight>
    </View>)
  }

  _onPress(item) {
    const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
    const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
    const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
    const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
    const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
    const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
    const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
    const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
    const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
    const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
    const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

    this.props.navigation.navigate('Watch', {
      rownum: item.rownum,
      itemId: item.id,
      video_id: item.video_id,
      title: item.title,
      video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
      image_path: item.image_path,
      image_cover_path: item.image_cover_path,
      thumbnail: url + item.thumbnail,
      provider_name: item.provider_name,
      time_agos: item.time_agos,
      views: item.views,
      subscribe: item.subscribe,
      username: item.username,
      phone_number: item.phone_number,
      destination: item.destination,
      findUri: getUri,
      playlist_id: item.playlist_id,
      title_playlist: item.title_playlist,
      desc_playlist: item.desc_playlist,
      video_number: item.video_number,
      description: item.description,
      email_address: item.email_address
    });
    this.props.navigation.setOptions({ tabBarVisible: false });
  }

  _renderBorder = () => (
    <View style={{
      backgroundColor: theme.colors.themBorderRgba,
      width: '100%',
      height: 1,
      marginTop: 20
    }}></View>
  )

  _handlePress = (value) => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        notfound: false,
        findTitle: value.title,
      }),
      () => {
        this.fetchVideo();
      },
    );
  };

  _renderItem = ({ item, index }) => (
    <View style={[styles.post, this.state.isTablet ? styles.post_orientation : this.state.orientation ? styles.post_orientation : null]}>
      <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={1} underlayColor="rgba(198, 198, 198, 0.1)">
        <LazyImage
          aspectRatio={item?.duration}
          shouldLoad={true}
          smallSource={{ uri: url + item?.thumbnail }}
          source={{ uri: url + item?.thumbnail }}
          providerName={item?.provider_name}
          videoNumber={item?.video_number}
        />
      </TouchableHighlight>
      <View style={styles.footer}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Channel',
            {
              userid: item.id,
              username: item.username,
              phone_number: item.phone_number,
              image_path: item.image_path,
              image_cover_path: item.image_cover_path
            })} >
          <Image style={styles.avatar} source={item.image_path ? { uri: url_profile + item.image_path } : require('../../../assets/me1.png')} />
        </TouchableOpacity>
        <View style={styles.timeAgo}>
          <Text style={styles.title}>{item.title_playlist ? item?.title_playlist : item?.title}</Text>
          <Text style={styles.titleTime}>{item.username ? `${item.username} - ` : null} {item.time_agos}</Text>
        </View>
      </View>
    </View>
  );

  render() {
    return (
      <React.Fragment>
        <View style={styles.container_view}>
          {!this.state.notfound ? <Loading /> : null}
          <View style={[this.state.isTablet ? styles.flatlistLayout : this.state.orientation ? styles.flatlistLayout : null]}>
            <FlatList
              style={{ backgroundColor: theme.colors.themeWhite, paddingTop: 15 }}
              numColumns={this.state.isTablet ? this.state.orientation ? 4 : 3 : this.state.orientation ? 3 : 1}
              key={this.state.isTablet ? this.state.orientation ? "three" : "foure" : this.state.orientation ? "three" : "list"}
              data={this.state.datas}
              keyExtractor={(item, index) => index.toString()}
              onViewableItemsChanged={this.handleViewableChanged}
              viewabilityConfig={{
                viewAreaCoveragePercentThreshold: 30,
              }}
              showsVerticalScrollIndicator={false}
              onRefresh={this.handleRefresh}
              refreshing={this.state.refreshing}
              onEndReachedThreshold={0.1}
              onEndReached={this.fetchMoreData}
              ListFooterComponent={this.state.loading && this.state.datas.length > 15 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
              ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
              renderItem={this._renderItem}
              initialNumToRender={this.state.datas.length}
            />
          </View>
        </View>
      </React.Fragment>
    );
  }
}
export default ScreenExploreCate;

const styles = StyleSheet.create({
  container_view: {
    backgroundColor: theme.colors.themeWhite,
    flex: 1,    
  },
  post: {
    flex: 1,
    paddingHorizontal: 15,
  },
  post_orientation: {
    paddingHorizontal: 10,
    paddingTop: 15
  },
  flatlistLayout: {
    paddingHorizontal: 7,
  },
  footer: {
    paddingTop: 10,
    paddingBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 7,
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: 18,
    marginRight: 10,
  },
  title: {
    //fontWeight: '700',
    flexWrap: 'wrap',
    flex: 1
  },
  description: {
    padding: 15,
    lineHeight: 18
  },
  emptyContain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "center",
    paddingTop: 50
  },
  emptyListStyle: {
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
    color: '#fff'
  },
  timeAgo: {
    width: 0,
    flexGrow: 1,
  },
  titleTime: {
    color: '#aaaaaa',
    fontSize: 12
  },
  shortLayout: {
    backgroundColor: '#FFF',
    margin: 5,
  },
  titleHeaderShorts: {
    paddingTop: 7,
    paddingBottom: 7
  },
  layoutShorts: {
    paddingLeft: 10
  },
  btnRetry: {
    backgroundColor: theme.colors.themeRed,
    width: 100,
    borderRadius: 5,
  },

});