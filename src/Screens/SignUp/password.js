import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, ScrollView, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native';
import Button from '../../components/Button';
import * as storeService from '../../storage/service';
import Icon from 'react-native-vector-icons/Ionicons';
import { API_URL } from '../../Services/api';
import Loading from '../../components/Loading/loadingVideo';
// THEME IMPORT
import * as theme from '../../constants/theme';

function Password(props) {
    const [state, setState] = useState({
        confirmPassword: "",
        password: "",
        passwordalidate: false,
        submitted: false,

    });
    const [hidePass, setHidePass] = useState(true);
    const [secure, setSecure] = useState(true);
    const [phoneNumber, setPhoneNumber] = useState(props.route.params.userNameProvider);
    const [passwordconalidate, setPasswordconalidate] = useState(false);
    const [passwordLength, setPasswordLength] = useState(false);
    const [onLoading, setOnLoading] = useState(false);

    const OTP = [];
    const ref_input = [];
    ref_input[0] = useRef();
    ref_input[1] = useRef();
    ref_input[2] = useRef();
    ref_input[3] = useRef();

    const focusNext = (text, index, name) => {
        setState(prevState => ({
            ...prevState,
            [name]: text
        }));
        if (index === 0 && name === 'password') {
            if (text.length >= 6) {
                setPasswordLength(false);
            } else {
                setPasswordLength(true);
            }
        }
        OTP[index] = text;
    }

    const handlePasswordHideShow = () => {
        setHidePass(!hidePass);
        setSecure(!secure);
    }
    const handlePassword = async () => {
        setState(prevState => ({
            ...prevState,
            submitted: true,
        }))
        const { provider } = props.route.params;
        let confirmPassword = state.confirmPassword;
        let password = state.password;
        if (password.length >= 6) {
            if (confirmPassword === password) {
                setPasswordconalidate(false);
                const mydata = {
                    oauthId: "AUTH_ID",
                    oauthSecret: "OAUTH_SECRET",
                    provider: provider,
                    userNameProvider: phoneNumber,
                    password: password
                }
                const res = await fetch(
                    API_URL + '/api/user-service/v1/register',
                    {
                        method: 'post',
                        body: JSON.stringify(mydata),
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                        },
                    }
                );
                const responseJson = await res.json();
                if (responseJson.success) {
                    setOnLoading(false);
                    const getdata = {
                        oauthId: "AUTH_ID",
                        oauthSecret: "OAUTH_SECRET",
                        provider: provider,
                        username: phoneNumber,
                        password: password
                    }
                    const resApi = await fetch(
                        API_URL + '/api/user-service/v1/auth/login',
                        {
                            method: 'post',
                            body: JSON.stringify(getdata),
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                            },
                        }
                    );
                    const resJson = await resApi.json();
                    if (resJson.success) {
                        setOnLoading(false);
                        await storeService.localStore('loginState', JSON.stringify(resJson), null)
                        props.navigation.navigate('Home');
                    } else {
                        Alert.alert(
                            'Error',
                            'Something went wrong',
                            [
                                { text: 'OK', onPress: () => console.log('OK Pressed') },
                            ]
                        );
                    }
                } else {
                    Alert.alert(
                        'Error',
                        'Something went wrong',
                        [
                            { text: 'OK', onPress: () => console.log('OK Pressed') },
                        ]
                    );
                }
            } else {
                setPasswordconalidate(true);
            }
        }
    }

    return (
        <React.Fragment>
            {onLoading ? <View style={styles.isloading}><Loading /></View> : null}
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator={false}>
                    <View style={styles.mainViewTitle}><Text style={styles.mainTitle}>Sign up</Text></View>
                    <View style={styles.titleWrap}>
                        <Text style={styles.titleHeading}>Create Password</Text>
                        <Text style={styles.subTitle}>{`Your new password must to different from \n previous used passwords`}</Text>
                    </View>
                    <View style={styles.containerInput}>
                        <View style={styles.inputView}>
                            <Text style={styles.labelname}>Password</Text>
                            <TextInput
                                mode="flat"
                                selectionColor={theme.colors.themeRed}
                                underlineColorAndroid="transparent"
                                textAlign='center'
                                maxLength={10}
                                keyboardType='default'
                                secureTextEntry={secure}
                                style={styles.otpInputStyle}
                                autoFocus={true}
                                returnKeyType="next"
                                ref={ref_input[0]}
                                onChangeText={text => focusNext(text, 0, 'password')}
                            />
                            <TouchableOpacity
                                onPress={handlePasswordHideShow}
                                style={styles.iconpassword}>
                                <Icon
                                    name={hidePass ? 'eye-off-outline' : 'eye-outline'}
                                    size={20}
                                    color="grey"
                                />
                            </TouchableOpacity>
                        </View>
                        {passwordLength ? <Text style={styles.errorMs}>The password must be at least 6 characters long.</Text> : null}
                        <View style={styles.inputView}>
                            <Text style={styles.labelname}>Confirm Password</Text>
                            <TextInput
                                mode="flat"
                                selectionColor={theme.colors.themeRed}
                                underlineColorAndroid="transparent"
                                textAlign='center'
                                maxLength={10}
                                keyboardType='default'
                                secureTextEntry={true}
                                style={styles.otpInputStyle}
                                returnKeyType="next"
                                ref={ref_input[1]}
                                onChangeText={text => focusNext(text, 1, 'confirmPassword')}
                            />
                        </View>
                        {passwordconalidate ? <Text style={styles.errorMs}>Please make sure your passwords match.</Text> : null}
                    </View>
                    <View style={styles.btnVerify}>
                        <Button
                            title='Sign Up'
                            action={handlePassword} style={{ height: 45, }} />
                    </View>
                </ScrollView>
            </SafeAreaView >
        </React.Fragment>
    );
}
export default Password;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    errorMs: {
        marginTop: 10,
        color: theme.colors.themeRed,
    },
    labelname: {
        fontSize: 16,
        paddingVertical: 7,
    },
    iconpassword: {
        position: 'absolute',
        right: 12,
        bottom: 12
    },
    containerInput: {
        flexDirection: 'column',
        marginTop: 20,
        marginHorizontal: 15,
    },
    // otpInputStyle: {
    //     borderColor: theme.colors.themBorderRgba,
    //     borderWidth: 1,
    //     height: 50,
    //     width: 50,
    //     marginLeft: 10,
    //     marginRight: 10,
    //     fontSize: 24
    // },
    mainViewTitle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        textAlign: 'center',
    },
    mainTitle: {
        fontSize: 20,
        marginTop: 10,
        fontWeight: 'bold'
    },
    titleWrap: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
    },
    titleHeading: {
        fontSize: 22,
        marginTop: 25,
    },
    subTitle: {
        marginTop: 10,
        marginBottom: 10,
        color: theme.colors.themBlackRgba,
        textAlign: 'center',
    },
    btnVerify: {
        marginTop: 25,
        paddingHorizontal: 15,
        width: '100%',
    },
    inputView: {
        flex: 1
    },
    otpInputStyle: {
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        height: 45,
        width: '100%',
        paddingRight: 15,
        paddingLeft: 15,
        fontSize: 18,
        textAlign: 'left',
    },
    isloading: {
        flex: 1,
        backgroundColor: theme.colors.themBlackRgba,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
});