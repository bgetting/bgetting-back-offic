import React, { useState, useRef } from 'react';
import { View, Text, StyleSheet, ScrollView, SafeAreaView, TextInput, } from 'react-native';
// import axios from 'axios';
import Button from '../../components/Button';
import TimeCountdown from '../../components/TimeCountdown';
import { API_URL } from '../../Services/api';
// THEME IMPORT
import * as theme from '../../constants/theme';

function Verify(props) {
    const [state, setState] = useState({
        phone_number: "",
        code_one: '',
        code_two: '',
        code_three: '',
        code_four: '',
        submitted: false,

    });
    const [disabled, setDisabled] = useState(false);
    const [onLoading, setOnLoading] = useState(false);
    const [phoneNumber, setPhoneNumber] = useState(props.route.params.formattedValue);
    const [errors, setErrors] = useState(false);
    const OTP = [];
    const ref_input = [];
    ref_input[0] = useRef();
    ref_input[1] = useRef();
    ref_input[2] = useRef();
    ref_input[3] = useRef();

    const focusNext = (text, index, name) => {
        if (index < ref_input.length - 1 && text) {
            ref_input[index + 1].current.focus();
        }
        if (index === 3 & text.length >= 1) {
            setDisabled(true);
        } else {
            setDisabled(false);
        }
        if (index == ref_input.length - 1) {
            ref_input[index].current.blur();
        }
        setState(prevState => ({
            ...prevState,
            [name]: text
        }));

        OTP[index] = text;
    }
    const focusPrev = (key, index) => {
        if (key === "Backspace" && index !== 0) {
            ref_input[index - 1].current.focus();
        }
        if (index === 2) {
            setDisabled(false);
        }
    }

    const sendCodeToServer = async () => {
        setState(prevState => ({
            ...prevState,
            submitted: true,
        }))
        let codeEnteredByUser = state.code_one + state.code_two + state.code_three + state.code_four
        const { provider } = props.route.params;
        if (codeEnteredByUser.length === 4) {
            const data = {
                provider: provider,
                userNameProvider: phoneNumber,
            }
            const mydata = {
                phoneNumber: phoneNumber,
                code: codeEnteredByUser
            }
            const res = await fetch(
                API_URL + '/api/user-service/v1/register-verify/verify',
                {
                    method: 'post',
                    body: JSON.stringify(mydata),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        oauthid: 'AUTH_ID',
                        oauthsecret: 'OAUTH_SECRET',
                    },
                }
            );
            const responseJson = await res.json();
            if (responseJson.success) {
                setOnLoading(false);
                setErrors(false);
                props.navigation.navigate('Password', data);
            } else {
                setErrors(true);
            }
        }
    }

    return (
        <React.Fragment>
            {/* {onLoading ? <View style={styles.isloading}><Loading /></View> : null} */}
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator={false}>
                    <View style={styles.mainViewTitle}><Text style={styles.mainTitle}>Sign up</Text></View>
                    <View style={styles.titleWrap}>
                        <Text style={styles.titleHeading}>Verify BGetting</Text>
                        <Text style={styles.subTitle}>{`Enter the 4 digit code that you received on your mobile number or email.`}</Text>
                        <Text style={styles.subTitlenumber}>{phoneNumber}</Text>
                    </View>
                    <View style={styles.containerInput}>
                        <TextInput
                            mode="flat"
                            selectionColor={theme.colors.themeRed}
                            underlineColorAndroid="transparent"
                            textAlign='center'
                            maxLength={1}
                            keyboardType='numeric'
                            style={styles.otpInputStyle}
                            autoFocus={true}
                            returnKeyType="next"
                            ref={ref_input[0]}
                            onChangeText={text => focusNext(text, 0, 'code_one')}
                            onKeyPress={e => focusPrev(e.nativeEvent.key, 0)}
                        />
                        <TextInput
                            mode="flat"
                            selectionColor={theme.colors.themeRed}
                            underlineColorAndroid="transparent"
                            textAlign='center'
                            maxLength={1}
                            keyboardType='numeric'
                            style={styles.otpInputStyle}
                            ref={ref_input[1]}
                            onChangeText={text => focusNext(text, 1, 'code_two')}
                            onKeyPress={e => focusPrev(e.nativeEvent.key, 1)}
                        />
                        <TextInput
                            mode="flat"
                            selectionColor={theme.colors.themeRed}
                            underlineColorAndroid="transparent"
                            textAlign='center'
                            maxLength={1}
                            keyboardType='numeric'
                            style={styles.otpInputStyle}
                            ref={ref_input[2]}
                            onChangeText={text => focusNext(text, 2, 'code_three')}
                            onKeyPress={e => focusPrev(e.nativeEvent.key, 2)}
                        />
                        <TextInput
                            mode="flat"
                            selectionColor={theme.colors.themeRed}
                            underlineColorAndroid="transparent"
                            textAlign='center'
                            maxLength={1}
                            keyboardType='numeric'
                            style={styles.otpInputStyle}
                            ref={ref_input[3]}
                            onChangeText={text => focusNext(text, 3, 'code_four')}
                            onKeyPress={e => focusPrev(e.nativeEvent.key, 3)}
                        />
                        {errors ? <View style={styles.error}><Text style={styles.errortitle}>{`That code is wrong... \n Please verify your code and try again`}</Text></View> : null}
                    </View>

                    <View style={styles.receive}>
                        <Text style={styles.receivetitle}>Didn't receive BGetting</Text>
                        <Text style={styles.receivetime}>{TimeCountdown(300)}</Text>
                    </View>
                    <View style={styles.btnVerify}>
                        <Button
                            title='Verify & Proceed'
                            disabled={disabled ? false : true}
                            action={sendCodeToServer} style={{ height: 45, }} />
                    </View>
                </ScrollView>
            </SafeAreaView >
        </React.Fragment>
    );
}
export default Verify;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.themeWhite,
    },
    containerInput: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        marginTop: 20,
    },
    otpInputStyle: {
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        height: 50,
        width: 50,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 24
    },
    mainViewTitle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        textAlign: 'center',
    },
    mainTitle: {
        fontSize: 20,
        marginTop: 10,
        fontWeight: 'bold'
    },
    titleWrap: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
    },
    titleHeading: {
        fontSize: 22,
        marginTop: 25,
    },
    subTitle: {
        marginTop: 10,
        marginBottom: 10,
        paddingHorizontal: 25,
        color: theme.colors.themBlackRgba,
        textAlign: 'center',
    },
    subTitlenumber: {
        fontWeight: 'bold',
    },
    btnVerify: {
        padding: 25,
        width: '100%',
    },
    receive: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: "center",
        marginTop: 50
    },
    receivetitle: {
        color: theme.colors.themBlackRgba,
    },
    receivetime: {
        color: theme.colors.themeRed,
        marginLeft: 10
    },
    isloading: {
        flex: 1,
        backgroundColor: theme.colors.themBlackRgba,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    error: {
        position: 'absolute',
        bottom: -40,
        left: 15,
        right: 15,
    },
    errortitle: {
        color: theme.colors.themeRed,
        textAlign: 'center',
    },
});