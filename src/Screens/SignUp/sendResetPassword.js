import React, { useState, useRef, } from 'react';
import { View, TouchableHighlight, Text, StyleSheet, ScrollView, SafeAreaView, TextInput, Alert, TouchableOpacity } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import Button from '../../components/Button';
import { API_URL } from '../../Services/api';
import Loading from '../../components/Loading/loadingVideo';

// THEME IMPORT
import * as theme from '../../constants/theme';

function SendResetPassword(props) {

    const [state, setState] = useState({
        country: "+855",
        phone_number: "",
        mobilevalidate: false,
        submitted: false,

    });
    const [mobilevalidate, setMobilevalidate] = useState(false);
    const [errors, setErrors] = useState('');
    const [onLoading, setOnLoading] = useState(false);

    const OTP = [];
    const ref_input = [];
    ref_input[0] = useRef();
    ref_input[1] = useRef();
    ref_input[2] = useRef();
    ref_input[3] = useRef();

    const mobilevalidatename = (text) => {
        const reg = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
        if (reg.test(text) === false) {
            setMobilevalidate(false);
            return false;
        } else {
            setMobilevalidate(true);
            setErrors('');
            return true;
        }
    }
    const focusNext = (text, index, name) => {
        setState(prevState => ({
            ...prevState,
            [name]: text
        }));
        setMobilevalidate(mobilevalidatename(text));
        OTP[index] = text;
    }

    const sendCodeToServer = async () => {
        setState(prevState => ({
            ...prevState,
            submitted: true,
        }))
        let enteredUser = state.phone_number;
        const data = {
            value: enteredUser,
            provider: 2,
            formattedValue: enteredUser,
            mobilevalidate: mobilevalidate,
        }
        const mydata = {
            provider: 2,
            phoneNumber: enteredUser
        }
        console.log(mydata);
        if (enteredUser.length > 10) {
            setErrors('');
            setOnLoading(true);
            await axios({
                method: 'POST',
                url: `${API_URL}/api/user-service/v1/register-verify/send-reset`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                },
                data: JSON.stringify(mydata)
            })
                .then(response => {
                    setOnLoading(false);
                    props.navigation.navigate('VerifyResetPassword', data);
                })
                .catch(error => {
                    console.log(error);
                    setOnLoading(false);
                    Alert.alert(
                        null,
                        'This email has not been registered',
                        [{ text: 'OK', onPress: () => console.log('OK Pressed') }]
                    );
                });
        } else {
            setErrors('Please input email');
        }
    }
    
    return (

        <React.Fragment>
            {onLoading ? <View style={styles.isloading}><Loading /></View> : null}
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator={false} style={{ marginBottom: 100, }}>
                    <View style={styles.titleWrap}>
                        <Text style={styles.titleHeading}>Forgot Password</Text>
                        <Text style={styles.subTitle}>Enter your email and we will send you 4 digits code for verification.</Text>
                    </View>
                    <View>
                        <View style={styles.containerInput}>
                        <TouchableHighlight
                            // onPress={action}
                            style={[styles.btnCountry]}
                            activeOpacity={0.6}
                            underlayColor="#f97c7c">
                            <Text style={styles.text} >Email</Text>
                        </TouchableHighlight>
                        <View style={styles.inputView}>
                            <TextInput
                                mode="flat"
                                selectionColor={theme.colors.themeRed}
                                underlineColorAndroid="transparent"
                                textAlign='center'
                                maxLength={50}
                                keyboardType="email-address"
                                style={styles.otpInputStyle}
                                autoFocus={true}
                                returnKeyType="next"
                                ref={ref_input[0]}
                                onChangeText={text => focusNext(text, 0, 'phone_number')}
                            />                           
                        </View>
                        </View>
                        <View style={styles.btnVerify}>
                            <Button
                                title='Continue'
                                disabled={mobilevalidate ? false : true}
                                action={sendCodeToServer} style={{ height: 45, }} />
                        </View>
                    </View>

                </ScrollView>

            </SafeAreaView >
        </React.Fragment>
    );
}
export default SendResetPassword;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    containerInput: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 20,
        marginHorizontal: 15,
    },
    inputView: {
        flex: 1
    },
    otpInputStyle: {
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        height: 45,
        width: '100%',
        paddingRight: 15,
        paddingLeft: 15,
        fontSize: 18,
        textAlign: 'left',
    },
    btnCountry: {
        borderColor: theme.colors.themBorderRgba,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        height: 45,
        width: 90,
        justifyContent: "center",
        alignItems: 'center',
    },
    mainViewTitle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        textAlign: 'center',
    },
    mainTitle: {
        fontSize: 20,
        marginTop: 10,
        fontWeight: 'bold'
    },
    titleWrap: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        textAlign: 'center',
        paddingLeft: 30,
        paddingRight: 30,
    },
    titleHeading: {
        fontSize: 22,
        textAlign: 'center',
        alignSelf: 'center',
    },
    subTitle: {
        marginTop: 15,
        marginBottom: 15,
        color: theme.colors.themBlackRgba,
        textAlign: 'center',
        alignSelf: 'center',
    },
    btnVerify: {
        marginTop: 25,
        paddingHorizontal: 15,
        width: '100%',
    },
    error: {
        position: 'absolute',
        bottom: -20
    },
    errortitle: {
        color: theme.colors.themeRed,
    },
    isloading: {
        flex: 1,
        backgroundColor: theme.colors.themBlackRgba,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    selectforgotsend: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 15

    },
    selectforgotsendmobileleft: {
        flex: 1,
        paddingRight: 7,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    selectforgotsendmobile: {
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 4,
        backgroundColor: theme.colors.themBlackWhite,
        width: 60,
        height: 60,

    },
    selectforgotsendactive: {
        shadowColor: theme.colors.themeRed,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.84,
        elevation: 3,
    },
    selectforgotsendmobileright: {
        flex: 1,
        paddingLeft: 7,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    selectactive: {
        color: theme.colors.themeRed
    },

});