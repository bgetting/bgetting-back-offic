import React, { Component } from "react";
import {
    AppRegistry,
    StyleSheet,
    View,
    Dimensions,
} from "react-native";

import StatusBarTop from '../../components/StatusBar';
import * as theme from '../../constants/theme';
import PlayVideo from "../../components/Video/PlayVideo";
import PlayVideoFile from "../../components/Video/PlayVideoFile";
import PlayYoutubeVideo from "../../components/Video/PlayYoutubeVideo";
//import Loading from "../../components/Loading";
import Orientation from 'react-native-orientation';
// const WIDTH = Dimensions.get('window').width;
// const HEIGHT = Dimensions.get('window').height;

export default class Watch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orientation: 'PORTRAIT',
            fullscreen: false,
        };

        // Dimensions.addEventListener('change', ({ window: { width, height } }) => {
        //     if (width < height) {
        //         Orientation.unlockAllOrientations();
        //         this.setState({ orientation: 'PORTRAIT' });
        //     } else {
        //         this.setState({ orientation: 'LANDSCAPE' });
        //     }
        // });

    }

    componentDidMount() {
        Orientation.unlockAllOrientations();
        // Orientation.getOrientation((err, orientation) => {
        //     if (orientation == 'PORTRAIT') {
        //         this.setState({ orientation: 'PORTRAIT' });
        //     } else {
        //         this.setState({ orientation: 'LANDSCAPE' });
        //     }
        //     //console.log(`Current Device Orientation: ${orientation}`);
        // });
        Orientation.removeOrientationListener(this._orientationDidChange);
    }
    _orientationDidChange = (orientation) => {
        if (orientation === 'PORTRAIT') {
            this.setState({ orientation: 'PORTRAIT' });
        } else {
            this.setState({ orientation: 'LANDSCAPE' });
        }
      }
    componentWillUnmount() {
        Orientation.lockToPortrait();
        // Orientation.getOrientation((err, orientation) => {            
        //     if (orientation == 'PORTRAIT') {
        //         this.setState({ orientation: 'PORTRAIT' });
        //     } else {
        //         this.setState({ orientation: 'LANDSCAPE' });
        //     }
        //     //console.log(`Current Device Orientation: ${orientation}`);
        // });
        Orientation.removeOrientationListener(this._orientationDidChange);

    }

    _onPressFullScreen = () => {
        this.setState({ fullscreen: !this.state.fullscreen });
    }

    render() {

        const {
            video_patch,
            title,
            thumbnail,
            provider_name
        } = this.props.route.params;
        const {
            orientation,
            fullscreen } = this.state;

        return (
            <React.Fragment>
                <StatusBarTop hidden={orientation == 'PORTRAIT' ? false : true} barStyle='default' backgroundColor={theme.colors.themBlack} />
                <View style={styles.container}>
                    {provider_name === 'BGetting' ? <PlayVideoFile
                        navigation={this.props.navigation}
                        videoUri={video_patch}
                        orientation={orientation}
                        fullscreen={fullscreen}
                        provider_name={provider_name}
                        thumbnail={thumbnail}
                        source={video_patch}
                        resizeMode='contain'
                        muted={false}
                        volume={1}
                        rate={1}
                        {...this.props} />
                        :
                        <PlayYoutubeVideo
                            navigation={this.props.navigation}
                            videoUri={video_patch}
                            orientation={orientation}
                            fullscreen={fullscreen}
                            provider_name={provider_name}
                            {...this.props}
                        />}

                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

AppRegistry.registerComponent("Watch", () => Watch);