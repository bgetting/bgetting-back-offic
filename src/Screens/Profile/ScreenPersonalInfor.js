'use strict';
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    Alert, ToastAndroid, TextInput,
    TouchableHighlight
} from 'react-native';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';
import ImgToBase64 from 'react-native-image-base64';
import * as theme from '../../constants/theme';
import StatusBarTop from '../../components/StatusBar';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import * as storeService from '../../storage/service';
import Loading from '../../components/Loading/loadingVideo';
import BottomSheet from 'react-native-simple-bottom-sheet';
import { API_URL, API_URL_FILE } from '../../Services/api';

class ScreenPersonalInfor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            panelRef: null,
            loginAccess: {},
            openBottomSheet: false,
            nicknameText: "",
            isLoading: false
        }
    }
    componentDidMount() {
        this.getLocalStore();
    }

    componentWillReceiveProps(nextProps) {
        for (const index in nextProps) {
            if (nextProps[index] !== this.props[index]) {
                this.getLocalStore();
            }
        }
    }

    getLocalStore = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "", image: null, username: null } };
        this.setState({ loginAccess: localdataJs });
    }

    showToast = (ms) => {
        ToastAndroid.show(ms, ToastAndroid.SHORT);
    }

    refreshStore = async (value) => {
        const localdata = await storeService.getLocalStore('loginState');
        const items = localdata ? JSON.parse(localdata) : { user: { id: "", image: null, username: null } };
        items.user.username = value;
        await storeService.localStore('loginState', JSON.stringify(items), null);
        this.setState({ nicknameText: items.user.username });
        this.getLocalStore();
        this.setState({ isLoading: false });
    }

    refreshStoreImage = async (value) => {
        const localdata = await storeService.getLocalStore('loginState');
        const items = localdata ? JSON.parse(localdata) : { user: { id: "", image: null, username: null } };
        items.user.image = value;
        await storeService.localStore('loginState', JSON.stringify(items), null);
        this.getLocalStore();
        this.setState({ isLoading: false });
    }

    focusNext = (text) => {
        if (text) {
            this.setState({ nicknameText: text });
        }
    }

    submitValue = () => {
        this._onPressSave();
    }

    _onPressSave = async () => {
        this.state.panelRef.togglePanel();
        if (this.state.nicknameText) {
            this.setState({ isLoading: true });
            const localdata = await storeService.getLocalStore('loginState');
            const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
            if (localdataJs.success) {
                await axios({
                    method: 'POST',
                    url: `${API_URL}/api/user-service/v1/users/${localdataJs.user.id ? localdataJs.user.id : ''}`,
                    headers: {
                        'Content-Type': 'application/json',
                        oauthid: 'AUTH_ID',
                        oauthsecret: 'OAUTH_SECRET',
                        Authorization: `Bearer ${localdataJs.accessToken}`
                    },
                    data: {
                        username: this.state.nicknameText
                    }
                })
                    .then(response => {
                        //console.log(response.data);
                        this.refreshStore(this.state.nicknameText);
                        this.showToast("Save successfully !");
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        } else {
            Alert.alert(
                'Save Failed',
                'Sorry, your nickname is null. Please try again.',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ]
            );
        }
    }

    _onClickbottomsheet = (value) => {
        this.setState({ nicknameText: value });
        this.setState({ openBottomSheet: true });
        this.state.panelRef.togglePanel();
    }

    _onUploadImg = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            const imgBase64 = await ImgToBase64.getBase64String(res.uri)
            .then(base64String => {
                return 'data:image/png;base64,' + base64String;
            })
            .catch(err => doSomethingWith(err));
            if (imgBase64) {
                this.setState({ isLoading: true });
                const localdata = await storeService.getLocalStore('loginState');
                const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
                if (localdataJs.success) {
                    await axios({
                        method: 'POST',
                        url: `${API_URL_FILE}/api/user-service/v1/users/${localdataJs.user.id ? localdataJs.user.id : ''}`,
                        headers: {
                            'Content-Type': 'application/json',
                            oauthid: 'AUTH_ID',
                            oauthsecret: 'OAUTH_SECRET',
                            Authorization: `Bearer ${localdataJs.accessToken}`
                        },
                        data: {
                            image: imgBase64
                        }
                    })
                        .then(response => {
                            console.log(response.data);
                            this.setState({ isLoading: false });
                            this.refreshStoreImage(imgBase64);
                            this.showToast("Upload successfully !");
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }
            } else {
                Alert.alert(
                    'Save Failed',
                    'Sorry, upload profile is null. Please try again.',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ]
                );
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    ButtomSheetNickname = () => {
        return (
            <View style={styles.container_buttomsheet}>
                <View style={styles.buttomsheet_label}>
                    <Text>Nickname (Max 20):</Text>
                </View>
                <View>
                    <View style={styles.searchIcon}>
                        <Feather name='user' size={22} color="#8a8b8c" />
                    </View>
                    <TextInput
                        selectionColor={theme.colors.themeRed}
                        underlineColorAndroid="transparent"
                        maxLength={20}
                        keyboardType='default'
                        style={styles.otpInputStyle}
                        placeholder="Input Nickname"
                        defaultValue={this.state.nicknameText}
                        onChangeText={text => this.focusNext(text)}
                        returnKeyType="done"
                        onSubmitEditing={() => this.submitValue()}
                        placeholderTextColor="rgba(0,0,0,0.6)"
                    />
                </View>
                <TouchableHighlight onPress={() => this._onPressSave()}
                    style={[styles.btnClickContain, { backgroundColor: theme.colors.themeRed }]}
                    underlayColor={theme.colors.themeRedRgba}>
                    <Text style={[styles.btnText, { color: '#fff' }]}>Save</Text>
                </TouchableHighlight>
            </View>
        )
    }

    render() {
        const { loginAccess, isLoading } = this.state;
        return (
            <React.Fragment>
                {isLoading ? <Loading /> : null}
                <StatusBarTop hidden={false} barStyle='default' backgroundColor={theme.colors.themeRed} />
                <ScrollView
                    pagingEnabled={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    style={styles.container}
                >
                    <View style={styles.me_image}>
                        <TouchableOpacity onPress={() => this._onUploadImg()}>
                            <Image
                                source={loginAccess.user?.image ? { uri: loginAccess.user?.image } : require('../../../assets/me1.png')}
                                style={styles.me}
                                resizeMode="cover"
                            />
                            <View style={styles.me_image_camera}>
                                <EvilIcons name="camera" size={30} color="#fff" />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.me_label}>Nickname:</Text>
                    <TouchableHighlight
                        style={styles.me_btn}
                        onPress={() => this._onClickbottomsheet(loginAccess.user?.username ? loginAccess.user?.username : loginAccess.user?.phone_number)}
                        activeOpacity={0.6}
                        underlayColor={theme.colors.themBorderRgba}>
                        <View style={styles.my_profile}>
                            <Feather name='user' size={25} color={theme.colors.themBlackRgba} style={{ marginRight: 10 }} />
                            <View style={styles.you_name}>
                                <Text>{loginAccess.success ? loginAccess.user?.username ? loginAccess.user?.username : loginAccess.user?.phone_number : "Sing In"}</Text>
                            </View>
                            <View style={styles.iconnex_profile}>
                                <Entypo name='chevron-small-right' size={25} color={theme.colors.themBlackRgba} />
                            </View>

                        </View>
                    </TouchableHighlight>
                    <View style={styles.me_border}></View>
                    <Text style={styles.me_label}>Email:</Text>
                    <TouchableHighlight
                        style={styles.me_btn}
                        activeOpacity={0.6}
                        underlayColor={theme.colors.themBorderRgba}>
                        <View style={styles.my_profile}>
                            <Feather name='mail' size={25} color={theme.colors.themBlackRgba} style={{ marginRight: 10 }} />
                            <View style={styles.you_name}>
                                <Text>{loginAccess.success ? loginAccess.user?.email_address : null}</Text>
                            </View>
                        </View>
                    </TouchableHighlight>

                </ScrollView>
                <BottomSheet
                    isOpen={this.state.openBottomSheet}
                    sliderMinHeight={1}
                    sliderMaxHeight={Dimensions.get('window').height * 0.90}
                    ref={ref => this.state.panelRef = ref}
                    wrapperStyle={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        paddingLeft: 0,
                        paddingRight: 0,
                        borderWidth: 1,
                        borderColor: theme.colors.themBtnBorder
                    }}
                >
                    {this.ButtomSheetNickname}
                </BottomSheet>
            </React.Fragment>
        )
    };

}

export default ScreenPersonalInfor;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.themeWhite,
    },
    container_buttomsheet: {
        flex: 1,
        paddingHorizontal: 15
    },
    me_image: {
        alignItems: 'center',
        height: 200,
        justifyContent: 'center'
    },
    me: {
        width: 90,
        height: 90,
        borderRadius: 50,
    },
    me_image_camera: {
        position: "absolute",
        bottom: 0,
        right: 0,
        backgroundColor: theme.colors.themBlackRgba,
        borderRadius: 3
    },
    my_profile: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    me_btn: {
        marginVertical: 5
    },
    you_name: {
        fontWeight: '900',
        flex: 7,
    },
    iconnex_profile: {
        right: 0,
        flex: 1,
        alignItems: 'flex-end',
    },
    me_label: {
        fontWeight: "100",
        paddingHorizontal: 15,
        fontSize: 12,
        color: theme.colors.themBlackRgba,
    },
    me_border: {
        height: 1,
        backgroundColor: theme.colors.themBorderRgba,
        marginVertical: 15,
    },
    btnClickContain: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        padding: 10,
        marginVertical: 25,
        paddingHorizontal: 15,
    },
    btnIcon: {
        flex: 1,
        color: theme.colors.themBlack
    },
    btnText: {
        flex: 2,
        fontSize: 16,
        justifyContent: 'center',
        alignItems: 'center',
        color: theme.colors.themBlack,
    },
    buttomsheet_label: {
        paddingVertical: 5,
        marginBottom: 10,
    },
    otpInputStyle: {
        height: 45,
        width: '100%',
        paddingHorizontal: 50,
        paddingLeft: 40,
        paddingRight: 15,
        textAlign: 'left',
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        borderRadius: 5
    },
    searchIcon: {
        position: "absolute",
        marginTop: 10,
        paddingLeft: 10
    },

});
