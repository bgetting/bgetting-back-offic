import React, { useEffect, useState, useRef } from 'react';
import { Text, View } from 'react-native';
import {
    Tabs,
    TabScreen,
} from 'react-native-paper-tabs';
import TabChannel from './TabChannel';
import MyVideo from '../MyVideo';
import MyVideoChannelPlaylist from '../MyVideo/myVideoChannelPlaylist';

const ScreenChannel = (props) => {

    const [setTab, setSetTab] = useState(0);

    return (
        <Tabs
            defaultIndex={0} // default = 0
            // uppercase={false} // true/false | default=true | labels are uppercase
            // showTextLabel={false} // true/false | default=false (KEEP PROVIDING LABEL WE USE IT AS KEY INTERNALLY + SCREEN READERS)
            // iconPosition // leading, top | default=leading
            style={{ backgroundColor: '#fff' }} // works the same as AppBar in react-native-paper
            dark={false} // works the same as AppBar in react-native-paper
            // theme={} // works the same as AppBar in react-native-paper
            // mode="scrollable" // fixed, scrollable | default=fixed
            onChangeIndex={(newIndex) => { setSetTab(newIndex) }} // react on index change
            showLeadingSpace={true} //  (default=true) show leading space in scrollable tabs inside the header
        >
            <TabScreen label="Home" icon={undefined}>
                <TabChannel {...props} />
            </TabScreen>
            <TabScreen label="Videos" icon={undefined}>
                {setTab === 1 ? <MyVideo {...props} /> : <View style={{ backgroundColor: '#fff', flex: 1 }} />}
            </TabScreen>
            <TabScreen label="Playlists" icon={undefined}>
                {/* <View style={{ backgroundColor: '#fff', flex: 1 }} /> */}
                {setTab === 2 ? <MyVideoChannelPlaylist {...props} /> : <View style={{ backgroundColor: '#fff', flex: 1 }} />}
            </TabScreen>
        </Tabs>
    );
}


export default ScreenChannel;
