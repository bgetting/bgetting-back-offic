import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute, } from '@react-navigation/native';
//import Icon from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
//import AntDesign from 'react-native-vector-icons/AntDesign';
// THEME IMPORT
import * as theme from '../../constants/theme';
import ScreenProfile from './ScreenProfile';
import SignUp from '../SignUp';
import Verify from '../SignUp/verify';
import SignIn from '../SignIn';
import Password from '../SignUp/password';
import History from "../History";
import MyVideo from "../MyVideo";
import Feedback from '../Feedback';
import ScreenPersonalInfor from './ScreenPersonalInfor';
import ScreenChannel from './ScreenChannel';
import SendResetPassword from '../SignUp/sendResetPassword';
import VerifyResetPassword from '../SignUp/verifyResetPassword';
import PasswordReset from '../SignUp/passwordReset';
import * as storeService from '../../storage/service';
import Loading from '../../components/Loading';

const Stack = createStackNavigator();

function RootProfile({ navigation, route }) {

    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);

        if (routeName === "SignUp" || routeName === "Verify" || routeName === "Password"  || routeName === "SignIn"  || routeName === "PersonalInfor" || routeName === "SendResetPassword" || routeName === "VerifyResetPassword" || routeName === "PasswordReset") {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [navigation, route]);

    const [loginAccess, setLoginAccess] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            async function getLocalStore() {
                const localdata = await storeService.getLocalStore('loginState');
                const localdataJs = localdata ? JSON.parse(localdata) : {};                
                setLoginAccess(localdataJs);
                setIsLoading(true);
            }
            getLocalStore();
        });

        return unsubscribe;

    }, [navigation, route]);

    return (!isLoading ? <Loading /> :
        <React.Fragment>
            <Stack.Navigator
                name="Modal"
                headerMode="screen"
                initialRouteName="UploadSignInSignUp"
                screenOptions={{
                    gestureEnabled: true,
                    gestureDirection: 'horizontal',
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                }}
            >
                <Stack.Screen
                    name="MyProfile"
                    component={ScreenProfile}
                    options={({ route }) => ({
                        // headerBackTitleVisible: false,
                        // headerTransparent: true,
                        // headerShown: true,
                        // headerTitle: true,
                        headerTitle: "Me",
                        headerStyle: {
                            elevation: 0,
                            shadowOpacity: 0,
                            borderBottomWidth: 0,

                        },
                        headerTitleStyle: {
                            textAlign: 'center',
                            alignSelf: 'center',
                        },
                    })}
                />
                <Stack.Screen
                        name="Feedback"
                        component={Feedback}
                        options={({ route }) => ({
                            title: 'Help & feedback',
                            headerTintColor: theme.colors.themeWhite,
                            headerStyle: {
                                elevation: 10,
                                shadowOpacity: 10,
                                borderBottomWidth: 1,
                                backgroundColor: '#7425D1',
                            },
                        })}
                    />
                {!loginAccess.success? <React.Fragment><Stack.Screen
                    name="SignUp"
                    component={SignUp}
                    options={({ route }) => ({
                        headerBackTitleVisible: false,
                        headerTransparent: true,
                        headerShown: true,
                        headerTitle: true,
                        headerStyle: {
                            elevation: 0,
                            shadowOpacity: 0,
                            borderBottomWidth: 0,

                        },
                        headerTitleStyle: {
                            textAlign: 'center',
                            alignSelf: 'center',
                        },
                    })}
                />
                    <Stack.Screen
                        name="SignIn"
                        component={SignIn}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Verify"
                        component={Verify}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Password"
                        component={Password}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="SendResetPassword"
                        component={SendResetPassword}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="VerifyResetPassword"
                        component={VerifyResetPassword}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="PasswordReset"
                        component={PasswordReset}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                </React.Fragment> : 
                <React.Fragment>
                <Stack.Screen
                        name="MyVideo"
                        component={MyVideo}
                        options={({ route }) => ({
                            title: 'Your Video',
                            headerTintColor: theme.colors.themBlack,
                            headerStyle: {
                                elevation: 10,
                                shadowOpacity: 10,
                                borderBottomWidth: 1,
                            },
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />
                <Stack.Screen
                        name="History"
                        component={History}
                        options={({ route }) => ({
                            title: 'History',
                            headerTintColor: theme.colors.themBlack,
                            headerStyle: {
                                elevation: 10,
                                shadowOpacity: 10,
                                borderBottomWidth: 1,
                            },
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="PersonalInfor"
                        component={ScreenPersonalInfor}
                        options={({ route }) => ({
                            title: 'Personal Information',
                            headerTintColor: theme.colors.themeWhite,
                            headerStyle: {
                                elevation: 10,
                                shadowOpacity: 10,
                                borderBottomWidth: 1,
                                backgroundColor: theme.colors.themeRed,
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Channel"
                        component={ScreenChannel}
                        options={({ route }) => ({
                            title: 'Your channel',
                            headerTintColor: theme.colors.themBlack,
                            headerStyle: {
                                // elevation: 10,
                                // shadowOpacity: 10,
                                // borderBottomWidth: 1,
                            },
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />
                    </React.Fragment>}
            </Stack.Navigator>            
        </React.Fragment>

    );
}
export default RootProfile;
