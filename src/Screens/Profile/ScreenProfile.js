'use strict';
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    ScrollView,
    Dimensions,
    TouchableOpacity,
    Alert,
    TouchableHighlight,
    Modal,
    TextInput,
    FlatList,
    CheckBox,
    ActivityIndicator,
    ToastAndroid
} from 'react-native';
import axios from 'axios';
import { GoogleSignin } from '@react-native-community/google-signin';
import { Button, Dialog, List } from 'react-native-paper';
import * as theme from '../../constants/theme';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import * as storeService from '../../storage/service';
import LazyImageShort from '../../components/LazyImageShort';
import NewPlaylistItem from '../../components/NewPlaylistImage';
// import BottomSheet from 'react-native-simple-bottom-sheet';
import Loading from '../../components/Loading';
import LoadingVideo from '../../components/Loading/loadingVideo';
import NetworkUtils from '../../network/NetworkUtills';
import NotFound from '../../components/NotFound';
// import { ButtomSheetSignUp, ButtomSheetSignIn } from '../../components/BottomSheet';
import { API_URL, API_URL_FILE } from '../../Services/api';
const url = `${API_URL_FILE}/resources/`;
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;

class ScreenProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            panelRef: null,
            loginAccess: {},
            isLoading: false,
            body: true,
            recentData: [],
            isLoadingRecent: false,
            channelInfo: {},
            showModal: false,
            inputTitle: '',
            isDialogVisible: false,
            datas: [],
            loading: false,
            page: 1,
            notfound: false,
            isNetwork: true,
            error: null,
            orientation: false,
            getVideoId: [],
            addPlaylistData: [],
            vodeoTotal: '',
            lengJsonData: ''
        };
    }

    componentDidMount() {
        const unsubscribe = this.props.navigation.addListener('focus', () => {
            this.getLocalStore();
            this.fetchVideoRecent();
        });
        Dimensions.addEventListener('change', ({ window: { width, height } }) => {
            if (width < height) {
                this.setState({ orientation: false });
            } else {
                this.setState({ orientation: true });
            }
        });
        this.getLocalStore();
        this.fetchVideoRecent();
        return unsubscribe;
    }
    componentWillReceiveProps() {
        this.getLocalStore();
        this.fetchVideoRecent();
    }

    componentWillUnmount() {
        this.getLocalStore();
        this.fetchVideoRecent();
    }

    showToast = (ms) => {
        ToastAndroid.show(ms, ToastAndroid.SHORT);
    }

    fetchVideoPlaylist = async () => {
        const isConnected = await NetworkUtils.isNetworkAvailable();
        this.setState({ isNetwork: isConnected });
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        this.setState({ loading: true });
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/channel/${localdataJs.user.id + '?'}limit=10&page=${this.state.page}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET',
                Authorization: `Bearer ${localdataJs.accessToken}`
            },
        })
            .then(response => {
                this.setState({ datas: this.state.datas.concat(response.data.data) });
                this.setState({ lengJsonData: Object.keys(response.data.data).length });
                this.setState({ vodeoTotal: response.data.total });
                this.setState({ loading: false });
                this.setState({ notfound: true });
            })
            .catch(error => {
                this.setState({ error: error });
            });
    };

    handleChange = (id) => {
        let temp = this.state.datas.map((item) => {
            if (id === item.video_id) {
                if (!item.video_status) {
                    let filteredArray = this.state.getVideoId.filter(item => item !== id);
                    this.setState({ getVideoId: filteredArray });
                } else {
                    this.setState({ getVideoId: [...this.state.getVideoId, id] });
                }
                return { ...item, video_status: !item.video_status };
            }
            return item;
        });

        this.setState({ datas: temp });
    };
    fetchMoreDataPlaylist = () => {
        if (this.state.lengJsonData <= this.state.vodeoTotal && this.state.lengJsonData != 0) {
            this.setState(
                prevState => ({
                    page: prevState.page + 1,
                }),
                () => {
                    this.fetchVideoPlaylist();
                },
            );
        }
    }

    getLocalStore = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "", image: null, username: null } };
        this.setState({ loginAccess: localdataJs });
    }

    fetchVideoRecent = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        if (localdataJs.success) {
            await axios({
                method: 'GET',
                url: `${API_URL}/api/auth-service/v1/video/video-history/${localdataJs.user.id ? localdataJs.user.id : ''}?limit=15&page=1`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                    Authorization: `Bearer ${localdataJs.accessToken}`
                },
            })
                .then(response => {
                    this.setState({
                        recentData: response.data.data
                    });
                    this.setState({ isLoadingRecent: true });
                })
                .catch(error => {
                    console.log(error);
                });

            await axios({
                method: 'GET',
                url: `${API_URL}/api/auth-service/v1/video/channel_videos_info/${localdataJs.user.id ? localdataJs.user.id : ''}`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET'
                },
            })
                .then(response => {
                    this.setState({
                        channelInfo: response.data
                    });
                })
                .catch(error => {
                    console.log(error);
                });
            await axios({
                method: 'GET',
                url: `${API_URL}/api/auth-service/v1/video/video-playlist?limit=5&page=1${localdataJs.user.id ? '&user_id=' + localdataJs.user.id : ''}`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                },
            })
                .then(response => {
                    this.setState({
                        addPlaylistData: response.data.data
                    });
                })
                .catch(error => {
                    console.log(error);
                });
        }
    };

    showConfirmDialog = () => {
        return Alert.alert(
            "Sign Out",
            "Are you sure you want to sign out?",
            [
                {
                    text: "Yes",
                    onPress: () => this.singOut(),
                },
                { text: "No" },
            ]
        );
    };

    onClickPlaylist = () => {
        this.fetchVideoPlaylist();
        this.setState({ showModal: !this.state.showModal });
    }

    onCloseAddVideos = () => {
        this.setState({ showModal: !this.state.showModal });
        this.setState({ isDialogVisible: false });
        this.setState({ datas: [] });
        this.setState({ getVideoId: [] });
        this.setState({ page: 1 });
        this.setState({ inputTitle: '' });
    }

    confirmCloseModal = () => {
        return Alert.alert(
            "Discard playlist?",
            "Discarding this playlist will not save to playlist or videos you added.",
            [
                { text: "Cancel" },
                {
                    text: "Yes",
                    onPress: () => this.onCloseAddVideos()
                },
            ]
        );
    };

    focusNext = (text) => {
        this.setState({ inputTitle: text });
    }
    onSavePlaylistRefresh = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/video-playlist?limit=5&page=1${localdataJs.user.id ? '&user_id=' + localdataJs.user.id : ''}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET',
            },
        })
            .then(response => {
                this.setState({
                    addPlaylistData: response.data.data
                });
                this.onCloseAddVideos();
                this.setState({ loading: false });
                this.showToast("Add new playlist successfully !");
            })
            .catch(error => {
                console.log(error);
            });
    }
    onSave = async () => {
        const getDataSave = {
            user_id: this.state.loginAccess.user.id,
            video_id: this.state.getVideoId,
            title: this.state.inputTitle
        }
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        this.setState({ loading: true });
        if (localdataJs.success) {
            await axios({
                method: 'POST',
                url: `${API_URL}/api/auth-service/v1/video/add-playlist`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                    Authorization: `Bearer ${localdataJs.accessToken}`
                },
                data: getDataSave
            })
                .then(response => {
                    //console.log(response.data);
                    this.setState({ addPlaylistData: [] });
                    this.onSavePlaylistRefresh();
                })
                .catch(error => {
                    console.log(error);
                });

        }
    }

    onIsDialogVisible = () => {
        this.setState({ isDialogVisible: true });
    }

    singOut = async () => {
        await storeService.removeSingle('loginState');
        this.setState({ loginAccess: { user: { id: "", image: null, username: null } } });
        this.setState({ isLoading: true });
        try {
            await GoogleSignin.revokeAccess();
            await GoogleSignin.signOut();
        } catch (error) {
            console.error(error);
        }
        this.props.navigation.navigate("Home");
    }

    signIn = () => {
        this.setState({ body: true });
    };

    signUp = () => {
        this.setState({ body: false });
    };

    _onPressTo = (data) => {
        this.props.navigation.navigate(data, { type: data });
    }
    _onPressWatchVideo = (item) => {
        const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
        const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
        const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
        const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
        const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
        const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
        const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
        const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
        const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
        const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
        const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

        this.props.navigation.navigate('Watch', {
            rownum: item.rownum,
            itemId: item.id,
            video_id: item.video_id,
            title: item.title,
            video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
            image_path: item.image_path,
            image_cover_path: item.image_cover_path,
            provider_name: item.provider_name,
            time_agos: item.time_agos,
            views: item.views,
            subscribe: item.subscribe,
            username: item.username,
            phone_number: item.phone_number,
            destination: item.destination,
            findUri: getUri,
            playlist_id: item.playlist_id,
            title_playlist: item.title_playlist,
            desc_playlist: item.desc_playlist,
            video_number: item.video_number,
            description: item.description,
            email_address: item.email_address
        });
        this.props.navigation.setOptions({ tabBarVisible: false });
    }
    RenderBorder = ({ size, color }) => {
        return <View style={{ marginTop: 10, marginBottom: 10, borderBottomColor: color, borderBottomWidth: size }}></View>
    }

    RenderFollow = ({ name, children }) => {
        return <TouchableOpacity style={styles.myfllow}>
            <Text style={styles.number_view}>{children}</Text>
            <Text style={styles.number_text}>{name}</Text>
        </TouchableOpacity>
    }

    RenderTools = ({ name, children, onPress }) => {
        return <TouchableOpacity style={styles.myfllow} onPress={onPress}>
            <AntDesign name={name} size={25} color="#767577" />
            <Text style={styles.tools_text}>{children}</Text>
        </TouchableOpacity>
    }

    RenderVideoRecent = () => {
        return (
            <View >
                {/* <View style={{ marginTop: 10, marginBottom: 10, borderBottomColor: theme.colors.themBorderRgba, borderBottomWidth: 1 }}></View> */}
                <View style={styles.layoutShorts}>
                    <Text style={styles.titleHeaderShorts}>Recent</Text>
                    <ScrollView
                        horizontal={true}
                        pagingEnabled={false}
                        showsVerticalScrollIndicator={false}
                        showsHorizontalScrollIndicator={false}>
                        {!this.state.isLoadingRecent ? <View style={styles.shortLayout}>
                            <View style={styles.shortLayoutSize}></View>
                            <View style={styles.footer}>
                                <View style={styles.timeAgo}>
                                    <Text style={styles.title}></Text>
                                </View>
                            </View>
                        </View> : this.state.recentData && this.state.recentData.map((row, index) => {
                            return <View style={styles.shortLayout} key={index}>
                                <TouchableHighlight onPress={() => this._onPressWatchVideo(row)} activeOpacity={0.6}
                                    underlayColor="rgba(198, 198, 198, 0.1)">
                                    <LazyImageShort
                                        aspectRatio={row?.duration}
                                        shouldLoad={true}
                                        smallSource={{ uri: url + row?.thumbnail }}
                                        source={{ uri: url + row?.thumbnail }}
                                        providerName={row?.provider_name}
                                    />
                                </TouchableHighlight>
                                <View style={styles.footer}>
                                    <View style={styles.timeAgo}>
                                        <Text style={styles.title}>{row?.title}</Text>
                                    </View>
                                </View>
                            </View>
                        })}
                    </ScrollView>
                </View>
                <View style={{ marginTop: 10, marginBottom: 10, borderBottomColor: theme.colors.themBorderRgba, borderBottomWidth: 1 }}></View>
            </View>
        )
    }
    _renderItemPlaylist = ({ item, index }) => {
        return (
            <TouchableHighlight onPress={() => this.handleChange(item.video_id)} activeOpacity={1} underlayColor="rgba(198, 198, 198, 0.1)">
                <View style={styles.playlistVideo}>
                    <View style={this.state.orientation ? styles.playlistVideoThumOrientation : styles.playlistVideoThum}>
                        <Image
                            source={{ uri: url + item?.thumbnail }}
                            style={styles.playlistThumbnail}
                            resizeMode={item.provider_name === 'YouTube' ? "cover" : "stretch"}
                        />
                    </View>
                    <View style={styles.playlistText}>
                        <View style={styles.video_title_list}>
                            <Text>{item.title?.length >= 60 ? this.state.orientation ? item?.title : item?.title.substring(0, 60) + '...' : item?.title}</Text>
                            <Text style={styles.playlistSubText}>{item.views} <Entypo name='dot-single' size={10} /> {item.time_agos}</Text>
                        </View>
                        <View style={styles.action}>
                            <CheckBox
                                onValueChange={() => this.handleChange(item.video_id)}
                                value={!item.video_status}
                            />
                        </View>
                    </View>
                </View>
            </TouchableHighlight>
        )
    };
    Empty = () => {
        return (<View style={styles.emptyContain}>
            <NotFound
                source={require('../../../assets/404.png')}
                title='Connect to the internet'
                desc='Check your connection' />
            <TouchableHighlight onPress={() => this.fetchVideoPlaylist()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
                <Text style={styles.emptyListStyle} >
                    RETRY
                </Text>
            </TouchableHighlight>
        </View>)
    }
    renderFlatList = (renderData) => {
        return (
            <>
                {this.state.loading ? <LoadingVideo /> : null}
                <FlatList
                    numColumns={1}
                    data={renderData}
                    key="list"
                    keyExtractor={(item, index) => index.toString()}
                    viewabilityConfig={{
                        viewAreaCoveragePercentThreshold: 10,
                    }}
                    showsVerticalScrollIndicator={false}
                    renderItem={this._renderItemPlaylist}
                    ListFooterComponent={this.state.loading && this.state.datas.length > 7 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
                    ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
                    onEndReachedThreshold={0.1}
                    onEndReached={this.fetchMoreDataPlaylist}
                />
            </>
        );
    };

    NewPlaylistItem = () => {
        return (
            this.state.addPlaylistData.map(item => {
                return (
                    <View style={styles.styleAddVideo} key={item.playlist_id}>
                        <View style={styles.styleAddVideoImage}>
                            <TouchableHighlight onPress={() => this._onPressWatchVideo(item)} activeOpacity={0.6}
                                underlayColor="rgba(198, 198, 198, 0.1)">
                                <NewPlaylistItem
                                    aspectRatio={item?.duration}
                                    shouldLoad={true}
                                    smallSource={{ uri: url + item?.thumbnail }}
                                    source={{ uri: url + item?.thumbnail }}
                                    videoNumber={item.video_number}
                                />
                            </TouchableHighlight>
                        </View>
                        <View style={styles.styleAddVideoText}>
                            <Text>{item.title_playlist.length >= 60 ? this.state.orientation ? item.title_playlist : item.title_playlist.substring(0, 60) + '...' : item.title_playlist}</Text>
                            <Text style={styles.styleAddVideoUser}>{item.username} <Entypo name='dot-single' size={10} /> {item.video_number > 1 ? item.video_number + " Videos" : item.video_number + " Video"}</Text>
                        </View>
                    </View>
                );
            })

        )
    }

    render() {
        const { RenderFollow, RenderBorder, RenderTools, RenderVideoRecent } = this;
        return (
            <React.Fragment>
                <ScrollView
                    pagingEnabled={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    style={styles.container}
                >
                    <TouchableOpacity onPress={this.state.loginAccess.success && this.state.loginAccess.success ? () => this.props.navigation.navigate('PersonalInfor') : () => { this.props.navigation.navigate('SignIn', { type: 'SignIn' }); }}>
                        {/* {this.state.panelRef.togglePanel()} */}
                        <View style={styles.my_profile}>
                            {<Image
                                source={this.state.loginAccess.user?.image ? { uri: this.state.loginAccess.user?.image } : require('../../../assets/me1.png')}
                                style={styles.me}
                                resizeMode="cover"
                            />}

                            <View style={styles.you_name}>
                                <Text style={styles.title_name}>{this.state.loginAccess.success ? this.state.loginAccess.user?.username ? this.state.loginAccess.user?.username : this.state.loginAccess.user?.email_address : "Sing In"}</Text>
                            </View>
                            <View style={styles.iconnex_profile}>
                                <Entypo name='chevron-small-right' size={25} color={theme.colors.themBlackRgba} />
                            </View>

                        </View>
                    </TouchableOpacity>
                    {this.state.loginAccess.success && this.state.recentData && <RenderVideoRecent />}
                    <View style={styles.main_works}>
                        <RenderFollow name="Videos">{this.state.loginAccess.success && this.state.channelInfo.videos_number ? this.state.channelInfo.videos_number : 0}</RenderFollow>
                        <RenderFollow name="Subscribe">{this.state.loginAccess.success && this.state.channelInfo.subscribe_number ? this.state.channelInfo.subscribe_number : 0}</RenderFollow>
                        <RenderFollow name="Liked">{this.state.loginAccess.success && this.state.channelInfo.liked ? this.state.channelInfo.liked : 0}</RenderFollow>
                        <RenderFollow name="Unliked">{this.state.loginAccess.success && this.state.channelInfo.unliked ? this.state.channelInfo.unliked : 0}</RenderFollow>
                    </View>
                    <RenderBorder size={1} color={theme.colors.themBorderRgba} />
                    <View><Text style={styles.Mytitle}>Tools</Text></View>
                    <View style={styles.main_works}>
                        <RenderTools name="videocamera" onPress={() => !this.state.loginAccess.success ? null : this.props.navigation.navigate('MyVideo', {
                            userid: this.state.loginAccess.user?.id,
                            username: this.state.loginAccess.user.username,
                            phone_number: this.state.loginAccess.user.phone_number,
                            img_profile: this.state.loginAccess.user.img_profile,
                            image_cover: this.state.loginAccess.user.image_cover
                        })}>Your video</RenderTools>
                        <RenderTools name="hearto">My Friends</RenderTools>
                        <RenderTools name="save">Save</RenderTools>
                        <RenderTools name="clockcircleo" onPress={() => this.state.loginAccess.user?.id && this.state.loginAccess.success ? this.props.navigation.navigate('History') : null}>History</RenderTools>
                    </View>
                    <RenderBorder size={1} color={theme.colors.themBorderRgba} />
                    {!this.state.loginAccess.success ? null : <>
                        <Text style={styles.MainTitlePlaylist}>Playlists</Text>
                        <List.Item
                            title="New playlist"
                            titleStyle={{ color: theme.colors.themBlue }}
                            onPress={() => this.onClickPlaylist()}
                            left={props => <List.Icon {...this.props} icon="plus" color={theme.colors.themBlue} />}
                        />
                        {this.NewPlaylistItem()}
                        <RenderBorder size={1} color={theme.colors.themBorderRgba} />
                    </>}
                    {
                        !this.state.loginAccess.success ? null : <List.Item
                            title="Your channel"
                            onPress={() => this.props.navigation.navigate('Channel',
                                {
                                    userid: this.state.loginAccess.user.id,
                                    username: this.state.loginAccess.user?.username,
                                    phone_number: this.state.loginAccess.user?.phone_number,
                                    img_profile: this.state.loginAccess.user?.img_profile,
                                    image_cover: this.state.loginAccess.user?.image_cover
                                }
                            )}
                            left={props => <List.Icon {...this.props} icon="card-account-details-outline" />}
                        />
                    }
                    {/* <List.Item
                        title="Your channel"
                        onPress={() => this.state.loginAccess.success ? this.props.navigation.navigate('Channel',
                            {
                                userid: this.state.loginAccess.user.id,
                                username: this.state.loginAccess.user?.username,
                                phone_number: this.state.loginAccess.user?.phone_number,
                                img_profile: this.state.loginAccess.user?.img_profile,
                                image_cover: this.state.loginAccess.user?.image_cover
                            }
                        ) : null}
                        left={props => <List.Icon {...this.props} icon="card-account-details-outline" />}
                    /> */}
                    <List.Item
                        title="Help & feedback"
                        onPress={() => this.props.navigation.navigate('Feedback')}
                        left={props => <List.Icon {...this.props} icon="help-circle-outline" />}
                    />
                    {!this.state.loginAccess.success ? null : <List.Item
                        title="Sign out"
                        titleStyle={{ color: theme.colors.themeRed }}
                        onPress={() => this.showConfirmDialog()}
                        left={props => <List.Icon {...this.props} icon="logout" color={theme.colors.themeRedRgba} />}
                    />}

                </ScrollView>
                {/* {
                    !this.state.loginAccess.success ? <BottomSheet
                        isOpen={false}
                        sliderMinHeight={0}
                        sliderMaxHeight={Dimensions.get('window').height * 0.90}
                        // ref={ref => panelRef.current = ref}
                        ref={ref => this.state.panelRef = ref}
                        wrapperStyle={{
                            borderTopLeftRadius: 20,
                            borderTopRightRadius: 20,
                            paddingLeft: 0,
                            paddingRight: 0,
                            borderWidth: 1,
                            borderColor: theme.colors.themBtnBorder
                        }}
                    >
                        {this.state.body ? <ButtomSheetSignIn signUp={this.signUp} _onPressTo={() => this._onPressTo('SignIn')} /> : <ButtomSheetSignUp signIn={this.signIn} _onPressTo={() => this._onPressTo('SignUp')} />}
                    </BottomSheet> : null
                } */}
                <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.showModal}
                    onRequestClose={() => {
                        console.log('Modal has been closed.');
                    }}>
                    <View style={styles.modal}>
                        <View style={styles.modal_header}>
                            <View style={styles.modal_header_left}>
                                <TouchableHighlight onPress={() => this.confirmCloseModal()} activeOpacity={0.6} underlayColor="rgba(198, 198, 198, 0.3)" style={{ paddingHorizontal: 10, paddingVertical: 5 }}>
                                    <AntDesign name="close" size={30} color="#767577" />
                                </TouchableHighlight>
                                <Text style={styles.modal_header_title}>Add videos</Text>
                            </View>
                            <View style={styles.modal_header_right}>
                                {this.state.getVideoId.length === 0 ? <TouchableHighlight activeOpacity={0.6} underlayColor="rgba(198, 198, 198, 0.3)" style={{ paddingHorizontal: 10, paddingVertical: 5 }}>
                                    <Text style={{ color: theme.colors.themBlackRgba }}>NEXT</Text>
                                </TouchableHighlight> :
                                    <TouchableHighlight onPress={() => this.onIsDialogVisible()} activeOpacity={0.6} underlayColor="rgba(198, 198, 198, 0.3)" style={{ paddingHorizontal: 10, paddingVertical: 5 }}>
                                        <Text style={{ color: theme.colors.themBlue }}>NEXT</Text>
                                    </TouchableHighlight>
                                }
                            </View>
                        </View>
                        {/* {console.log(this.state.datas)} */}
                        <View style={{ flex: 1 }}>{this.renderFlatList(this.state.datas)}</View>

                        <Dialog
                            visible={this.state.isDialogVisible}
                            onDismiss={() => this.setState({ isDialogVisible: false })}>
                            <Dialog.Title>New playlist</Dialog.Title>
                            <Dialog.Content>
                                <TextInput
                                    selectionColor={theme.colors.themeRed}
                                    underlineColorAndroid="transparent"
                                    maxLength={250}
                                    keyboardType='default'
                                    style={styles.otpInputStyle}
                                    placeholder="Title"
                                    // defaultValue={this.state.textSearch}
                                    onChangeText={text => this.focusNext(text)}
                                    returnKeyType="next"
                                    // onSubmitEditing={() => this.submitSearch()}
                                    placeholderTextColor="rgba(0,0,0,0.6)"
                                />
                            </Dialog.Content>
                            <Dialog.Actions>
                                <Button onPress={() => this.setState({ isDialogVisible: false })}>Cancel</Button>
                                {this.state.inputTitle.length < 5 ? <Button><Text style={{ color: theme.colors.themBlackRgba }}>Create</Text></Button> :
                                    <Button onPress={() => this.onSave()}>Create</Button>
                                }
                            </Dialog.Actions>
                        </Dialog>

                    </View>
                </Modal>
            </React.Fragment >
        );

    }
}
export default ScreenProfile;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.themeWhite,
    },
    modal: {
        flex: 1,
    },
    modal_header: {
        flexDirection: 'row',
        height: 55,
        paddingHorizontal: 8,
        alignItems: 'center',
    },
    modal_header_left: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
    modal_header_right: {
        flex: 1,
        alignItems: 'flex-end',
    },
    modal_header_title: {
        paddingLeft: 15,
        fontSize: 18
    },
    header: {
        flexDirection: 'row',
        height: 45,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 15
    },
    header_text: {
        color: theme.colors.themeRed,
    },
    my_profile: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
        marginTop: 10,
        paddingHorizontal: 15
    },
    me: {
        width: 60,
        height: 60,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: 'rgba(226, 226, 226, 1)'
    },
    you_name: {
        left: 15,
        fontWeight: '900',
        flex: 7,
    },
    title_name: {
        fontSize: 17,
    },
    iconnex_profile: {
        right: 0,
        flex: 1,
        alignItems: 'flex-end',
    },
    myfllow: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    number_view: {
        color: theme.colors.themText,
        fontWeight: 'bold',
    },
    number_text: {
        color: theme.colors.themBlackRgba,
        fontSize: 12
    },
    main_works: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingVertical: 15,
    },
    tools_text: {
        marginTop: 10
    },
    Mytitle: {
        fontWeight: 'bold',
        paddingHorizontal: 15,
    },
    layoutShorts: {
        paddingLeft: 10
    },
    shortLayout: {
        backgroundColor: '#FFF',
        margin: 5,
    },
    titleHeaderShorts: {
        paddingTop: 7,
        paddingBottom: 7
    },
    timeAgo: {
        width: 0,
        flexGrow: 1,
    },
    title: {
        // fontWeight: '700',
        flexWrap: 'wrap',
        flex: 1
    },
    footer: {
        paddingTop: 10,
        paddingBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 7,
    },
    shortLayoutSize: {
        // backgroundColor: theme.colors.themBorderRgba,
        width: 130,
        height: 68
    },
    otpInputStyle: {
        borderBottomWidth: 1,
        borderColor: theme.colors.themBtnBorder
    },

    card: {
        padding: 10,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    modalView: {
        margin: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 5,
        justifyContent: 'space-between',
        alignItems: 'center',
        elevation: 5,
    },
    text: {
        textAlign: 'center',
        fontWeight: 'bold',
    },

    playlistVideo: {
        flexDirection: "row",
        height: WIDTH / 3.7,
        marginVertical: 10,
        paddingHorizontal: 15
    },
    playlistVideoThum: {
        flex: 2,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#2F3337",
        overflow: 'hidden',
    },
    playlistVideoThumOrientation: {
        flex: 0.7,
        borderRadius: 7,
        borderWidth: 1,
        borderColor: "#2F3337",
        overflow: 'hidden',
    },
    playlistThumbnail: {
        width: "100%",
        height: "100%",
        borderRadius: 7
    },
    playlistText: {
        flex: 2,
        paddingLeft: 15,
        flexDirection: 'row',
    },
    playlistSubText: {
        color: theme.colors.themBlackRgba,
        fontSize: 12
    },
    video_title_list: {
        flex: 5,
    },
    action: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    emptyContain: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        paddingTop: 50
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        color: '#fff'
    },
    btnRetry: {
        backgroundColor: theme.colors.themeRed,
        width: 100,
        borderRadius: 5,
    },
    MainTitlePlaylist: {
        paddingHorizontal: 15,
        fontWeight: 'bold',
    },
    styleAddVideo: {
        flexDirection: 'row',
        paddingVertical: 8
    },
    styleAddVideoImage: {
        paddingHorizontal: 15
    },
    styleAddVideoText: {
        flex: 1
    },
    styleAddVideoUser: {
        fontSize: 11,
        color: theme.colors.themBlackRgba
    }
});
