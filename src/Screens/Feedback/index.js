'use strict';
import React, { Component } from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    ScrollView,
    TouchableOpacity,
    Alert, ToastAndroid, TextInput,
    TouchableHighlight,
    Linking
} from 'react-native';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';
import ImgToBase64 from 'react-native-image-base64';
import * as theme from '../../constants/theme';
import StatusBarTop from '../../components/StatusBar';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';
import Loading from '../../components/Loading/loadingVideo';
import { API_URL } from '../../Services/api';

class Feedback extends Component {
    constructor(props) {
        super(props);
        this.state = {
            feedbackText: "",
            imageFeedback: null,
            isLoading: false,
        }
    }

    showToast = (ms) => {
        ToastAndroid.show(ms, ToastAndroid.SHORT);
    }

    focusNext = (text) => {
        if (text) {
            this.setState({ feedbackText: text });
        }
    }

    _onPressSave = async () => {
        if (this.state.feedbackText || this.state.imageFeedback) {
            this.setState({isLoading: true });
            await axios({
                method: 'POST',
                url: `${API_URL}/api/auth-service/v1/video/feedback`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                },
                data: {
                    feedback: this.state.feedbackText,
                    image: this.state.imageFeedback,
                }
            })
                .then(response => {
                    console.log(response.data);
                    this.setState({
                        feedbackText: "",
                        imageFeedback: null,
                        isLoading: false,
                    });
                    this.showToast("Send to successfully !");
                })
                .catch(error => {
                    console.log(error);
                });
        } else {
            Alert.alert(
                'Send To Failed',
                'Sorry, your send is null. Please try again.',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ]
            );
        }
    }


    _onUploadImg = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images],
            });
            const imgBase64 = await ImgToBase64.getBase64String(res.uri)
                .then(base64String => {
                    return 'data:image/png;base64,' + base64String;
                })
                .catch(err => doSomethingWith(err));
            if (imgBase64) {
                this.setState({ imageFeedback: imgBase64 });
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    render() {
        const { isLoading } = this.state;
        console.log(this.state.feedbackText);
        return (
            <React.Fragment>
                {isLoading ? <Loading /> : null}
                <StatusBarTop hidden={false} barStyle='default' backgroundColor={`#7425D1`} />
                <ScrollView
                    pagingEnabled={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    style={styles.container}
                >
                    <View style={styles.me_image}>
                        <TouchableOpacity onPress={() => this._onUploadImg()}>
                            <Image
                                source={this.state.imageFeedback ? { uri: this.state.imageFeedback } : require('../../../assets/img.png')}
                                style={styles.me}
                                resizeMode="cover"
                            />
                            <View style={styles.me_image_camera}>
                                <EvilIcons name="image" size={30} color="#fff" />
                            </View>
                        </TouchableOpacity>
                        <Text>Upload photo</Text>
                    </View>
                    <Text style={styles.me_label}>Enter your feedback here (Max 200):</Text>
                    <View style={styles.container_buttomsheet}>
                        <View style={styles.buttomsheet_label}></View>
                        <View>
                            <View style={styles.searchIcon}>
                                <Feather name='message-square' size={22} color="#8a8b8c" />
                            </View>
                            <TextInput
                                selectionColor={theme.colors.themeRed}
                                underlineColorAndroid="transparent"
                                maxLength={200}
                                keyboardType='default'
                                style={styles.otpInputStyle}
                                placeholder="Input feedback"
                                defaultValue={this.state.feedbackText}
                                onChangeText={text => this.focusNext(text)}
                                returnKeyType="done"
                                placeholderTextColor="rgba(0,0,0,0.6)"
                                multiline={true}
                            />
                        </View>
                        <TouchableHighlight onPress={() => this._onPressSave()}
                            style={[styles.btnClickContain, { backgroundColor: theme.colors.themeRed }]}
                            underlayColor={theme.colors.themeRedRgba}>
                            <Text style={[styles.btnText, { color: '#fff' }]}>Send to</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={styles.me_border}></View>
                    <Text style={styles.me_label_contact}>💰 ឧបត្ថម្ភគាំទ្រខ្ញុំបាទតាមរយៈ</Text>
                    <Text style={styles.me_label}>1- Account ABA : 001994568    Yun Sariem</Text>
                    <Text style={styles.me_label}>2- Account WING: 0976212876   Yun Sariem</Text>
                    <View style={styles.me_border}></View>
                    <Text style={styles.me_label_contact}>Contact</Text>
                    <Text style={styles.me_label}>Phone number:</Text>
                    <TouchableHighlight
                        style={styles.me_btn}
                        activeOpacity={0.6}
                        underlayColor={theme.colors.themBorderRgba}>
                        <View style={styles.my_profile}>
                            <Feather name='phone-call' size={25} color={theme.colors.themBlackRgba} style={{ marginRight: 10 }} />
                            <View style={styles.you_name}>
                                <Text>(+855) 097 621 287 6 / 096 621 287 5</Text>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <Text style={styles.me_label}>Email:</Text>
                    <TouchableHighlight
                        style={styles.me_btn}
                        activeOpacity={0.6}
                        underlayColor={theme.colors.themBorderRgba}
                        onPress={() => Linking.openURL('mailto:yunsariem@gmail.com?subject=User Feedback&body=Dear BGetting,')}
                        title="Feedback to BGetting">
                        <View style={styles.my_profile}>
                            <Fontisto name='email' size={25} color={theme.colors.themBlackRgba} style={{ marginRight: 10 }} />
                            <View style={styles.you_name}>
                                <Text style={{ color: 'blue', textDecorationLine: 'underline' }}>yunsariem@gmail.com</Text>
                            </View>
                        </View>
                    </TouchableHighlight>
                    <Text style={styles.me_label}>Our socials:</Text>

                    <View style={styles.my_profile}>
                        <TouchableOpacity
                            style={styles.me_btn}
                            onPress={() => Linking.openURL(`https://t.me/joinchat/Zqkc_S4sBfhiMzY1`)}>
                            <Fontisto name='telegram' size={25} color={theme.colors.themBlackRgba} style={{ marginRight: 25 }} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.me_btn}
                            onPress={() => Linking.openURL(`https://web.facebook.com/bgettingcom-519158148141521/?ref=pages_you_manage`)}>
                            <Fontisto name='facebook' size={25} color={theme.colors.themBlackRgba} style={{ marginRight: 10 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.me_border}></View>
                    <Text style={styles.me_label_contact}>About this app</Text>
                    <Text style={styles.me_label}>
                    BGetting គឺជាវេទិកាសិក្សាតាមប្រព័ន្ធអេឡិចត្រូនិចកំពូលនៅកម្ពុជាសម្រាប់សិស្សានុសិស្ស ឬអ្នកបង្កើតខ្លឹមសារ និងអ្នកដែលចង់ពង្រីកចំណេះដឹងរបស់ពួកគេ ។
                    BGetting ផ្តល់វេទិកាឌីជីថលសម្រាប់ស្ថាប័ន អ្នកជំនាញការបណ្តុះបណ្តាលវីដេអូ ដើម្បីចែករំលែកចំណេះដឹង គន្លឹះ និងល្បិច និងអ្នកជំនាញក្នុងទម្រង់ជាខ្លឹមសារវីដេអូ។ វេទិការបស់យើងបង្កើតមធ្យោបាយមួយដើម្បីចែកចាយចំណេះដឹងតាមរបៀបដែលមានប្រសិទ្ធភាព និងមានន័យ ។
                    </Text>
                    <View style={styles.me_border}></View>
                    <Text style={styles.me_label}>***យើងខ្ញុំសូមអធ្យាស្រ័យទៅដល់ម្ចាស់កម្មសិទ្ធិ Video ដែលខ្ញុំបាទយក់មកដាក់បង្ហាញ ។</Text>
                </ScrollView>
            </React.Fragment>
        )
    };

}

export default Feedback;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.themeWhite,
    },
    container_buttomsheet: {
        flex: 1,
        paddingHorizontal: 15
    },
    me_image: {
        alignItems: 'center',
        height: 200,
        justifyContent: 'center'
    },
    me: {
        width: 150,
        height: 150,
        borderRadius: 5,
        backgroundColor: theme.colors.themBtnBorder,
        borderColor: theme.colors.themBtnBorder,
        borderWidth: 1,
    },
    me_image_camera: {
        position: "absolute",
        alignItems: 'center',
        justifyContent: 'center',
        width: 150,
        height: 40,
        bottom: 0,
        right: 0,
        backgroundColor: theme.colors.themBlackRgba,
    },
    my_profile: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    me_btn: {
        marginVertical: 5
    },
    you_name: {
        fontWeight: '900',
        flex: 7,
    },
    iconnex_profile: {
        right: 0,
        flex: 1,
        alignItems: 'flex-end',
    },
    me_label: {
        fontWeight: "100",
        paddingHorizontal: 15,
        fontSize: 12,
        color: theme.colors.themBlackRgba,
    },
    me_border: {
        height: 1,
        backgroundColor: theme.colors.themBorderRgba,
        marginVertical: 15,
    },
    btnClickContain: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        padding: 10,
        marginVertical: 25,
        paddingHorizontal: 15,
    },
    btnIcon: {
        flex: 1,
        color: theme.colors.themBlack
    },
    btnText: {
        flex: 2,
        fontSize: 16,
        justifyContent: 'center',
        alignItems: 'center',
        color: theme.colors.themBlack,
    },
    buttomsheet_label: {
        paddingVertical: 5,
        marginBottom: 10,
    },
    otpInputStyle: {
        width: '100%',
        paddingHorizontal: 50,
        paddingLeft: 40,
        paddingRight: 15,
        textAlign: 'left',
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        borderRadius: 5,
        height: 150,
        justifyContent: "flex-start",
        textAlignVertical: 'top'
    },
    searchIcon: {
        position: "absolute",
        marginTop: 10,
        paddingLeft: 10
    },
    me_label_contact: {
        paddingHorizontal: 15,
        marginBottom: 15,
        fontWeight: 'bold'
    }

});
