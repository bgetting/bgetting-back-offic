import React, { Component } from 'react';
// import all the components we are going to use
import {
    // ActivityIndicator,
    // TouchableHighlight,
    Text,
    StyleSheet,
    View,
    Dimensions,
    // FlatList,
    // TextInput,
    // TouchableOpacity,
    // Image
} from 'react-native';
// import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
// import Ionicons from 'react-native-vector-icons/Ionicons';
// import EvilIcons from 'react-native-vector-icons/EvilIcons';
import DeviceInfo from 'react-native-device-info';
// import LazyImage from '../../components/LazyImage';
// import NetworkUtils from '../../network/NetworkUtills';
// import * as storeService from '../../storage/service';
import * as theme from '../../constants/theme';
import { API_URL } from '../../Services/api';
// const url = `${API_URL}/resources/`;
// import NotFound from '../../components/NotFound';
import Loading from '../../components/Loading';

class ScreenNotification extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            datasList: [],
            onLoading: false,
            orientation: false,
            page: 1,
            refreshing: false,
            error: null,
            loading: false,
            notfound: false,
            isNetwork: true,
            searchClear: false,
            textSearch: "",
            submitTextSearch: "",
            dropdown: "",
            onBack: false,
            onautoFocus: true,
            orientation: false,
            isTablet: DeviceInfo.isTablet()
        };
    }

    componentDidMount() {
        Dimensions.addEventListener('change', ({ window: { width, height } }) => {
            if (width < height) {
                this.setState({ orientation: false });
            } else {
                this.setState({ orientation: true });
            }
        });
    }


    render() {
        return (
            <React.Fragment>
                {this.state.loading ? <Loading /> : null}
                <View style={styles.container}>
                    <View style={styles.containerNofi}>
                        <MaterialIcons name="notifications-none" color={theme.colors.themBorderRgba} size={100} />
                        <Text style={styles.containerNofiText}>No notification history</Text>
                    </View>
                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerNofi: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    containerNofiText: {
        color: theme.colors.themBorderRgba  
    }
});

export default ScreenNotification;