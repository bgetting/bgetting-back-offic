import React, { useEffect, useState } from 'react';
import { View, TouchableHighlight, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';


// THEME IMPORT
import * as theme from '../../constants/theme';
import Watch from '../Watch/Watch';
import ScreenChannel from '../Profile/ScreenChannel';
import ScreenNotification from './ScreenNotification';
import Loading from '../../components/Loading';
import * as storeService from '../../storage/service';
import Search from '../Search';

const Stack = createStackNavigator();

function RootNotification({ navigation, route }) {

    const [isLoading, setIsLoading] = useState(false);
    const [loginAccess, setLoginAccess] = useState({});

    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === "Watch" || routeName === "SignUp" || routeName === "Verify" || routeName === "Password" || routeName === "SignIn"  || routeName === "Search") {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }

        const unsubscribe = navigation.addListener('focus', () => {
            setIsLoading(true);
        });

        return unsubscribe;
    }, [navigation, route]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            async function getLocalStore() {
                const localdata = await storeService.getLocalStore('loginState');
                const localdataJs = localdata ? JSON.parse(localdata) : {};
                setLoginAccess(localdataJs);
                setIsLoading(true);
            }
            getLocalStore();
        });

        return unsubscribe;

    }, [navigation, route]);

    return (
        !isLoading ? <Loading /> :
            <React.Fragment>
                <Stack.Navigator
                    name="Modal"
                    headerMode="screen"
                    initialRouteName="ScreenNotification"
                    screenOptions={{
                        gestureEnabled: true,
                        gestureDirection: 'horizontal',
                        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                    }}
                >
                    <Stack.Screen
                        name="ScreenNotification"
                        component={ScreenNotification}
                        options={({ route }) => ({
                            title: 'Notifications',
                            headerTitle: true,
                            headerTintColor: theme.colors.themBlack,
                            headerStyle: {
                                elevation: 10,
                                shadowOpacity: 10,
                                borderBottomWidth: 1,
                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                            headerLeft: () => (
                                <View style={{ marginLeft: 15, flexDirection: 'row', }}>
                                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>Notifications</Text>
                                </View>
                            ),
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />                    
                    <Stack.Screen
                        name="Search"
                        component={Search}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: false,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,
                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Watch"
                        component={Watch}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTitle: false,
                            headerTransparent: true,
                            headerShown: false,
                        })}
                    />
                    <Stack.Screen
                        name="Channel"
                        component={ScreenChannel}
                        options={({ route }) => ({
                            title: 'Your channel',
                            headerTintColor: theme.colors.themBlack,
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />                    
                </Stack.Navigator>
            </React.Fragment>
    );
}
export default RootNotification;