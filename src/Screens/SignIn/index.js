import React, { useState, useRef, useEffect } from 'react';
import { View, Text, StyleSheet, ScrollView, SafeAreaView, TextInput, TouchableOpacity, Alert } from 'react-native';
import {
    GoogleSignin,
    GoogleSigninButton,
    statusCodes,
} from '@react-native-community/google-signin';
import Icon from 'react-native-vector-icons/Ionicons';
import * as storeService from '../../storage/service';
import Button from '../../components/Button';
import { API_URL } from '../../Services/api';
import Loading from '../../components/Loading/loadingVideo';

// THEME IMPORT
import * as theme from '../../constants/theme';

function SignIn(props) {

    const [state, setState] = useState({
        country: "+855",
        phone_number: "",
        password: "",
        mobilevalidate: false,
        submitted: false,

    });
    const [onLoading, setOnLoading] = useState(false);

    const [hidePass, setHidePass] = useState(true);
    const [secure, setSecure] = useState(true);

    const [loggedIn, setloggedIn] = useState(false);
    //const [userInfo, setuserInfo] = useState([]);

    const OTP = [];
    const ref_input = [];
    ref_input[0] = useRef();
    ref_input[1] = useRef();
    ref_input[2] = useRef();
    ref_input[3] = useRef();

    useEffect(() => {
        GoogleSignin.configure({
            scopes: ['email'], // what API you want to access on behalf of the user, default is email and profile
            webClientId: '897287676328-90k94g6a0mgii6rkenrqdshn58h79sic.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
            offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
            forceCodeForRefreshToken: true,
        });
    }, []);

    const _signIn = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            setloggedIn(true);
            const mydata = {
                oauthId: "AUTH_ID",
                oauthSecret: "OAUTH_SECRET",
                provider: 4,
                username: userInfo.user.email,
                password: userInfo.user.id
            }
            setOnLoading(true);
            const resFetch = await fetch(
                API_URL + '/api/user-service/v1/auth/login/googlesignin',
                {
                    method: 'post',
                    body: JSON.stringify(mydata),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                }
            );
            const resJson = await resFetch.json();
            if (resJson.success) {
                setOnLoading(false);
                await storeService.localStore('loginState', JSON.stringify(resJson), null);
                Alert.alert(
                    null,
                    'Sing In successfully !',
                    [{ text: 'OK', onPress: () => props.navigation.navigate('Home') }]
                );
            } else {
                setOnLoading(false);
                Alert.alert(
                    'Sing In Failed',
                    'Sorry, your user or password is incorrect. Please try again.',
                    [{ text: 'OK', onPress: () => console.log('OK Pressed') }]
                );
            }

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
                //Alert.alert(null, 'Cancel', [{ text: 'OK', onPress: () => console.log('OK Pressed') }]);
            } else if (error.code === statusCodes.IN_PROGRESS) {
                Alert.alert(null, 'Signin in progress', [{ text: 'OK', onPress: () => console.log('OK Pressed') }]);
                // operation (f.e. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                Alert.alert(null, 'PLAY_SERVICES_NOT_AVAILABLE', [{ text: 'OK', onPress: () => console.log('OK Pressed') }]);
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    const focusNext = (text, index, name) => {
        setState(prevState => ({
            ...prevState,
            [name]: text
        }));
        OTP[index] = text;
    }

    const handlePasswordHideShow = () => {
        setHidePass(!hidePass);
        setSecure(!secure);
    }

    const sendCodeToServer = async () => {
        setState(prevState => ({
            ...prevState,
            submitted: true,
        }))
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let enteredUser = state.phone_number;

        let pass = state.password;
        let zero = enteredUser.substring(1, 0);
        let number = enteredUser.substring(1, 10);
        let username = '';
        if (reg.test(enteredUser) === true) {
            username = enteredUser;
        } else {
            if (zero == 0) {
                username = state.country + number;
            } else {
                username = state.country + enteredUser;
            }
        }
        const mydata = {
            oauthId: "AUTH_ID",
            oauthSecret: "OAUTH_SECRET",
            provider: reg.test(enteredUser) ? 2 : 1,
            username: username.toLowerCase(),
            password: pass
        }
        if (username.length > 5 && pass.length >= 6) {
            setOnLoading(true);
            const res = await fetch(
                API_URL + '/api/user-service/v1/auth/login',
                {
                    method: 'post',
                    body: JSON.stringify(mydata),
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                }
            );
            const responseJson = await res.json();
            // console.log('test', JSON.stringify(responseJson));
            if (responseJson.success) {
                setOnLoading(false);
                await storeService.localStore('loginState', JSON.stringify(responseJson), null);
                Alert.alert(
                    null,
                    'Sing In successfully !',
                    [
                        { text: 'OK', onPress: () => props.navigation.navigate('Home') },
                    ]
                );
            } else {
                setOnLoading(false);
                Alert.alert(
                    'Sing In Failed',
                    'Sorry, your user or password is incorrect. Please try again.',
                    [
                        { text: 'OK', onPress: () => console.log('OK Pressed') },
                    ]
                );
            }
        } else {
            Alert.alert(
                'Sing In Failed',
                'Invalid user credentials',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ]
            );
        }

    }
    return (

        <React.Fragment>
            {onLoading ? <View style={styles.isloading}><Loading /></View> : null}
            <SafeAreaView style={styles.container}>
                <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator={false}>
                    <View style={styles.mainViewTitle}><Text style={styles.mainTitle}>Sign In</Text></View>
                    <View style={styles.titleWrap}>
                        <Text style={styles.titleHeading}>Welcome to BGetting</Text>
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.labelname}>Email</Text>
                        <View style={styles.containerInput}>
                            <View style={styles.inputViewuser}>
                                <TextInput
                                    mode="flat"
                                    selectionColor={theme.colors.themeRed}
                                    underlineColorAndroid="transparent"
                                    textAlign='center'
                                    maxLength={50}
                                    keyboardType='default'
                                    style={styles.otpInputStyle}
                                    autoFocus={true}
                                    returnKeyType="next"
                                    ref={ref_input[0]}
                                    onChangeText={text => focusNext(text, 0, 'phone_number')}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={styles.inputView}>
                        <Text style={styles.labelname}>Password</Text>
                        <TextInput
                            mode="flat"
                            selectionColor={theme.colors.themeRed}
                            underlineColorAndroid="transparent"
                            textAlign='center'
                            maxLength={10}
                            keyboardType='default'
                            secureTextEntry={secure}
                            style={styles.otpInputStyle}
                            returnKeyType="done"
                            ref={ref_input[1]}
                            onChangeText={text => focusNext(text, 1, 'password')}
                        />
                        <TouchableOpacity
                            onPress={handlePasswordHideShow}
                            style={styles.iconpassword}>
                            <Icon
                                name={hidePass ? 'eye-off-outline' : 'eye-outline'}
                                size={20}
                                color="grey"
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btnSignin}>
                        <Button
                            title='Sign In'
                            // disabled={mobilevalidate ? false : true}
                            action={sendCodeToServer} style={{ height: 45, }} />
                    </View>
                    <View style={styles.forgetPassword}>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('SendResetPassword')}>
                            <Text style={styles.forgetPasswordText}>Forget Password ?</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.btnSigninemail}>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('SignUp')}>
                            <Text style={styles.SigninemailText}>Don't have on account ? <Text style={styles.SigninemailTextsub}>Sign Up</Text></Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.sectionContainer}>
                        <GoogleSigninButton
                            style={{ width: '100%', height: 50 }}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Dark}
                            onPress={_signIn}
                        />
                    </View>
                </ScrollView>
            </SafeAreaView >
        </React.Fragment>
    );
}
export default SignIn;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    containerInput: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
    },
    inputViewuser: {
        flex: 1,
    },
    inputView: {
        flex: 1,
        paddingHorizontal: 15,
    },
    labelname: {
        fontSize: 14,
        paddingVertical: 7,
    },
    otpInputStyle: {
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        height: 45,
        width: '100%',
        paddingHorizontal: 15,
        fontSize: 18,
        textAlign: 'left',
        borderRadius: 7
    },
    btnCountry: {
        borderColor: theme.colors.themBorderRgba,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        height: 45,
        width: 90,
        justifyContent: "center",
        alignItems: 'center',
    },
    mainViewTitle: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        textAlign: 'center',
    },
    mainTitle: {
        fontSize: 20,
        marginTop: 10,
        fontWeight: 'bold'
    },
    titleWrap: {
        flex: 1,
        alignItems: 'center',
        justifyContent: "center",
        textAlign: 'center',
        paddingLeft: 30,
        paddingRight: 30,
        marginBottom: 30,
    },
    titleHeading: {
        fontSize: 22,
        marginTop: 25,
        textAlign: 'center',
        alignSelf: 'center',
    },
    subTitle: {
        marginTop: 15,
        marginBottom: 15,
        color: theme.colors.themBlackRgba,
        textAlign: 'center',
        alignSelf: 'center',
    },
    btnSigninemail: {
        padding: 15,
        textAlign: 'center',
        alignSelf: 'center'
    },
    SigninemailText: {
        color: theme.colors.themBlackRgba,
    },
    SigninemailTextsub: {
        color: theme.colors.themBlack,
        textDecorationLine: 'underline',
    },
    error: {
        position: 'absolute',
        bottom: -20
    },
    errortitle: {
        color: theme.colors.themeRed,
    },
    isloading: {
        flex: 1,
        backgroundColor: theme.colors.themBlackRgba,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    iconpassword: {
        position: 'absolute',
        right: 27,
        bottom: 12
    },
    forgetPassword: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        paddingHorizontal: 15
    },
    forgetPasswordText: {
        color: '#FF3E3F'
    },
    btnSignin: {
        paddingHorizontal: 15,
        marginTop: 35
    },
    sectionContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 12
    }
});