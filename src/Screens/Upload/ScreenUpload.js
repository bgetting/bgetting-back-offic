import React, { useState, useEffect } from 'react';
// Import core components
import {
    Alert,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    ScrollView,
    SafeAreaView,
    TextInput,
    ToastAndroid
} from 'react-native';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import Clipboard from '@react-native-community/clipboard'
import { getYoutubeMeta } from 'react-native-youtube-iframe';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import TagInput from 'react-native-tags-input';
import * as storeService from '../../storage/service';
import TagInput from '../../components/Tags';
import Loading from '../../components/Loading/loadingVideo';
// THEME IMPORT
import * as theme from '../../constants/theme';
import Button from '../../components/Button';
import { API_URL_FILE } from '../../Services/api';

const ScreenUpload = (props) => {
    const [youtubeMeta, setYoutubeMeta] = useState({});
    const [youtubeId, setYoutubeId] = useState('');
    const [youtubeUri, setYoutubeUri] = useState(false);
    const [onProgress, setonProgress] = useState(false);
    const [onLoading, setOnLoading] = useState(false);
    const [btnOnLoading, setBtnOnLoading] = useState(false);
    const [descriptionText, setDescriptionText] = useState('');
    const [desCount, setDesCount] = useState(0);
    const [tageCount, setTageCount] = useState(0);
    const [isTablet, setIsTablet] = useState(false);
    const [tags, setTags] = useState({
        tag: '',
        tagsArray: []
    });

    useEffect(() => {
        setIsTablet(DeviceInfo.isTablet());
    }, []);

    const readFromClipboard = async () => {
        const clipboardContent = await Clipboard.getString();
        var pieces = clipboardContent.split(/[\s/]+/);
        var checkUri = pieces[pieces.length - 1];
        setYoutubeUri(true);
        getYoutubeMeta(checkUri).then(meta => {
            setYoutubeMeta(meta);
            setonProgress(true);
            setYoutubeId(checkUri);
        }).catch(error => {
            setYoutubeUri(false);
            console.log('error', error);
            Alert.alert(
                'Error',
                `Paste Youtube link wrong. Please try again.`,
                [{ text: 'OK', onPress: () => console.log('OK Pressed') }]
            );
        });
    };

    const showToast = (ms) => {
        ToastAndroid.show(ms, ToastAndroid.SHORT);
    }

    const clearState = () => {
        setYoutubeMeta({});
        setYoutubeId('');
        setDescriptionText('');
        setYoutubeUri(false);
        setOnLoading(false);
        setBtnOnLoading(false);
        setonProgress(false);
        setTags({
            tag: '',
            tagsArray: []
        })
    }

    const updateTagState = (state) => {
        setTageCount(state.tag.length);
        setTags(state);
    };

    const focusNext = (text) => {
        if (text) {
            setDescriptionText(text);
            setDesCount(text.length);
        }
    }

    const sendCodeToServer = async () => {
        const localdata = await storeService.getLocalStore('loginState');
        const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
        if (localdataJs.success && youtubeMeta.thumbnail_url && tags.tagsArray.length > 0) {
            if (tags.tagsArray.length <= 10) {
                const mydata = {
                    youtube_videoId: youtubeId,
                    title: youtubeMeta.title,
                    userId: localdataJs.user.id,
                    tags: tags.tagsArray,
                    provider_name: "YouTube",
                    thumbnail_url: youtubeMeta.thumbnail_url,
                    description: descriptionText
                }
                setBtnOnLoading(true);
                await axios({
                    method: 'POST',
                    url: `${API_URL_FILE}/api/auth-service/v1/youTubeVideo`,
                    headers: {
                        'Content-Type': 'application/json',
                        oauthid: 'AUTH_ID',
                        oauthsecret: 'OAUTH_SECRET',
                        'Authorization': 'Bearer ' + localdataJs.accessToken,
                    },
                    data: mydata
                })
                    .then(response => {
                        setOnLoading(false);
                        clearState();
                        setBtnOnLoading(false);
                        setDescriptionText('');
                        showToast("Upload video successfully !");
                    })
                    .catch((error) => {
                        // Error
                        if (error.response) {
                            showToast("Duplicate video");
                            setOnLoading(false);
                            setBtnOnLoading(false);
                        } else {
                            Alert.alert(
                                'Upload Error',
                                error,
                                [{ text: 'OK', onPress: () => console.log('OK Pressed') }]
                            );
                        }

                    });
            } else {
                Alert.alert(
                    'Upload Failed',
                    'Sorry, your tags is limit, max tags 10. Please try again.',
                    [{ text: 'OK', onPress: () => console.log('OK Pressed') }]
                );
            }
        } else {
            Alert.alert(
                'Upload Failed',
                'Sorry, your video or tags is incorrect. Please try again.',
                [{ text: 'OK', onPress: () => console.log('OK Pressed') }]
            );
        }
    }

    const _onRemoveYoutubLink = () => {
        setYoutubeUri(false);
        setYoutubeMeta({});
        setonProgress(false);
    }

    return (
        onLoading ? <View style={styles.isloading}><Loading /></View> :
            <React.Fragment>
                <SafeAreaView style={styles.container}>
                    <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator={false}>
                        {!youtubeUri ? (<View style={styles.viewinputcart}>
                            <View style={isTablet ? styles.viewinputRotate : styles.viewinput}>
                                <TouchableOpacity style={styles.btnPaste} onPress={readFromClipboard}>
                                    <Text style={{ color: "#fff" }}> Paste</Text>
                                </TouchableOpacity>
                                <View style={styles.imageContainer}>
                                    <Image
                                        resizeMode='contain'
                                        style={styles.logoyoutub}
                                        source={require('../../../assets/youtube.png')} />
                                </View>
                            </View></View>) :
                            onProgress ? (<View style={styles.viewinputcart}>
                                <View style={isTablet ? styles.viewinputRotate : styles.viewinput}>
                                    <TouchableOpacity style={styles.deletelink} onPress={_onRemoveYoutubLink}>
                                        <AntDesign name='delete' size={20} color="#fff" />
                                    </TouchableOpacity>
                                    <Image
                                        resizeMode='cover'
                                        style={styles.youtubThumbnail}
                                        source={{ uri: youtubeMeta.thumbnail_url }} />
                                </View>
                                <Text style={styles.title}>{youtubeMeta.title}</Text>
                            </View>) : <View style={styles.viewinputcart}><View style={isTablet ? styles.viewinputRotate : styles.viewinput}><Loading /></View></View>}

                        <View style={{ paddingHorizontal: 15 }}>
                            <TagInput
                                updateState={updateTagState}
                                maxLength={100}
                                tags={tags}
                                label={`Tag ${tageCount}/100`}
                                labelStyle={{ color: theme.colors.themBlackRgba }}
                                leftElement={<FontAwesome5 name={'tags'} type={'material-community'} size={16} color={theme.colors.themBlackRgba} />}
                                containerStyle={{ width: '100%' }}
                                inputContainerStyle={[styles.textInput]}
                                inputStyle={{ color: theme.colors.themBlackRgba }}
                                autoCorrect={false}
                                tagStyle={styles.tag}
                                tagTextStyle={styles.tagText}
                                keysForTag={', '} />
                            <Text style={styles.upload_label}>Description ({`${desCount}/1000`})</Text>
                            <View style={styles.container_buttomsheet}>
                                <View style={styles.searchIcon}>
                                    <Feather name='file-text' size={22} color={theme.colors.themBlackRgba} />
                                </View>
                                <TextInput
                                    selectionColor={theme.colors.themeRed}
                                    underlineColorAndroid="transparent"
                                    maxLength={1000}
                                    keyboardType='default'
                                    style={styles.otpInputStyle}
                                    onChangeText={text => focusNext(text)}
                                    returnKeyType="done"
                                    placeholderTextColor="rgba(0,0,0,0.6)"
                                    multiline={true}
                                />
                            </View>
                            <View style={styles.btnCheckLink}>
                                <Button
                                    title='UPLOAD'
                                    // disabled={true}
                                    activityIndicator={btnOnLoading ? true : false}
                                    action={sendCodeToServer} style={{ height: 45, }} />
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </React.Fragment>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // paddingHorizontal: 15,
        backgroundColor: theme.colors.themeWhite,
    },
    otpInputStyle: {
        width: '100%',
        paddingHorizontal: 50,
        paddingLeft: 40,
        paddingRight: 15,
        textAlign: 'left',
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
        borderRadius: 5,
        height: 150,
        justifyContent: "flex-start",
        textAlignVertical: 'top'
    },
    viewinputcart: {
        backgroundColor: '#fff',
    },
    viewinput: {
        flex: 1,
        flexDirection: 'column',
        height: 180,
        // backgroundColor: '#32475B',
    },
    viewinputRotate: {
        flex: 1,
        flexDirection: 'column',
        height: 380,
        backgroundColor: '#32475B',
    },
    title: {
        padding: 10,
        fontSize: 16,
        fontWeight: '900'
    },
    imageContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: "flex-end",
        bottom: 10,
        right: 10,
        position: 'absolute',
    },
    logoyoutub: {
        height: 20,
        width: 50,
    },
    youtubThumbnail: {
        width: '100%',
        height: '100%',
    },
    btnCheckLink: {
        marginTop: 10,
    },
    deletelink: {
        right: 10,
        position: 'absolute',
        top: 10,
        zIndex: 999,
        backgroundColor: theme.colors.themBlackRgba,
        height: 30,
        width: 30,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3
    },
    btnPaste: {
        right: 10,
        position: 'absolute',
        top: 10,
        zIndex: 999,
        backgroundColor: theme.colors.themBlackRgba,
        height: 30,
        width: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 3
    },
    textInput: {
        height: 45,
        marginTop: 8,
        borderRadius: 3,
        paddingHorizontal: 5,
        backgroundColor: '#fff',
        borderColor: theme.colors.themBorderRgba,
        borderWidth: 1,
    },
    tag: {
        backgroundColor: '#fff',
    },
    tagText: {
        color: theme.colors.themBlackRgba,
    },
    isloading: {
        flex: 1,
        backgroundColor: theme.colors.themBlackRgba,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        zIndex: 1,
        left: 0,
        right: 0,
        bottom: 0,
        top: 0
    },
    container_buttomsheet: {
        flex: 1,
        marginTop: 5,
        backgroundColor: theme.colors.themeWhite
    },
    searchIcon: {
        position: "absolute",
        marginTop: 10,
        paddingLeft: 10
    },
    upload_label: {
        color: theme.colors.themBlackRgba,
        fontSize: 15,
        marginTop: 5
    }
});

export default ScreenUpload;