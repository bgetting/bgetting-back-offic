import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    SafeAreaView,
} from 'react-native';

// THEME IMPORT
import * as theme from '../../constants/theme';

const UploadHelp = (props) => {
    const B = (props) => <Text style={{fontWeight: 'bold'}}>{props.children}</Text>

    return (        
            <React.Fragment>
                <SafeAreaView style={styles.container}>
                    <ScrollView contentInsetAdjustmentBehavior="automatic" showsVerticalScrollIndicator={false}>
                       <View style={styles.header_title}>
                           <Text style={styles.header_title_text}>How to copy YouTube link</Text>
                       </View>
                       <View>
                       <Text style={styles.label}><B>Step1:</B> Select a video and click Share.</Text>
                       <Image
                            source={require('../../../assets/help1.png')}
                            style={styles.imgHelp}
                            resizeMode="contain"
                        />
                       </View>
                       <View>
                       <Text style={styles.label}><B>Step2:</B> Click "Copy link" and click buttom Paste.</Text>
                       <Image
                            source={require('../../../assets/help2.png')}
                            style={styles.imgHelp}
                            resizeMode="contain"
                        />
                       </View>
                    </ScrollView>
                </SafeAreaView>
            </React.Fragment>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.themeWhite,
        paddingHorizontal: 15,
    },
    header_title:{
        alignItems: 'center',
        marginTop: 25,
    },
    header_title_text: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    label: {
        marginVertical: 20
    },
    imgHelp: {
        width: '100%',
        borderWidth:1,
        borderColor: theme.colors.themBorderRgba,
    }
});

export default UploadHelp;