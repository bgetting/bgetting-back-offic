import React, { useState, useEffect, useRef } from 'react';
import { View, TouchableHighlight, Text, StyleSheet, Image, Dimensions } from 'react-native';
// import AntDesign from 'react-native-vector-icons/AntDesign';
// import { createStackNavigator } from '@react-navigation/stack';
// import BottomSheet from 'react-native-simple-bottom-sheet';
// import * as storeService from '../../storage/service';
import Button from '../../components/Button';

// import { ButtomSheetSignUp, ButtomSheetSignIn } from '../../components/BottomSheet';

// THEME IMPORT
// import * as theme from '../../constants/theme';

function ScreenUploadSignInSignUp(props) {
    const panelRef = useRef(null);
    const [body, setBody] = useState(true);

    // const signIn = () => {
    //     setBody(true);
    // };

    // const signUp = () => {
    //     setBody(false);
    // };

    // const _onPressTo = (data) => {
    //     props.navigation.navigate(data, { type: data });
    // }

    return (
        <React.Fragment>
            <React.Fragment>
                <View style={styles.container}>
                    <Image
                        source={require('../../../assets/profile.png')}
                        style={styles.img}
                        resizeMode='stretch' />
                    <Text style={styles.title} >Sign in or sing up for an account</Text>
                    <Button
                        title='Sign In or Sign Up'
                        action={() => {props.navigation.navigate('SignIn');}}
                        // action={() => { panelRef.current.togglePanel() }}
                        style={{ marginTop: 20, width: 200, }} />
                </View>
                {/* <BottomSheet
                    isOpen={false}
                    sliderMinHeight={0}
                    sliderMaxHeight={Dimensions.get('window').height * 0.90}
                    ref={ref => panelRef.current = ref}
                    wrapperStyle={{
                        borderTopLeftRadius: 20,
                        borderTopRightRadius: 20,
                        paddingLeft: 0,
                        paddingRight: 0,
                        borderWidth: 1,
                        borderColor: theme.colors.themBtnBorder
                    }}
                >
                    {body ? <ButtomSheetSignIn signUp={signUp} _onPressTo={() => _onPressTo('SignIn')} /> :
                        <ButtomSheetSignUp signIn={signIn} _onPressTo={() => _onPressTo('SignUp')} />}
                </BottomSheet> */}
            </React.Fragment>
        </React.Fragment>
    );
}
export default ScreenUploadSignInSignUp;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: "center",
    },
    img: {
        width: 120,
        height: 120,
    },
    title: {
        marginTop: 20
    },
});