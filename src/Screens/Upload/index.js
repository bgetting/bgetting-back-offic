import React, { useState, useEffect } from 'react';
import { View, TouchableOpacity } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute, } from '@react-navigation/native';

// THEME IMPORT
import * as theme from '../../constants/theme';
import ScreenUpload from './ScreenUpload';
import ScreenUploadSignInSignUp from './ScreenUploadSignInSignUp';
import SignUp from '../SignUp';
import SignIn from '../SignIn';
import Verify from '../SignUp/verify';
import Password from '../SignUp/password';
import UploadHelp from './help';
import SendResetPassword from '../SignUp/sendResetPassword';
import VerifyResetPassword from '../SignUp/verifyResetPassword';
import PasswordReset from '../SignUp/passwordReset';
import * as storeService from '../../storage/service';
import Loading from '../../components/Loading/loadingVideo';

const Stack = createStackNavigator();

function RootUpload({ navigation, route }) {

    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);

        if (routeName === "SignUp" || routeName === "Verify" || routeName === "Password" || routeName === "SignIn" || routeName === "SendResetPassword" || routeName === "VerifyResetPassword" || routeName === "PasswordReset") {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }
    }, [navigation, route]);

    const [loginAccess, setLoginAccess] = useState({});
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            async function getLocalStore() {
                const localdata = await storeService.getLocalStore('loginState');
                setLoginAccess(localdata);
                setIsLoading(true);
            }
            getLocalStore();
        });

        return unsubscribe;

    }, [navigation, route]);

    return (!isLoading ? <Loading /> :
        <React.Fragment>
            <Stack.Navigator
                name="Modal"
                headerMode="screen"
                initialRouteName="UploadSignInSignUp"
                screenOptions={{
                    gestureEnabled: true,
                    gestureDirection: 'horizontal',
                    cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                }}
            >
                <Stack.Screen
                    name="Upload"
                    component={loginAccess ? ScreenUpload : ScreenUploadSignInSignUp}
                    options={({ route }) => ({
                        title: 'Upload',
                        headerTintColor: theme.colors.themBlack,
                        headerStyle: {
                            elevation: 0,
                            shadowOpacity: 0,
                            borderBottomWidth: 1,

                        },
                        headerTitleStyle: {
                            textAlign: 'center',
                            flexGrow: 1,
                            alignSelf: 'center',
                        },
                        headerRight: () => (
                            <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('UploadHelp')}
                                    style={{
                                        marginRight: 10,
                                        flexDirection: 'row',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                >
                                    <Feather name="help-circle" size={25} color={theme.colors.themBlack} />
                                </TouchableOpacity>
                            </View>
                        ),
                    })}
                />
                <Stack.Screen
                    name="UploadHelp"
                    component={UploadHelp}
                    options={({ route }) => ({
                        title: 'Help',
                        headerStyle: {
                            elevation: 0,
                            shadowOpacity: 0,
                            borderBottomWidth: 1,
                        },
                    })}
                />
                {!loginAccess ? <React.Fragment><Stack.Screen
                    name="SignUp"
                    component={SignUp}
                    options={({ route }) => ({
                        headerBackTitleVisible: false,
                        headerTransparent: true,
                        headerShown: true,
                        headerTitle: true,
                        headerStyle: {
                            elevation: 0,
                            shadowOpacity: 0,
                            borderBottomWidth: 0,

                        },
                        headerTitleStyle: {
                            textAlign: 'center',
                            alignSelf: 'center',
                        },
                    })}
                />
                    <Stack.Screen
                        name="SignIn"
                        component={SignIn}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Verify"
                        component={Verify}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Password"
                        component={Password}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="SendResetPassword"
                        component={SendResetPassword}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="VerifyResetPassword"
                        component={VerifyResetPassword}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="PasswordReset"
                        component={PasswordReset}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                </React.Fragment> : null}
            </Stack.Navigator>
        </React.Fragment>

    );
}
export default RootUpload;