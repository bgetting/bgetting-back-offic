import React, { Component } from 'react';
// import all the components we are going to use
import {
    ActivityIndicator,
    TouchableHighlight,
    Text,
    StyleSheet,
    View,
    Dimensions,
    FlatList,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';
import axios from 'axios';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import DeviceInfo from 'react-native-device-info';
import LazyImage from '../../components/LazyImage';
import NetworkUtils from '../../network/NetworkUtills';
// import * as storeService from '../../storage/service';
import * as theme from '../../constants/theme';
import { API_URL, API_URL_FILE } from '../../Services/api';
const url = `${API_URL_FILE}/resources/`;
const url_profile = `${API_URL_FILE}/resources-profile/`;
import NotFound from '../../components/NotFound';
import Loading from '../../components/Loading/loadingVideo';

class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            datas: [],
            datasList: [],
            onLoading: false,
            orientation: false,
            page: 1,
            refreshing: false,
            error: null,
            loading: false,
            notfound: false,
            isNetwork: true,
            searchClear: false,
            textSearch: "",
            submitTextSearch: "",
            dropdown: "",
            onBack: false,
            onautoFocus: true,
            orientation: false,
            isTablet: DeviceInfo.isTablet(),
            vodeoTotal: '',
            lengJsonData: ''
        };
    }

    componentDidMount() {
        Dimensions.addEventListener('change', ({ window: { width, height } }) => {
            if (width < height) {
                this.setState({ orientation: false });
            } else {
                this.setState({ orientation: true });
            }
        });
    }

    ItemView = ({ item }) => {
        return (
            <TouchableHighlight
                activeOpacity={1}
                underlayColor={"rgba(145, 145, 145, 0.05)"}
                onPress={() => this.getItem(item)}
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}
            >
                <View style={styles.searchTitle}>
                    <MaterialIcons name="youtube-searched-for" size={25} color="#8a8b8c" />
                    <Text style={styles.itemStyle}>{item.title}</Text>
                    <MaterialIcons name="north-west" size={25} color="#8a8b8c" style={styles.searchTitleIconRight} />
                </View>

            </TouchableHighlight>
        );
    };

    getItem = (item) => {
        this.setState({ searchClear: true });
        this.setState({ textSearch: item.title });
        this.submitSearch(item);
    };

    focusNext = async (text) => {
        this.setState({ onBack: false });
        if (text) {
            this.setState({ searchClear: true });
            this.setState({ textSearch: text });
            await axios({
                method: 'GET',
                url: `${API_URL}/api/auth-service/v1/video/search-title?page=1&limit=50&title=${text}`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET',
                },
            })
                .then(response => {
                    this.setState({ datasList: response.data.data });
                })
                .catch(error => {
                    console.log(error);
                });
        }
    }

    btnSearchClear = () => {
        this.setState({ searchClear: false });
        this.setState({ textSearch: "" });
        this.setState({ submitTextSearch: "" });
        this.setState({ datas: [] });
        this.setState({ datasList: [] });
    }

    submitSearch = async (item) => {
        const getTitle = item && item.title ? item.title : this.state.textSearch
        this.setState({ onBack: true });
        const { page } = this.state;
        const isConnected = await NetworkUtils.isNetworkAvailable();
        this.setState({ isNetwork: isConnected });
        this.setState({ refreshing: false });
        this.setState({ loading: true });
        await axios({
            method: 'GET',
            url: `${API_URL}/api/auth-service/v1/video/search-video?limit=30&page=${page}${getTitle ? '&title=' + getTitle : ''}`,
            headers: {
                'Content-Type': 'application/json',
                oauthid: 'AUTH_ID',
                oauthsecret: 'OAUTH_SECRET'
            },
        })
            .then(response => {
                this.setState({
                    datas: this.state.datas.concat(response.data.data),
                });
                this.setState({ lengJsonData: Object.keys(response.data.data).length });
                this.setState({ vodeoTotal: response.data.total });
                this.setState({ notfound: true })
                this.setState({ loading: false });
            })
            .catch(error => {
                this.setState({ error: error });
            });
    }

    fetchMoreData = () => {
        if (this.state.lengJsonData <= this.state.vodeoTotal && this.state.lengJsonData != 0) {
            this.setState(
                prevState => ({
                    page: prevState.page + 1,
                }),
                () => {
                    this.submitSearch();
                },
            );
        }

    };

    _onBack = () => {
        this.setState({ onautoFocus: false });
        this.setState({ onBack: true });
    }
    // onFocus = () => {
    //     this.setState({ onBack: false });  
    //     this.setState({ onautoFocus: false });
    // }
    Empty = () => {
        return (<View style={styles.emptyContain}>
            <NotFound
                source={require('../../../assets/404.png')}
                title='Connect to the internet'
                desc='Check your connection' />
            <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
                <Text style={styles.emptyListStyle} >
                    RETRY
                </Text>
            </TouchableHighlight>
        </View>)
    }

    _onPress(item) {
        const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
        const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
        const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
        const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
        const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
        const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
        const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
        const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
        const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
        const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
        const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

        this.props.navigation.navigate('Watch', {
            itemId: item.id,
            video_id: item.video_id,
            title: item.title,
            video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
            image_path: item.image_path,
            image_cover_path: item.image_cover_path,
            thumbnail: url + item.thumbnail,
            provider_name: item.provider_name,
            time_agos: item.time_agos,
            views: item.views,
            subscribe: item.subscribe,
            username: item.username,
            phone_number: item.phone_number,
            destination: item.destination,
            findUri: getUri,
            playlist_id: item.playlist_id,
            title_playlist: item.title_playlist,
            desc_playlist: item.desc_playlist,
            video_number: item.video_number,
            description: item.description,
            email_address: item.email_address
        });
        this.setState({ findUri: getUri });
        this.props.navigation.setOptions({ tabBarVisible: false });
    }

    _renderItem = ({ item, index }) => {
        return (
            <View style={[styles.post, this.state.isTablet ? styles.post_orientation : this.state.orientation ? styles.post_orientation : null]}>
                <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={0.6}
                    underlayColor="rgba(198, 198, 198, 0.1)">
                    <LazyImage
                        aspectRatio={item.duration}
                        shouldLoad={true}
                        smallSource={{ uri: url + item.thumbnail }}
                        source={{ uri: url + item.thumbnail }}
                        providerName={item.provider_name}
                    />
                </TouchableHighlight>
                <View style={styles.footer}>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('Channel',
                            {
                                userid: item.id,
                                username: item.username,
                                phone_number: item.phone_number,
                                image_path: item.image_path,
                                image_cover_path: item.image_cover_path,
                            })} >
                        <Image style={styles.avatar} source={item.image_path ? { uri: url_profile + item.image_path } : require('../../../assets/me1.png')} />
                    </TouchableOpacity>
                    <View style={styles.timeAgo}>
                        <Text style={styles.title}>{item.title}</Text>
                        <Text style={styles.titleTime}>{item.username ? `${item.username} - ` : null} {item.time_agos}</Text>
                    </View>
                </View>
            </View>
        )
    };

    render() {
        return (
            <React.Fragment>
                {this.state.loading ? <Loading /> : null}
                <View style={styles.search_constainer}>
                    {this.state.onBack ? <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.btnBack}>
                        <Ionicons name="arrow-back-sharp" size={25} color="#8a8b8c" />
                    </TouchableOpacity> :
                        <TouchableOpacity onPress={() => this._onBack()} style={styles.btnBack}>
                            <Ionicons name="arrow-back-sharp" size={25} color="#8a8b8c" />
                        </TouchableOpacity>
                    }
                    <TextInput
                        selectionColor={theme.colors.themeRed}
                        underlineColorAndroid="transparent"
                        maxLength={250}
                        keyboardType='default'
                        autoFocus={this.state.onautoFocus}
                        // onFocus={ () => this.onFocus() }
                        style={styles.otpInputStyle}
                        placeholder="Search BGetting"
                        defaultValue={this.state.textSearch}
                        onChangeText={text => this.focusNext(text)}
                        returnKeyType="search"
                        onSubmitEditing={() => this.submitSearch()}
                        placeholderTextColor="rgba(0,0,0,0.4)"
                    />
                    {this.state.searchClear ? <View style={styles.searchIconDelete}>
                        <TouchableOpacity onPress={() => this.btnSearchClear()}>
                            <EvilIcons name="close" size={25} color="#8a8b8c" />
                        </TouchableOpacity>
                    </View> : null}
                </View>
                <View style={styles.container_text_search}>
                    {!this.state.onBack ? <FlatList
                        data={this.state.datasList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this.ItemView}
                    /> : <View style={this.state.isTablet ? styles.flatlistLayout : this.state.orientation ? styles.flatlistLayout : null}>
                        <View style={{ marginTop: 15 }}>
                            <FlatList
                                numColumns={this.state.isTablet ? this.state.orientation ? 4 : 3 : this.state.orientation ? 3 : 1}
                                key={this.state.isTablet ? this.state.orientation ? "three" : "foure" : this.state.orientation ? "three" : "list"}
                                data={this.state.datas}
                                keyExtractor={(item, index) => index.toString()}
                                onViewableItemsChanged={this.handleViewableChanged}
                                viewabilityConfig={{
                                    viewAreaCoveragePercentThreshold: 30,
                                }}
                                showsVerticalScrollIndicator={false}
                                onEndReachedThreshold={0.1}
                                onEndReached={this.fetchMoreData}
                                ListFooterComponent={this.state.loading && this.state.datas.length > 15 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
                                ListEmptyComponent={!this.state.isNetwork ? this.Empty : null}
                                renderItem={this._renderItem}
                                style={{ backgroundColor: "#fff", marginBottom: 50 }}
                            />
                        </View>
                    </View>}

                </View>
            </React.Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
    },
    post_orientation: {
        paddingHorizontal: 10,
        paddingTop: 15
    },
    flatlistLayout: {
        paddingHorizontal: 7,
    },
    search_constainer: {
        flexDirection: "row",
        backgroundColor: theme.colors.themeWhite,
        height: 50,
        paddingRight: 15,
        elevation: 10,
        shadowOpacity: 10,
        borderBottomWidth: 0,
        alignItems: "center",

    },
    btnBack: {
        alignItems: "center",
        flex: 1,
    },
    otpInputStyle: {
        flex: 3,
        height: 35,
        paddingRight: 40,
        paddingLeft: 10,
        paddingBottom: 9,
        textAlign: 'left',
        backgroundColor: "rgba(145, 145, 145, 0.05)",
        borderRadius: 5,
    },
    searchIconDelete: {
        position: "absolute",
        marginTop: 10,
        right: 20
    },
    container_text_search: {
        backgroundColor: theme.colors.themeWhite,
        height: "100%",
    },
    searchTitle: {
        paddingHorizontal: 15,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    searchTitleIconRight: {
        // flex: 1,
        width: 20
    },

    itemStyle: {
        flex: 8,
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    textInputStyle: {
        height: 40,
        borderWidth: 1,
        paddingLeft: 20,
        margin: 5,
        borderColor: '#009688',
        backgroundColor: '#FFFFFF',
    },
    btnRetry: {
        backgroundColor: theme.colors.themeRed,
        width: 100,
        borderRadius: 5,
    },
    emptyListStyle: {
        padding: 10,
        fontSize: 18,
        textAlign: 'center',
        color: '#fff'
    },
    post: {
        flex: 1,
        paddingHorizontal: 15,
    },
    footer: {
        paddingTop: 10,
        paddingBottom: 15,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 7,
    },
    avatar: {
        width: 35,
        height: 35,
        borderRadius: 18,
        marginRight: 10,
    },
    title: {
        //fontWeight: '700',
        flexWrap: 'wrap',
        flex: 1
    },
    timeAgo: {
        width: 0,
        flexGrow: 1,
    },
    titleTime: {
        color: '#aaaaaa',
        fontSize: 12
    },
});

export default Search;