import React, { Component } from "react";
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    ScrollView,
    TouchableOpacity,
    PanResponder,
    Animated,
    TouchableWithoutFeedback
} from "react-native";
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ProgressBar } from 'react-native-paper';
import Icon from "react-native-vector-icons/FontAwesome";
import * as theme from '../../../../Constants/theme';
const Lights = "http://169.254.250.226:8001/resources/20210420/1618894890414_9acv19ro8knpk7vzi-Record_2020-10-30-12-24-01_7a4090f09f6554852d748ee9fd6f40d3.mp4";
const Thumbnail = "http://192.168.25.1:8001/resources/20210420/thumbnail/1618894890414_9acv19ro8knpk7vzi-Record_2020-10-30-12-24-01_7a4090f09f6554852d748ee9fd6f40d3.mp4_thumbnail.png";
const ChannelIcon = "http://192.168.25.1:8001/resources/20210420/thumbnail/1618894890414_9acv19ro8knpk7vzi-Record_2020-10-30-12-24-01_7a4090f09f6554852d748ee9fd6f40d3.mp4_thumbnail.png";
const { width, height: screenHeight } = Dimensions.get("window");
const height = width * 0.5625;
const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
const url = 'http://169.254.250.226:8001/resources/';

const TouchableIcon = ({ name, children }) => {
    return (
        <TouchableOpacity style={styles.touchIcon}>
            <Icon name={name} size={30} color="#767577" />
            <Text style={styles.iconText}>{children}</Text>
        </TouchableOpacity>
    );
};
const PlaylistVideo = ({ name, channel, views, image }) => {
    return (
        <View style={styles.playlistVideo}>
            <Image
                source={image}
                style={styles.playlistThumbnail}
                resizeMode="cover"
            />
            <View style={styles.playlistText}>
                <Text style={styles.playlistVideoTitle}>{name}</Text>
                <Text style={styles.playlistSubText}>{channel}</Text>
                <Text style={styles.playlistSubText}>{views} views</Text>
            </View>
        </View>
    );
};

const ArticleMore = () => {
    return (
        <Animated.ScrollView style={[styles.scrollView,]}>
            <View style={[styles.topContent, styles.padding]}>
                <Text style={styles.title}>name of video</Text>
                <Text>1M Views</Text>
                <View style={styles.likeRow}>
                    <TouchableIcon name="thumbs-up">10,000</TouchableIcon>
                    <TouchableIcon name="thumbs-down">3</TouchableIcon>
                    <TouchableIcon name="share">Share</TouchableIcon>
                    <TouchableIcon name="download">Save</TouchableIcon>
                    <TouchableIcon name="plus">Add to</TouchableIcon>
                </View>
            </View>

            <View style={[styles.channelInfo, styles.padding]}>
                <Image
                    source={{ uri: ChannelIcon }}
                    style={styles.channelIcon}
                    resizeMode="contain"
                />
                <View style={styles.channelText}>
                    <Text style={styles.channelTitle}>Prerecorded MP3s</Text>
                    <Text>1M Subscribers</Text>
                </View>
            </View>
            <View style={styles.padding}>
                <Text style={styles.playlistUpNext}>Up next</Text>
                <PlaylistVideo
                    image={{ uri: Thumbnail }}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                />
                <PlaylistVideo
                    image={{ uri: Thumbnail }}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                />
                <PlaylistVideo
                    image={{ uri: Thumbnail }}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                />
                <PlaylistVideo
                    image={{ uri: Thumbnail }}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                />
                <PlaylistVideo
                    image={{ uri: Thumbnail }}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                />
                <PlaylistVideo
                    image={{ uri: Thumbnail }}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                />
            </View>

        </Animated.ScrollView>
    )
}

const styles = StyleSheet.create({

    touchIcon: {
        alignItems: "center",
        justifyContent: "center",
    },
    iconText: {
        marginTop: 5,
    },
    channelInfo: {
        flexDirection: "row",
        borderBottomWidth: 1,
        borderBottomColor: "#DDD",
        borderTopWidth: 1,
        borderTopColor: "#DDD",
    },
    channelIcon: {
        width: 50,
        height: 50,
    },
    channelText: {
        marginLeft: 15,
    },
    channelTitle: {
        fontSize: 18,
        marginBottom: 5,
    },
    title: {
        fontSize: 28,
    },
    likeRow: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingVertical: 15,
    },
    padding: {
        paddingVertical: 15,
        paddingHorizontal: 15,
    },
    playlistUpNext: {
        fontSize: 24,
    },
    playlistVideo: {
        flexDirection: "row",
        height: 100,
        marginTop: 15,
        marginBottom: 15,
    },
    playlistThumbnail: {
        width: null,
        height: null,
        flex: 1,
    },
    playlistText: {
        flex: 2,
        paddingLeft: 15,
    },
    playlistVideoTitle: {
        fontSize: 18,
    },
    playlistSubText: {
        color: "#555",
    },
    scrollView: {
        backgroundColor: "#fff",
    },

});

export default ArticleMore;
