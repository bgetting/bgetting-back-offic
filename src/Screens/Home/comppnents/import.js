import React, {Component} from 'react';
import {View, Text, FlatList, Image} from 'react-native';
import axios from 'axios';

class HomeAll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      page: 1,
      refreshing: true,
      error: null,
    };
  }
  componentDidMount() {
    this.fetchUsers();
  }
 

  fetchUsers = () => {
    const {page} = this.state;
    this.setState({ refreshing: true });
    axios
      .get(`http://192.168.25.1:8001/api/auth-service/v1/video?limit=10&page=${page}`)
      .then(response => {
        this.setState({
          users: this.state.users.concat(response.data.data),
        });
        this.setState({ refreshing: false });
      })
      .catch(error => {
        this.setState({error: error});
      });
  };

  fetchMoreUsers = () => {
    this.setState(
      prevState => ({
        page: prevState.page + 1,
      }),
      () => {
        this.fetchUsers();
      },
    );
    
  };
  
  handleRefresh = () => {
    this.setState({ refreshing: false }, () => { this.fetchUsers() });
  }

  ItemSeparator = () => <View style={{
    height: 2,
    backgroundColor: "rgba(0,0,0,0.5)",
    marginLeft: 10,
    marginRight: 10,
  }}
  />
  render() {
    return (
      <View>{console.log(this.state.page)}
      {console.log('TEST',this.state.refreshing)}
      <FlatList
        contentContainerStyle={{
          backgroundColor: '#FBFBF8',
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 15,
        }}
        data={this.state.users}
        keyExtractor={(item, index) => index.toString()}
        onEndReached={this.fetchMoreUsers}
        onEndReachedThreshold={0.1}
        refreshing={this.state.refreshing}
        onRefresh={this.handleRefresh}
        ItemSeparatorComponent={this.ItemSeparator}
        renderItem={({item}) => (
          <View
            style={{
              marginTop: 150,
            }}>              
              <Text>{item.patch+ '-----/---' + item.video_id}</Text>
          </View>
        )}
      />

</View>
    );
  }
}
export default HomeAll;