import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  ActivityIndicator,
  Dimensions,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';
import axios from 'axios';
import DeviceInfo from 'react-native-device-info';
import Orientation from 'react-native-orientation';
import LazyImage from '../../../../components/LazyImage';
import LazyImageShort from '../../../../components/LazyImageShort';
import { API_URL, API_URL_FILE } from '../../../../Services/api';
import NotFound from '../../../../components/NotFound';
import Loading from '../../../../components/Loading';
import * as storeService from '../../../../storage/service';
import NetworkUtils from '../../../../network/NetworkUtills';
// THEME IMPORT
import * as theme from '../../../../constants/theme';
const url = `${API_URL_FILE}/resources/`;
const url_profile = `${API_URL_FILE}/resources-profile/`;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      popular: [],
      page: 1,
      vodeoTotal: '',
      lengJsonData: '',
      refreshing: false,
      error: null,
      loading: false,
      notfound: false,
      isNetwork: true,
      orientation: false,
      isTablet: DeviceInfo.isTablet()
    };

  }

  componentDidMount() {
    Orientation.lockToPortrait();
    Orientation.addOrientationListener(this._orientationDidChange);
    this.fetchVideo();
    this.fetchVideoPopular();
  }
  _orientationDidChange = (orientation) => {
    if (orientation === 'PORTRAIT') {
      this.setState({ orientation: false });
    } else {
      this.setState({ orientation: true });
    }
  }

  componentWillUnmount() {
    Orientation.removeOrientationListener(this._orientationDidChange);
  }

  componentWillReceiveProps(nextProps) {
    for (const index in nextProps) {
      if (nextProps[index] !== this.props[index]) {
        this.fetchRefresh();
      }
    }
  }

  fetchVideoPopular = async () => {
    await axios({
      method: 'GET',
      url: `${API_URL}/api/auth-service/v1/video/popular?limit=15`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        this.setState({
          popular: this.state.popular.concat(response.data.popular),
        });
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  fetchVideo = async () => {
    const isConnected = await NetworkUtils.isNetworkAvailable();
    this.setState({ isNetwork: isConnected });
    const { page } = this.state;
    const localdata = await storeService.getLocalStore('loginState');
    const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
    this.setState({ refreshing: false });
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ notfound: true })
    }, 15000);
    await axios({
      method: 'GET',
      url: `${API_URL}/api/auth-service/v1/video?limit=30&page=${page}${localdataJs.user.id ? '&user_watch_id=' + localdataJs.user.id : ''}${this.props.findTitle ? '&title_tags=' + this.props.findTitle : ''}`,
      headers: {
        'Content-Type': 'application/json',
        oauthid: 'AUTH_ID',
        oauthsecret: 'OAUTH_SECRET'
      },
    })
      .then(response => {
        this.setState({
          datas: this.state.datas.concat(response.data.data),
        });
        this.setState({ lengJsonData: Object.keys(response.data.data).length });
        this.setState({ vodeoTotal: response.data.total });
        this.setState({ loading: false });
        this.setState({ notfound: true });
      })
      .catch(error => {
        this.setState({ error: error });
      });
  };

  fetchMoreData = () => {
    if (this.state.lengJsonData <= this.state.vodeoTotal && this.state.lengJsonData != 0) {
      this.setState(
        prevState => ({
          page: prevState.page + 1,
        }),
        () => {
          this.fetchVideo();
        },
      );
    }
  };
  fetchRefresh = () => {
    this.setState(
      prevState => ({
        page: prevState.page / prevState.page,
        datas: [],
        popular: [],
        notfound: false
      }),
      () => {
        this.fetchVideo();
      },
    );

  };

  handleRefresh = () => {
    this.setState({ refreshing: true }, () => { this.fetchRefresh(); this.fetchVideoPopular() });
    this.setState({ refreshing: false });
  }

  ItemSeparator = () => <View style={{
    height: 2,
    backgroundColor: "rgba(0,0,0,0.1)",
  }}
  />

  Empty = () => {

    return (<View style={styles.emptyContain}>
      <NotFound
        source={require('../../../../../assets/404.png')}
        title='Connect to the internet'
        desc='Check your connection' />
      <TouchableHighlight onPress={() => this.fetchVideo()} style={styles.btnRetry} activeOpacity={0.6} underlayColor="#f97c7c">
        <Text style={styles.emptyListStyle} >
          RETRY
        </Text>
      </TouchableHighlight>
    </View>)
  }

  _onPress(item) {
    const tags0 = item.title_tags[0] ? `&title_tags=${item.title_tags[0]}` : '';
    const tags1 = item.title_tags[1] ? `&title_tags=${item.title_tags[1]}` : '';
    const tags2 = item.title_tags[2] ? `&title_tags=${item.title_tags[2]}` : '';
    const tags3 = item.title_tags[3] ? `&title_tags=${item.title_tags[3]}` : '';
    const tags4 = item.title_tags[4] ? `&title_tags=${item.title_tags[4]}` : '';
    const tags5 = item.title_tags[5] ? `&title_tags=${item.title_tags[5]}` : '';
    const tags6 = item.title_tags[6] ? `&title_tags=${item.title_tags[6]}` : '';
    const tags7 = item.title_tags[7] ? `&title_tags=${item.title_tags[7]}` : '';
    const tags8 = item.title_tags[8] ? `&title_tags=${item.title_tags[8]}` : '';
    const tags9 = item.title_tags[9] ? `&title_tags=${item.title_tags[9]}` : '';
    const getUri = `${tags0}${tags1}${tags2}${tags3}${tags4}${tags5}${tags6}${tags7}${tags8}${tags9}`;

    this.props.navigation.navigate('Watch', {
      rownum: item.rownum,
      itemId: item.id,
      video_id: item.video_id,
      title: item.title,
      video_patch: item.provider_name === 'BGetting' ? url + item.video_patch : item.filename,
      image_path: item?.image_path,
      image_cover_path: item.image_cover_path,
      thumbnail: url + item.thumbnail,
      provider_name: item.provider_name,
      time_agos: item.time_agos,
      views: item.views,
      subscribe: item.subscribe,
      username: item.username,
      phone_number: item.phone_number,
      destination: item.destination,
      findUri: getUri,
      playlist_id: item.playlist_id,
      title_playlist: item.title_playlist,
      desc_playlist: item.desc_playlist,
      video_number: item.video_number,
      description: item.description,
      email_address: item.email_address
    });
    this.props.navigation.setOptions({ tabBarVisible: false });
  }
  _renderItemPopular = (item) => {
    const { rownum } = item.leadingItem;
    return (
      rownum && rownum == 1 ?
        <View >
          {this.ItemSeparator()}
          <View style={styles.layoutShorts}>
            <Text style={styles.titleHeaderShorts}>Recommended</Text>
            <ScrollView
              horizontal={true}
              pagingEnabled={false}
              showsVerticalScrollIndicator={false}
              showsHorizontalScrollIndicator={false}>
              {this.state.popular.map((row, index) => {
                return <View style={styles.shortLayout} key={index}>
                  <TouchableHighlight onPress={() => this._onPress(row)} activeOpacity={0.6}
                    underlayColor="#f97c7c">
                    <LazyImageShort
                      aspectRatio={row?.duration}
                      shouldLoad={true}
                      smallSource={{ uri: url + row?.thumbnail }}
                      source={{ uri: url + row?.thumbnail }}
                      providerName={row?.provider_name}
                    />
                  </TouchableHighlight>
                  <View style={styles.footer}>
                    <View style={styles.timeAgo}>
                      <Text style={styles.title}>{row?.title}</Text>
                    </View>
                  </View>
                </View>
              })}
            </ScrollView>
          </View>
          {this.ItemSeparator()}
          <View style={{ marginTop: 15 }}></View>
        </View> : null
    )
  }
  _renderItem = ({ item, index }) => (
    <View style={[styles.post, this.state.isTablet ? styles.post_orientation : this.state.orientation ? styles.post_orientation : null]}>
      <TouchableHighlight onPress={() => this._onPress(item)} activeOpacity={1} underlayColor="rgba(198, 198, 198, 0.1)">
        <LazyImage
          aspectRatio={item?.duration}
          shouldLoad={true}
          smallSource={{ uri: url + item?.thumbnail }}
          source={{ uri: url + item?.thumbnail }}
          providerName={item?.provider_name}
          videoNumber={item?.video_number}
        />
      </TouchableHighlight>
      <View style={styles.footer}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Channel',
            {
              userid: item.id,
              username: item.username,
              phone_number: item.phone_number,
              image_path: item?.image_path,
              image_cover_path: item.image_cover_path
            })} >
          <Image style={styles.avatar} source={item?.image_path ? { uri: url_profile + item?.image_path } : require('../../../../../assets/me1.png')} />
        </TouchableOpacity>
        <View style={styles.timeAgo}>
          <Text style={styles.title}>{item.title_playlist ? item?.title_playlist : item?.title}</Text>
          <Text style={styles.titleTime}>{item?.username ? `${item?.username} - ` : null} {item?.time_agos}</Text>
        </View>
      </View>
    </View>
  );  

  render() {
    return (
      <React.Fragment>
        {!this.state.notfound ? <Loading /> : null}
        <View style={this.state.isTablet ? styles.flatlistLayout : this.state.orientation ? styles.flatlistLayout : null}>
          <FlatList
            numColumns={this.state.isTablet ? this.state.orientation ? 3 : 2 : this.state.orientation ? 2 : 1}
            key={this.state.isTablet ? this.state.orientation ? "three" : "foure" : this.state.orientation ? "three" : "list"}
            data={this.state.datas}
            keyExtractor={(item, index) => index.toString()}
            onViewableItemsChanged={this.handleViewableChanged}
            viewabilityConfig={{
              viewAreaCoveragePercentThreshold: 10,
            }}
            showsVerticalScrollIndicator={false}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReachedThreshold={0.1}
            onEndReached={this.fetchMoreData}
            ListFooterComponent={this.state.loading && this.state.datas.length > 5 && <ActivityIndicator size="large" color={theme.colors.themeRed} />}
            ItemSeparatorComponent={this._renderItemPopular}
            ListEmptyComponent={this.state.notfound ? this.Empty : !this.state.isNetwork ? this.Empty : null}
            renderItem={this._renderItem}
            style={{ height: '100%' }}
          />
        </View>
      </React.Fragment>
    );
  }
}
export default Home;

const styles = StyleSheet.create({
  post: {
    flex: 1,
    paddingHorizontal: 15,
    marginTop: 7
  },
  post_orientation: {
    paddingHorizontal: 10,
    paddingTop: 15
  },
  flatlistLayout: {
    paddingHorizontal: 7,
  },
  footer: {
    paddingTop: 10,
    paddingBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 7,
  },
  avatar: {
    width: 35,
    height: 35,
    borderRadius: 18,
    marginRight: 10,
  },
  title: {
    // fontWeight: '700',
    flexWrap: 'wrap',
    flex: 1
  },
  description: {
    padding: 15,
    lineHeight: 18
  },
  emptyContain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "center",
    paddingTop: 50
  },
  emptyListStyle: {
    padding: 10,
    fontSize: 18,
    textAlign: 'center',
    color: '#fff'
  },
  timeAgo: {
    width: 0,
    flexGrow: 1,
  },
  titleTime: {
    color: '#aaaaaa',
    fontSize: 12
  },
  shortLayout: {
    backgroundColor: '#FFF',
    margin: 5,
  },
  titleHeaderShorts: {
    paddingTop: 7,
    paddingBottom: 7
  },
  layoutShorts: {
    paddingLeft: 10
  },
  btnRetry: {
    backgroundColor: theme.colors.themeRed,
    width: 100,
    borderRadius: 5,
  }
});