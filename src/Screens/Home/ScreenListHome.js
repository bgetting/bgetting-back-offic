'use strict';
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TouchableOpacity
} from 'react-native';
import axios from 'axios';
import { API_URL } from '../../Services/api';
import * as storeService from '../../storage/service';
import Home from './comppnents/Home/Home';
import * as theme from '../../constants/theme';
import StatusBarTop from '../../components/StatusBar';

const ScreenHome = (props) => {

    const [active, setActive] = useState(0);
    const [tagsRecommended, setTagsRecommended] = useState([]);
    const [findTitle, setFindTitle] = useState('');

    useEffect(() => {

        const fetchVideoRecommendTags = async () => {
            const localdata = await storeService.getLocalStore('loginState');
            const localdataJs = localdata ? JSON.parse(localdata) : { user: { id: "" } };
            await axios({
                method: 'GET',
                url: `${API_URL}/api/auth-service/v1/video/recommend-tags${localdataJs.user.id ? '?user_watch_id=' + localdataJs.user.id : ''}`,
                headers: {
                    'Content-Type': 'application/json',
                    oauthid: 'AUTH_ID',
                    oauthsecret: 'OAUTH_SECRET'
                },
            })
                .then(response => {
                    setTagsRecommended(response.data.data);
                })
                .catch(function (error) {
                    console.log(error);
                })
        };

        fetchVideoRecommendTags();

    }, [props.navigation]);


    const _handlePress = (value) => {
        setFindTitle(value.title_tags);
        setActive(value.rownum);
    };

    return (
        <>
            <StatusBarTop hidden={false} barStyle='dark-content' backgroundColor={theme.colors.themeWhite} />
            <View style={styles.container_view}>
                <ScrollView
                    horizontal={true}
                    pagingEnabled={false}
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}>
                    <View style={styles.viewChip}>
                        <TouchableOpacity
                            onPress={() => _handlePress({ rownum: 0, title_tags: '' })}
                            style={active === 0 ? styles.btnActive : styles.btn}>
                            <Text style={active === 0 ? styles.textChip : styles.text} > All </Text>
                        </TouchableOpacity>
                    </View>
                    {
                        tagsRecommended && tagsRecommended.map((data) => {
                            return (<View style={styles.viewChip} key={data.rownum}>
                                <TouchableOpacity
                                    onPress={() => _handlePress(data)}
                                    style={active === data.rownum ? styles.btnActive : styles.btn}>
                                    <Text style={active === data.rownum ? styles.textChip : styles.text} > {data.title_tags.length >= 20 ? data.title_tags.substring(0, 20) + "..." : data.title_tags} </Text>
                                </TouchableOpacity>
                            </View>)
                        })
                    }
                </ScrollView>
            </View>
            <View style={styles.container}>
                <Home findTitle={findTitle} {...props} />
            </View>
        </>
    );

}
export default ScreenHome;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.colors.themeWhite,
    },
    container_view: {
        backgroundColor: theme.colors.themeWhite,
        paddingBottom: 5
    },
    btn: {
        alignItems: 'center',
        borderColor: theme.colors.themeRed,
        borderRadius: 20,
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 7,
    },
    btnActive: {
        alignItems: 'center',
        backgroundColor: theme.colors.themeRed,
        borderColor: theme.colors.themeRed,
        borderRadius: 20,
        borderWidth: 1,
        paddingHorizontal: 10,
        paddingVertical: 7,
    },
    viewChip: {
        padding: 5
    },
    textChip: {
        color: theme.colors.themeWhite
    },
    text: {
        color: theme.colors.themeRed
    }
});
