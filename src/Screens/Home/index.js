import React, { useEffect, useState } from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
// import Icon from 'react-native-vector-icons/Ionicons';
// import Feather from 'react-native-vector-icons/Feather';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';


// THEME IMPORT
import * as theme from '../../constants/theme';
import ScreenHome from './ScreenListHome';
import Watch from '../Watch/Watch';
import ScreenChannel from '../Profile/ScreenChannel';
import Loading from '../../components/Loading';
import * as storeService from '../../storage/service';

import SignUp from '../SignUp';
import SignIn from '../SignIn';
import Verify from '../SignUp/verify';
import Password from '../SignUp/password';
import Search from '../Search';
import SendResetPassword from '../SignUp/sendResetPassword';
import VerifyResetPassword from '../SignUp/verifyResetPassword';
import PasswordReset from '../SignUp/passwordReset';

const Stack = createStackNavigator();

function RootHome({ navigation, route }) {

    const [isLoading, setIsLoading] = useState(false);
    const [loginAccess, setLoginAccess] = useState({});

    React.useLayoutEffect(() => {
        const routeName = getFocusedRouteNameFromRoute(route);
        if (routeName === "Watch" || routeName === "SignUp" || routeName === "Verify" || routeName === "Password" || routeName === "SignIn" || routeName === "Search") {
            navigation.setOptions({ tabBarVisible: false });
        } else {
            navigation.setOptions({ tabBarVisible: true });
        }

        const unsubscribe = navigation.addListener('focus', () => {
            setIsLoading(true);
        });

        return unsubscribe;
    }, [navigation, route]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            async function getLocalStore() {
                const localdata = await storeService.getLocalStore('loginState');
                const localdataJs = localdata ? JSON.parse(localdata) : {};
                setLoginAccess(localdataJs);
                setIsLoading(true);
            }
            getLocalStore();
        });

        return unsubscribe;

    }, [navigation, route]);

    return (
        !isLoading ? <Loading /> :
            <React.Fragment>
                <Stack.Navigator
                    name="Modal"
                    headerMode="screen"
                    initialRouteName="HomeScreen"
                    screenOptions={{
                        gestureEnabled: true,
                        gestureDirection: 'horizontal',
                        animationEnabled: true,
                        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                    }}
                    mode="modal"
                >
                    <Stack.Screen
                        name="HomeScreen"
                        component={ScreenHome}
                        options={({ route }) => ({
                            title: 'BGetting',
                            headerTitle: false,
                            headerTintColor: theme.colors.themBlack,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,
                            },
                            headerLeft: () => (
                                <View style={{ paddingLeft: 10 }}>
                                    <Image
                                        source={require('../../../assets/logo.png')}
                                        resizeMode="stretch"
                                        style={{height:25, width: 85}}
                                    />
                                </View>
                            ),
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />
                    <Stack.Screen
                        name="Search"
                        component={Search}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: false,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,
                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="Watch"
                        component={Watch}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTitle: false,
                            headerTransparent: true,
                            headerShown: false,
                        })}
                    />
                    <Stack.Screen
                        name="Channel"
                        component={ScreenChannel}
                        options={({ route }) => ({
                            title: 'Your channel',
                            headerTintColor: theme.colors.themBlack,
                            headerRight: () => (
                                <View style={{ marginRight: 10, flexDirection: 'row', }}>
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('Search')}
                                        style={{
                                            marginRight: 10,
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                        }}
                                    >
                                        <EvilIcons name="search" size={30} color={theme.colors.themBlack} />
                                    </TouchableOpacity>
                                </View>
                            ),
                        })}
                    />
                    {!loginAccess.success ? <React.Fragment><Stack.Screen
                        name="SignUp"
                        component={SignUp}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: true,
                            headerShown: true,
                            headerTitle: true,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                        <Stack.Screen
                            name="SignIn"
                            component={SignIn}
                            options={({ route }) => ({
                                headerBackTitleVisible: false,
                                headerTransparent: true,
                                headerShown: true,
                                headerTitle: true,
                                headerStyle: {
                                    elevation: 0,
                                    shadowOpacity: 0,
                                    borderBottomWidth: 0,

                                },
                                headerTitleStyle: {
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                },
                            })}
                        />
                        <Stack.Screen
                            name="Verify"
                            component={Verify}
                            options={({ route }) => ({
                                headerBackTitleVisible: false,
                                headerTransparent: true,
                                headerShown: true,
                                headerTitle: true,
                                headerStyle: {
                                    elevation: 0,
                                    shadowOpacity: 0,
                                    borderBottomWidth: 0,

                                },
                                headerTitleStyle: {
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                },
                            })}
                        />
                        <Stack.Screen
                            name="Password"
                            component={Password}
                            options={({ route }) => ({
                                headerBackTitleVisible: false,
                                headerTransparent: true,
                                headerShown: true,
                                headerTitle: true,
                                headerStyle: {
                                    elevation: 0,
                                    shadowOpacity: 0,
                                    borderBottomWidth: 0,

                                },
                                headerTitleStyle: {
                                    textAlign: 'center',
                                    alignSelf: 'center',
                                },
                            })}
                        />
                        <Stack.Screen
                        name="SendResetPassword"
                        component={SendResetPassword}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="VerifyResetPassword"
                        component={VerifyResetPassword}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    <Stack.Screen
                        name="PasswordReset"
                        component={PasswordReset}
                        options={({ route }) => ({
                            headerBackTitleVisible: false,
                            headerTransparent: false,
                            headerShown: true,
                            headerTitle: false,
                            headerStyle: {
                                elevation: 0,
                                shadowOpacity: 0,
                                borderBottomWidth: 0,

                            },
                            headerTitleStyle: {
                                textAlign: 'center',
                                alignSelf: 'center',
                            },
                        })}
                    />
                    </React.Fragment> : null}
                </Stack.Navigator>
            </React.Fragment>
    );
}
export default RootHome;